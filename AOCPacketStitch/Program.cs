﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using AOC.Library.PacketHelpers;
using AOC.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AOCPacketStitch
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Read in all the packets

            var allImports = new List<string>
            {
                "using System.Collections.Generic; ",
                "using AOC.Library.Primitive; ",
                "using AOC.Util;",
                "using AOC.Network.Packets.Base;",
                "using AOC.Network.Data;"

        };
            var sb = new StringBuilder();
            allImports.ForEach(x => sb.AppendLine(x));
            sb.AppendLine("namespace AOC.Network.Packets.Generated");
            sb.AppendLine("{");

            foreach (var file in Directory.GetFiles("../../../../AOCConsole/bin/Debug/netcoreapp3.1/PacketArchive_new"))
            {
                var packetObject = JsonConvert.DeserializeObject<AOCPacket>(File.ReadAllText(file));
                var classOrigin = "";
                switch (packetObject.Origin)
                {
                    case "OperationResponse":
                        classOrigin = "BaseOpResponse";
                        break;
                    case "Event":
                        classOrigin = "BaseEvent";
                        break;
                    case "OperationRequest":
                        classOrigin = "BaseOpRequest";
                        break;
                }
                var isEvent = packetObject.Origin.Contains("Event");
                var mappingDictionary = new Dictionary<string, Dictionary<int, string>>();
                sb.AppendLine();
                sb.AppendLine($"\t//{packetObject.Origin} | BaseCode - {packetObject.BaseCode}");
                sb.AppendLine($"\tpublic class G{packetObject.Origin}{packetObject.TranslatedCode} : {classOrigin}");
                sb.AppendLine("\t{");
                foreach (var parameter in packetObject.Parameters)
                {
                    var objectType = "";
                    if (parameter.Value is JToken)
                    {
                        var jObject = (JToken) parameter.Value;
                        objectType = DeriveObjectType(jObject);
                    }
                    else
                    {
                        objectType = parameter.Value.GetType().ToString();
                    }

                    // Special string handling
                    object value = parameter.Value;
                    if (objectType == "System.String" && parameter.Value.ToString().EndsWith("=="))
                    {
                        value = $"Original {JsonConvert.SerializeObject(value)} : Converted : {JsonConvert.SerializeObject(value.ToInt16Array())}";
                        objectType = "System.Int16[]";
                    }
                    else if (objectType == "System.String" && parameter.Value.ToString().EndsWith("="))
                    {
                        value = $"Original {JsonConvert.SerializeObject(value)} : Converted : {JsonConvert.SerializeObject(value.ToByteArray())}";
                        objectType = "System.Byte[]";
                    }
                    else
                    {
                        value = JsonConvert.SerializeObject(value);
                    }

                    var paramName = $"Parameter{parameter.Key}";
                    mappingDictionary.Add(paramName,
                        new Dictionary<int, string> {{parameter.Key, DeriveObjectTransformation(objectType)}});
                    sb.AppendLine($"\t\t// From : {JsonConvert.SerializeObject(value)}");
                    sb.AppendLine($"\t\tprotected {objectType} {paramName} {{get;set;}}");
                }
                var codeOrigin = isEvent ? "EventCodes" : "OperationCodes";

                sb.AppendLine($"\t\tprotected G{packetObject.Origin}{packetObject.TranslatedCode}(Dictionary<byte,object> data) : base({codeOrigin}.{packetObject.TranslatedCode},data)");
                sb.AppendLine("\t\t{");
                foreach (var paramName in mappingDictionary)
                {
                    var name = paramName.Key;
                    var index = paramName.Value.First().Key;
                    var transformation = paramName.Value.First().Value;
                    sb.AppendLine($"\t\t\t{name} = SafeExtract({index}){transformation}");
                }

                sb.AppendLine("\t\t}");
                sb.AppendLine("\t}");
            }
            sb.AppendLine("}");

            File.WriteAllText($"../../../../AOC/Network/Packets/Generated/GeneratedPacketStructures_NEW.cs", sb.ToString());
            Console.WriteLine("Done");
        }

        private static string DeriveObjectTransformation(string objectType)
        {
            switch (objectType)
            {
                case "System.Object":
                    return ";";
                case "System.Single":
                    return ".ToFloat();";
                case "System.Int64":
                    return ".ToInt();";
                case "System.String":
                    return ".ToString();";
                case "System.Int64[]":
                    return ".ToIntArray();";
                case "System.Int64[][]":
                    return ".ToInt2DArray();";
                case "System.Single[]":
                    return ".ToFloatArray();";
                case "System.Single[][]":
                    return ".ToFloat2DArray();";
                case "System.Double":
                    return ".ToDouble();";
                case "System.String[]":
                    return ".ToStringArray();";
                case "System.Boolean":
                    return ".ToBoolean();";
                case "System.Boolean[]":
                    return ".ToBooleanArray();";
                case "System.Byte[]":
                    return ".ToByteArray();";
                case "System.Int16[]":
                    return ".ToInt16Array();";
                default:
                    return ".ToUnknown();";
            }
        }

        private static string DeriveObjectType(JToken originToken)
        {
            if (originToken == null) return "System.Single";
            switch (originToken.Type)
            {
                case JTokenType.Array:
                    return $"{DeriveObjectType(originToken.First)}[]";
                case JTokenType.Float:
                    return "System.Single";
                case JTokenType.Integer:
                    return "System.Int64";
                case JTokenType.String:
                    return "System.String";
                case JTokenType.Boolean:
                    return "System.Boolean";
                case JTokenType.Object:
                    return "System.Object";
                default:
                    return "UNKNOWN";
            }
        }
    }
}