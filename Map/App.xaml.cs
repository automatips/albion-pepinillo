﻿using Map;
using System.Windows;

namespace map
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            string arg1 = e.Args[0];

            Comunicador comunicador = new Comunicador(arg1);
            comunicador.Launch();
            MainWindow mainWindow = new MainWindow(comunicador);
            mainWindow.Show();
        }
    }
}
