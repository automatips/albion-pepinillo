﻿using System;

namespace Map.Model
{
    class FishSpot : Entity
    {
        private long id;
        private Single posX;
        private Single posY;
        private string a;
        private string b;
        private string c;

        public FishSpot(long id, Single posX, Single posY, string a, string b, string c)
        {
            this.id = id;
            this.SetLoc(new float[] { posX, posY });
            //this.posX = posX;
            //this.posY = posY;
            this.a = a;
            this.b = b;
            this.c = c;
        }
        public override string ToString()
        {
            return "id: " + id + " a: " + a.ToString() + " b: " + b.ToString();
        }


        public long Id
        {
            get { return id; }
        }



        public string B
        {
            get { return b; }
            set { b = value; }
        }

        public string A
        {
            get { return a; }
            set { a = value; }
        }

        public string C
        {
            get { return c; }
            set { c = value; }
        }

    }
}