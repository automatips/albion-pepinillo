﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Map
{
    internal class ItemObject
    {
        [JsonProperty("LocalizationNameVariable")]
        public string LocalizationNameVariable { get; set; }
        [JsonProperty("LocalizationDescriptionVariable")]
        public string LocalizationDescriptionVariable { get; set; }
        [JsonProperty("LocalizedNames")]
        public Dictionary<string, string> LocalizedNames { get; set; }
        [JsonProperty("LocalizedDescriptions")]
        public Dictionary<string, string> LocalizedDescriptions { get; set; }
        [JsonProperty("Index")]
        public string Index { get; set; }
        [JsonProperty("UniqueName")]
        public string UniqueName { get; set; }


        public Dictionary<string, string> Nombre { get; set; }
        public Dictionary<string, string> Descripcion { get; set; }
        public string Tier { get; set; }
        public string Enchant { get; set; }
        public int TierAbsoluto { get; set; }


        private void setTierCalidad()
        {

        }


        public ItemObject(int ii)
        {

        }



        internal string Text(long langCode)
        {
            var lang = "";
            switch (langCode)
            {
                case 0:
                    lang = "ES";
                    break;
                case 1:
                    lang = "PT";
                    break;
                case 2:
                    lang = "EN";
                    break;

            }
            List<string> lista_negra = new List<string> { "del iniciado", "del maestro", "del anciano", "del experto", "del adepto", "del gran maestro"
            ,"iniciado","de maestro","de iniciadio","de experto","de adepto","de anciano"};

            lista_negra.Concat(new List<string> {"of the initiate", "of the teacher", "of the old man", "of the expert", "of the adept", "of the great teacher"
             , "initiate", "master", "initiate", "expert", "adept", "old man"});

            lista_negra.Concat(new List<string> {"do iniciado", "do professor", "do velho", "do especialista", "do especialista", "do grande professor"
             , "iniciar", "mestre", "iniciar", "especialista", "adepto", "velho"});

            string result = Nombre[lang];

            foreach (string s in lista_negra)
            {
                result = result.ToLower().Replace(s, "");
            }
            if (int.Parse(Tier) > 4)
            {
                result = result + " T" + Tier.Replace(" ", "");
                if (int.Parse(Enchant) > 0)
                {
                    result = result + "." + Enchant.Replace(" ", "");
                }
            }


            return result;
        }



        internal void Inicializar()
        {
            this.Nombre = new Dictionary<string, string>();
            this.Descripcion = new Dictionary<string, string>();
            if (this.LocalizedNames != null)
            {
                try { this.Nombre.Add("ES", this.LocalizedNames["ES-ES"].ToString()); } catch { this.Nombre.Add("ES", ""); }
                try { this.Nombre.Add("PT", this.LocalizedNames["PT-BR"].ToString()); } catch { this.Nombre.Add("PT", ""); }
                try { this.Nombre.Add("EN", this.LocalizedNames["EN-US"].ToString()); } catch { this.Nombre.Add("EN", ""); }
                this.Descripcion.Add("ES", "");
                this.Descripcion.Add("PT", "");
                this.Descripcion.Add("EN", "");
                if (this.LocalizedDescriptions != null)
                {
                    if (this.LocalizedDescriptions.ContainsKey("ES-ES"))
                    {
                        if (!this.Descripcion.ContainsKey("ES"))
                        {
                            try { this.Descripcion.Add("ES", this.LocalizedDescriptions["ES-ES"].ToString()); } catch { }

                        }
                    }
                    if (this.LocalizedDescriptions.ContainsKey("PT-BR"))
                    {
                        
                        if (!this.Descripcion.ContainsKey("PT"))
                        {
                            try { this.Descripcion.Add("PT", this.LocalizedDescriptions["PT-BR"].ToString()); } catch { }
                        }

                    }
                    if (this.LocalizedDescriptions.ContainsKey("EN-US"))
                    {
                        if (!this.Descripcion.ContainsKey("EN"))
                        {
                            try { this.Descripcion.Add("EN", this.LocalizedDescriptions["EN-US"].ToString()); } catch { }
                        }

                    }
                }

                try { this.Tier = this.UniqueName.ToString().Split("_".ToCharArray())[0].Replace("T", ""); } catch { this.Tier = "0"; }
                if(this.UniqueName.Split("@".ToCharArray()).Length > 1)
                {
                    try { this.Enchant = this.UniqueName.Split("@".ToCharArray())[1]; } catch { this.Enchant = "0"; }
                }
                if (Regex.IsMatch(this.Tier, @"^[a-zA-Z]+$"))
                {
                    this.Tier = "0";
                }
                if(this.Enchant == null)
                {
                    this.Enchant = "0";
                }
                try { this.TierAbsoluto = int.Parse(this.Tier) + int.Parse(this.Enchant); } catch { this.TierAbsoluto = 0; }

            }

        }
    }
}