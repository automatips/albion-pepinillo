﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;


namespace Map.Model
{
    public enum MobType
    {
        SKINNABLE,
        HARVESTABLE,
        RESOURCE,
        OTHER

    }

    class MobInfo
    {


        public static List<MobInfo> mobsInfo = new List<MobInfo>()
        {

        };

        int id;
        byte tier;
        MobType mobType;
        HarvestableMobType harvestableMobType;

        public MobInfo(int id, byte tier, MobType mobType)
        {
            this.id = id;
            this.tier = tier;
            this.mobType = mobType;
        }
        public MobInfo(int id, byte tier, MobType mobType, HarvestableMobType harvestableMobType)
        {
            this.id = id;
            this.tier = tier;
            this.mobType = mobType;
            this.harvestableMobType = harvestableMobType;
        }
        public override string ToString()
        {
            return "id: " + id + " tier: " + tier + " mobType: " + mobType;
        }
        internal MobType MobType
        {
            get { return mobType; }
            set { mobType = value; }
        }
        public byte Tier
        {
            get { return tier; }
            set { tier = value; }
        }
        public HarvestableMobType HarvestableMobType
        {
            get { return harvestableMobType; }
            set { harvestableMobType = value; }
        }

        public static MobInfo getMobInfo(long mobId, bool DEBUG = false)
        {
            foreach (MobInfo i in mobsInfo)
                if (i.id == mobId)
                    return i;
            return null;
        }



        public string getMapStringInfo()
        {
            var IconStringKey = "";
            if (MobType != null)
            {

                if (mobType == MobType.HARVESTABLE)
                {

                    switch (harvestableMobType)
                    {
                        case HarvestableMobType.ESSENCE:
                            return "E";
                        case HarvestableMobType.SWAMP:
                            return "F";
                        case HarvestableMobType.STEPPE:
                            return "L";
                        case HarvestableMobType.MOUNTAIN:
                            return "O";
                        case HarvestableMobType.FOREST:
                            return "W";
                        case HarvestableMobType.HIGHLAND:
                            return "R";

                    }

                }
                else if (mobType == MobType.SKINNABLE)
                {
                 return "S";
                }
                else if (mobType == MobType.OTHER)
                {
                    return "M";
                }
                else if (mobType == MobType.RESOURCE)
                {
                   
                    return "T";

                }


            }
            return "NA";

        }


    }
}
