﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace Map.Model
{
    class Player : Entity
    {
        internal bool sonar = true;

        public string Name { get; internal set; }
        public string Alliance { get; internal set; }
        public string Guild { get; internal set; }
        public long Flag { get; internal set; }
        public long isPK { get; internal set; }
        public long[] ItemIndexList { get; internal set; }
        public FrameworkElement cursorArrow { get; internal set; }
        public bool Teleporting { get; internal set; }
        public long Fame { get; internal set; }
        public long Zfame { get; internal set; }
        public DateTime MapTime { get; private set; }
        public DateTime AppTime { get; private set; }

        public Player()
        {
            this.AppTime = DateTime.Now;
            this.MapTime = DateTime.Now;
            this.Fame = 0;
            this.Zfame = 0;
            this.Teleporting = false;
        }

        internal void DrawFame()
        {
            var oput = "";
            var diff = DateTime.Now.Subtract(this.MapTime);
            oput = oput + "Map time: " + diff.ToString().Split('.')[0] + Environment.NewLine;
            oput = oput + "Map fame: " + (int)this.Zfame + Environment.NewLine;
            var dsec = diff.TotalSeconds;
            oput = oput + "Fame/min in map: " + (int)(this.Zfame / dsec * 60.0) + Environment.NewLine;
            oput = oput + Environment.NewLine;
            oput = oput + Environment.NewLine;
            var tdiff = DateTime.Now.Subtract(this.AppTime);
            oput = oput + "Total time: " + tdiff.ToString().Split('.')[0] + Environment.NewLine;
            oput = oput + "Total fame: " + (int)this.Fame + Environment.NewLine;
            var dsec2 = tdiff.TotalSeconds;
            oput = oput + "Total Fame/min: " + (int)(this.Fame / dsec2 * 60.0) + Environment.NewLine;
            //this.FForm.fText = oput;
        }


        public string getRol(List<ItemObject> ItemList)
        {
            //healer.png
            //dps.png
            //tank.png
            string rol = "";
            foreach (ItemObject io in ItemList)
            {
                if (io.UniqueName.Contains(Constants.HOLY) || io.UniqueName.Contains(Constants.NATURE))
                {
                    rol = Constants.HEALER_ROL;
                    break;
                }
                else if (io.UniqueName.Contains(Constants.ICE) || io.UniqueName.Contains(Constants.INFERNO) || io.UniqueName.Contains(Constants.SCIMITAR) || io.UniqueName.Contains(Constants.BOW) ||
                   io.UniqueName.Contains(Constants.RAPIER) || io.UniqueName.Contains(Constants.IRON) || io.UniqueName.Contains(Constants.STAFF) || io.UniqueName.Contains(Constants.HELL) ||
                   io.UniqueName.Contains(Constants.SPEAR) || io.UniqueName.Contains(Constants.SWORD) || io.UniqueName.Contains(Constants.AXE) || io.UniqueName.Contains(Constants.DAGGER) ||
                   io.UniqueName.Contains(Constants.SCYTHE))
                {
                    rol = Constants.DPS_ROL;
                    break;

                }
                else if (io.UniqueName.Contains(Constants.HAMMER) || io.UniqueName.Contains(Constants.MACE) || io.UniqueName.Contains(Constants.FLAI) || io.UniqueName.Contains(Constants.RAM_KEEPER))
                {
                    rol = Constants.TANK_ROL;
                    break;

                }

            }

            return rol;

        }


    }



}
