﻿using System;

namespace Map.Model
{
    class Dungeon : Entity
    {
        private long id;
        //private Single posX;
        //private Single posY;
        private string texto;
        private bool bandera;

        public Dungeon(long id, Single posX, Single posY, string texto, bool bandera)
        {
            this.id = id;
            this.SetLoc(new float[] { posX, posY });
            this.texto = texto;
            this.bandera = bandera;
        }
        public override string ToString()
        {
            return "id: " + id + " texto: " + texto + " bandera: " + bandera;
        }


        public long Id
        {
            get { return id; }
        }



        public bool Bandera
        {
            get { return bandera; }
            set { bandera = value; }
        }

        public string Texto
        {
            get { return texto; }
            set { texto = value; }
        }

    }
}