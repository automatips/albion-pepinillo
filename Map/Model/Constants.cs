﻿namespace Map.Model
{
    internal class Constants
    {

        public static string MOBS = "mobs.json";
        public static string MAPS = "world.json";
        public static string ITS = "items.json";

        public static string HOLY = "HOLY";
        public static string NATURE = "NATURE";


        public static string HEALER_ROL = "healer";
        public static string TANK_ROL = "tank";
        public static string DPS_ROL = "dps";


        public static string ICE = "ICE";
        public static string RAPIER = "RAPIER";
        public static string SPEAR = "SPEAR";
        public static string SCYTHE = "SCYTHE";
        public static string INFERNO = "INFERNO";
        public static string IRON = "IRON";
        public static string SWORD = "SWORD";
        public static string BOW = "BOW";
        public static string DAGGER = "DAGGER";
        public static string HELL = "HELL";
        public static string SCIMITAR = "SCIMITAR";
        public static string STAFF = "STAFF";
        public static string AXE = "AXE";
        public static string RAM_KEEPER = "RAM_KEEPER";
        public static string FLAI = "FLAI";
        public static string MACE = "MACE";
        public static string HAMMER = "HAMMER";
    }
}