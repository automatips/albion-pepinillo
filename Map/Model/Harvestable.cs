﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;

namespace Map.Model
{
    class Harvestable : Entity
    {
        public long Type { get; internal set; }
        public long Tier { get; internal set; }
        public long Size { get; internal set; }
        public long Charges { get; internal set; }
        public string IconStringKey { get; internal set; }


        public Color[] fontPerColor = {
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.White,
                Colors.Black,
                Colors.Black,
                Colors.Black

            };

        public Brush[] harvestBrushes = new Brush[9]
            {
            Brushes.Black,
            Brushes.Gray,
            Brushes.Gray,
            Brushes.Gray,
            Brushes.Blue,
            Brushes.Red,
            Brushes.Coral,
            Brushes.Goldenrod,
            Brushes.Silver
            };
        public Color[] harvestColors = new Color[9]
        {
            Colors.Black,
            Colors.Gray,
            Colors.Gray,
            Colors.Gray,
            Colors.Blue,
            Colors.Red,
            Colors.Coral,
            Colors.Goldenrod,
            Colors.Silver
        };

        public Brush[] chargesBrushes = {
             Brushes.Black,
             Brushes.Green,
             Brushes.Blue,
             Brushes.Purple
            };


        public void setIconStringKey()
        {

            string iconStringKey = "";
            switch (getMapInfo())
            {
                case "W":
                    iconStringKey = "T" + Tier + "_WOOD@" + Charges + ".png";
                    break;
                case "R":
                    iconStringKey = "T" + Tier + "_ROCK@" + Charges + ".png";
                    break;
                case "F":
                    iconStringKey = "T" + Tier + "_FIBER@" + Charges + ".png";
                    break;
                case "O":
                    iconStringKey = "T" + Tier + "_ORE@" + Charges + ".png";
                    break;
                case "H":
                    iconStringKey = "T" + Tier + "_HIDE@" + Charges + ".png";
                    break;
            }
            this.IconStringKey = iconStringKey;

        }

        public string getMapInfo()
        {
            switch ((HarvestableType)Type)
            {
                case HarvestableType.WOOD:
                case HarvestableType.WOOD_GIANTTREE:
                case HarvestableType.WOOD_CRITTER_GREEN:
                case HarvestableType.WOOD_CRITTER_RED:
                case HarvestableType.WOOD_CRITTER_DEAD:
                case HarvestableType.WOOD_CRITTER_FOREST:
                case HarvestableType.WOOD_CRITTER_FOREST_VETERAN:
                
                   return "W";
                case HarvestableType.WOOD_BIG:
                case HarvestableType.WOOD_CRITTER_FOREST_ELITE:
               
                    return "WW";
                case HarvestableType.WOOD_GUARDIAN_RED:
                case HarvestableType.WOOD_GUARDIAN_FOREST:
                case HarvestableType.WOOD_MINIGUARDIAN_FOREST:
                case HarvestableType.WOOD_MINIGUARDIAN_RED:
                    return "WWW";
                case HarvestableType.ROCK:
                case HarvestableType.ROCK_CRITTER_GREEN:
                case HarvestableType.ROCK_CRITTER_RED:
                case HarvestableType.ROCK_CRITTER_DEAD:
                case HarvestableType.ROCK_CRITTER_RANDOM_DUNGEON:
                case HarvestableType.ROCK_CRITTER_FOREST:
                case HarvestableType.ROCK_CRITTER_FOREST_VETERAN:
                    return "R";
                case HarvestableType.ROCK_BIG:
                case HarvestableType.ROCK_CRITTER_FOREST_ELITE:
                    return "RR";
                case HarvestableType.ROCK_GUARDIAN_RED:
                case HarvestableType.ROCK_GUARDIAN_FOREST:
                case HarvestableType.ROCK_MINIGUARDIAN_FOREST:
                case HarvestableType.ROCK_MINIGUARDIAN_RED:
                    return "RRR";
                case HarvestableType.FIBER:
                case HarvestableType.FIBER_CRITTER_FOREST:
                case HarvestableType.FIBER_CRITTER_FOREST_VETERAN:
                case HarvestableType.FIBER_CRITTER:
                    return "F";
                case HarvestableType.FIBER_BIG:
                case HarvestableType.FIBER_CRITTER_FOREST_ELITE:
                    return "FF";
                case HarvestableType.FIBER_GUARDIAN_RED:
                case HarvestableType.FIBER_GUARDIAN_FOREST:
                case HarvestableType.FIBER_MINIGUARDIAN_FOREST:
                case HarvestableType.FIBER_GUARDIAN_DEAD:
                case HarvestableType.FIBER_MINIGUARDIAN_RED:
                    return "FFF";
                case HarvestableType.HIDE:
                case HarvestableType.HIDE_FOREST:
                case HarvestableType.HIDE_FOREST_SMALL:
                case HarvestableType.HIDE_STEPPE:
                case HarvestableType.HIDE_STEPPE_SMALL:
                case HarvestableType.HIDE_SWAMP:
                case HarvestableType.HIDE_MOUNTAIN:
                case HarvestableType.HIDE_HIGHLAND:
                case HarvestableType.HIDE_CRITTER:
                case HarvestableType.HIDE_CRITTER_FOREST:
                case HarvestableType.HIDE_CRITTER_FOREST_VETERAN:
                    return "H";
                case HarvestableType.HIDE_FOREST_BIG:
                case HarvestableType.HIDE_CRITTER_FOREST_ELITE:
                case HarvestableType.HIDE_STEPPE_BIG:
                    return "HH";
                case HarvestableType.HIDE_GUARDIAN:
                case HarvestableType.HIDE_GUARDIAN_FOREST:
                case HarvestableType.HIDE_MINIGUARDIAN_FOREST:
                case HarvestableType.HIDE_MINIGUARDIAN:
                    return "HHH";
                case HarvestableType.ORE:
                case HarvestableType.ORE_CRITTER_GREEN:
                case HarvestableType.ORE_CRITTER_RED:
                case HarvestableType.ORE_CRITTER_FOREST:
                case HarvestableType.ORE_CRITTER_FOREST_VETERAN:
                case HarvestableType.ORE_CRITTER_DEAD:
                    return "O";
                case HarvestableType.ORE_BIG:
                case HarvestableType.ORE_CRITTER_FOREST_ELITE:
                    return "OO";
                case HarvestableType.ORE_GUARDIAN_RED:
                case HarvestableType.ORE_GUARDIAN_FOREST:
                case HarvestableType.ORE_MINIGUARDIAN_FOREST:
                case HarvestableType.ORE_MINIGUARDIAN_RED:
                    return "OOO";
                case HarvestableType.DEADRAT:
                    return "DEAD_RAT";
                case HarvestableType.SILVERCOINS_NODE:
                case HarvestableType.SILVERCOINS_NODE_RICH:
                case HarvestableType.SILVERCOINS_LOOT_STANDARD_TRASH:
                case HarvestableType.SILVERCOINS_LOOT_VETERAN_TRASH:
                case HarvestableType.SILVERCOINS_LOOT_ELITE_TRASH:
                case HarvestableType.SILVERCOINS_LOOT_ROAMING:
                case HarvestableType.SILVERCOINS_LOOT_ROAMING_MINIBOSS:
                case HarvestableType.SILVERCOINS_LOOT_ROAMING_BOSS:
                case HarvestableType.SILVERCOINS_LOOT_STANDARD:
                case HarvestableType.SILVERCOINS_LOOT_VETERAN:
                case HarvestableType.SILVERCOINS_LOOT_ELITE:
                case HarvestableType.SILVERCOINS_LOOT_STANDARD_MINIBOSS:
                case HarvestableType.SILVERCOINS_LOOT_VETERAN_MINIBOSS:
                case HarvestableType.SILVERCOINS_LOOT_ELITE_MINIBOSS:
                case HarvestableType.SILVERCOINS_LOOT_STANDARD_BOSS:
                case HarvestableType.SILVERCOINS_LOOT_VETERAN_BOSS:
                case HarvestableType.SILVERCOINS_LOOT_ELITE_BOSS:
                case HarvestableType.SILVERCOINS_RD_STANDARD_TRASH:
                case HarvestableType.SILVERCOINS_RD_STANDARD:
                case HarvestableType.SILVERCOINS_RD_STANDARD_STANDARD:
                case HarvestableType.SILVERCOINS_RD_STANDARD_UNCOMMON:
                case HarvestableType.SILVERCOINS_RD_STANDARD_RARE:
                case HarvestableType.SILVERCOINS_RD_STANDARD_LEGENDARY:
                case HarvestableType.SILVERCOINS_RD_VETERAN_TRASH:
                case HarvestableType.SILVERCOINS_RD_VETERAN:
                case HarvestableType.SILVERCOINS_RD_VETERAN_STANDARD:
                case HarvestableType.SILVERCOINS_RD_VETERAN_UNCOMMON:
                case HarvestableType.SILVERCOINS_RD_VETERAN_RARE:
                case HarvestableType.SILVERCOINS_RD_VETERAN_LEGENDARY:
                case HarvestableType.SILVERCOINS_RD_ELITE_TRASH:
                case HarvestableType.SILVERCOINS_RD_ELITE:
                case HarvestableType.SILVERCOINS_RD_ELITE_STANDARD:
                case HarvestableType.SILVERCOINS_RD_ELITE_UNCOMMON:
                case HarvestableType.SILVERCOINS_RD_ELITE_RARE:
                case HarvestableType.SILVERCOINS_RD_ELITE_LEGENDARY:
                case HarvestableType.SILVERCOINS_LOOT_CHEST_STANDARD:
                case HarvestableType.SILVERCOINS_LOOT_CHEST_STANDARD_TRASH:
                case HarvestableType.SILVERCOINS_LOOT_CHEST_VETERAN:
                case HarvestableType.SILVERCOINS_LOOT_CHEST_DEMON:
                case HarvestableType.SILVERCOINS_LOOT_SARCOPHAGUS_STANDARD_MINIBOSSWOOD:
                    return "SILVER";
                case HarvestableType.CHEST_EXP_SILVERCOINS_LOOT_STANDARD:
                case HarvestableType.CHEST_EXP_SILVERCOINS_LOOT_VETERAN:
                    return "CHEST";
                default:
                    return "ERR  " + Type;
            }
        }


    }
}
