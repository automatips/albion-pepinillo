﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Map.Model
{
	public enum HarvestableMobType
	{
		ESSENCE,
		SWAMP,
		STEPPE,
		MOUNTAIN,
		FOREST,
		HIGHLAND
	}
}
