﻿using Map.Handlers;
using Map.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using NHotkey.Wpf;
using NHotkey;
using System.Runtime.InteropServices;
using Map.Util;
using SharpPcap;
using System.Media;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace Map
{

    public partial class MainWindow : Window
	{
		private static Thread _backgroundThread2;

		private float FixedWidth;
		private float FixedHeight;
		private float SCALE;
		TextBlock coordsText;
		private readonly DispatcherTimer timer = new DispatcherTimer();
		private double RotationAngle = 135;
		private int animTick = 0;

		internal EntityHandler eHandler;
		private Dictionary<string, Uri> iconsDict;
		private Dictionary<string, List<string>> alliesData;

		internal List<Ellipse> listA;
		//private Comunicador comunicador;


		private bool cursorArrowDrawn = false;
        private __MenuWindow menuWindow;


		private bool redrawCanvas = false;
		private float lastSize = 0;


        //private __IMySettings Settings;
        private __pSettings Settings;
		private IConfigurationRoot ConfigRoot;
        private ICaptureDevice DeviceSelected;
        private string PATH;
        private bool ventanaActiva;
        private bool dibujar;
        private bool dibujando;
        private SoundPlayer soundPlayer;
        private Lang lang;

		private List<string> SafeMaps;
       

  //      [DllImport("user32.dll")]
		//private static extern bool GetCursorPos(out Point lpPoint);

		//private static Point GetCursorPos()
		//{
		//	Point mouse = default;
		//	if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
		//	{
		//		GetCursorPos(out mouse);
		//	}
		//	else
		//	{
		//		// How to do on Linux and OSX?
		//	}
		//	return mouse;
		//}



		protected override void OnClosed(EventArgs e)
		{
			__MainClass.Run=false;
			base.OnClosed(e);
			Application.Current.Shutdown();
			Environment.Exit(-1);
		}





		internal MainWindow(Comunicador comunicador)
		{

			PATH = Process.GetCurrentProcess().MainModule.FileName.Replace("\\Map.exe", "");
			//Settings = new ConfigurationBuilder<__IMySettings>().UseJsonFile(PATH + "\\appsettings.json").Build();
			//var _Settings = new ConfigurationBuilder().AddJsonFile(PATH + "\\appsettings.json", optional: false, reloadOnChange: true).Build();

			ConfigRoot = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile(PATH + "\\appsettings.json", optional: true, reloadOnChange: true)
				.Build();
			Settings = Serialize(ConfigRoot).ToObject<__pSettings>();





			DeviceSelected = SelectPacketDevice();


			//HotkeyManager.Current.AddOrReplace("Increment", Key.F5, ModifierKeys.Control | ModifierKeys.Alt, OnIncrement);
			HotkeyManager.Current.AddOrReplace("abrirMenu", Key.F5, ModifierKeys.Control, __abrirMenu);
			HotkeyManager.Current.AddOrReplace("mostrar/ocultar", Key.F6, ModifierKeys.Control, __alternarRadar);
			HotkeyManager.Current.AddOrReplace("clearEntitys", Key.F7, ModifierKeys.Control, __clearEntitys);


			//string arg1 = sEArgs.Args[0];


			_backgroundThread2 = new Thread(BackgroundThread2);
			_backgroundThread2.IsBackground = true;
			_backgroundThread2.Name = "Calc.exe";



			//comunicador = new Comunicador(name);

			eHandler = new EntityHandler();
			eHandler.Comunicador = comunicador;
			//comunicador.SetEH(eHandler);
			//comunicador.GetData();

			InitializeComponent();



			MouseDown += Window_MouseDown;
			SizeChanged += WindowOnSizeChanged;
			//KeyDown += new KeyEventHandler(MainWindow_KeyDown);
			_backgroundThread2.SetApartmentState(ApartmentState.MTA);
			_backgroundThread2.Start();



		}


	
		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			SafeMaps = new List<string>();
			SafeMaps.Add("caerleon");
			SafeMaps.Add("lymhurst");
			SafeMaps.Add("fort sterling");
			SafeMaps.Add("bridgewatch");
			SafeMaps.Add("martlock");
			SafeMaps.Add("conqueror");
			UnmanagedMemoryStream s = __Resource.ding;
			soundPlayer = new SoundPlayer(s);

			lang = new Lang(0);
			dibujar = true;
			Window window = (Window)sender;
			window.Topmost = true;

			Canvas canvas = Canvas;
			/////

			iconsDict = new Dictionary<string, Uri>();
			alliesData = new Dictionary<string, List<string>>();


			Utils.LoadMobs();
			Utils.LoadItems();
			Utils.LoadMaps();
			Utils.LoadAlliesData(alliesData);


			const string BANNER = @"
  _____                   _ 
 |  __ \                 (_)
 | |__) |   ___   _ __    _ 
 |  ___/   / _ \ | '_ \  | |
 | |      |  __/ | |_) | | |
 |_|       \___| | .__/  |_|
                 | |        
                 |_| 
Hotkeys:
CTRL+F5 : Open and Close Menu
CTRL+F6 : Open and Close Radar
CTRL+F7 : Refresh Radar
";

			Console.Clear();
			Console.WriteLine(BANNER);
			string path = Process.GetCurrentProcess().MainModule.FileName.Replace("\\Map.exe", "");
			var basepath = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName.Replace("Map.exe", "");
			var PATH = Process.GetCurrentProcess().MainModule.FileName.Replace("\\Map.exe", "");
			FileInfo[] files = new DirectoryInfo(PATH + "\\images\\").GetFiles("*.png");
			foreach (FileInfo obj in files)
			{
				Uri uri = new Uri(obj.FullName);
				string name = obj.Name.Replace("_LEVEL1", "").Replace("_LEVEL2", "").Replace("_LEVEL3", "");
				iconsDict.Add(name, uri);
			}

			timer.Tick += BackgroundThread;
			timer.Interval = TimeSpan.FromMilliseconds(0);
			timer.Start();

		}


		private void BackgroundThread(object sender, EventArgs e)
		{//aca


			Window window = (Window)this;
			window.Topmost = false;
			window.Topmost = true;

			Application.Current.Dispatcher.Invoke(new Action(delegate
			{
				
				if (!dibujar)
                {
					Canvas.Children.Clear();
					dibujando = false;
					lastSize = -1; //force redraw circles 
					return;

				}
				else
                {
					dibujar = true;
					dibujando = true;
				}

				//Settings = new ConfigurationBuilder<__IMySettings>().UseJsonFile(PATH + "\\appsettings.json").Build();

				MainWindow mainWindows = (MainWindow)Canvas.Parent;
				double windowWidht = mainWindows.ActualWidth;
				double windowHeight = mainWindows.ActualHeight;
				float centerX = Convert.ToSingle(windowWidht / 2.0);
				float centerY = Convert.ToSingle(windowHeight / 2.0);

				(float widht, float height, float scale) = getFixedWindowSize(windowWidht, windowHeight);


				if (lastSize != widht * height)
                {
					Canvas.Children.Clear();
					redrawCanvas = false;
					lastSize = widht * height;
					Canvas.Children.Add(CEllipse((double)widht * 0.25, (double)height * 0.25, centerX, centerY,"radar1"));
					Canvas.Children.Add(CEllipse((double)widht * 0.50, (double)height * 0.50, centerX, centerY, "radar2"));
					Canvas.Children.Add(CEllipse((double)widht * 0.75, (double)height * 0.75, centerX, centerY, "radar3"));
					Canvas.Children.Add(CEllipseFill(7.0, 7.0, widht / 2f, height / 2f, Brushes.Fuchsia, "centro"));

					Canvas.RenderTransform = new ScaleTransform(1.0, 1.0);

				}

				int albionRotationAngle = 135;

				//TextBlock text = Text(widht / 2f - 5f, height / 2f, eHandler.LocalPlayer.X + " " + eHandler.LocalPlayer.Y, Colors.Purple);
				//text.RenderTransform = new RotateTransform(albionRotationAngle);
				//Canvas.Children.Add(text);


				if (animTick > 9)
					animTick = 0;
				else
					animTick = animTick + 1;

				drawThread(Canvas, widht, height, scale);
			}));
			//}
		}

		private void drawThread2(Canvas canvas, float fixedWidth, float fixedHeight, float scale)
        {

			eHandler.LocalPlayer.setMapCoords(fixedWidth, fixedHeight, scale, eHandler.LocalPlayer.X, eHandler.LocalPlayer.Y);

			float entiyX = fixedWidth / 2f + scale * (-1f * eHandler.LocalPlayer.XClickMap + eHandler.LocalPlayer.X);
			float enityY = fixedHeight / 2f + scale * (eHandler.LocalPlayer.YClickMap - eHandler.LocalPlayer.Y);

			if (!cursorArrowDrawn)
			{
				cursorArrowDrawn = true;
				eHandler.LocalPlayer.setMapCoords(FixedWidth, FixedHeight, SCALE, eHandler.LocalPlayer.X, eHandler.LocalPlayer.Y);
				Canvas.Children.Add((Grid)eHandler.LocalPlayer.cursorArrow);
			}
			else
			{
				//moveTo(eHandler.LocalPlayer.cursorArrow, eHandler.LocalPlayer.XClickMap, eHandler.LocalPlayer.YClickMap);
			}


			canvas.RenderTransform = new RotateTransform(-135, fixedWidth / 2f, fixedHeight / 2f);


		}
		private void drawThread(Canvas canvas, float fixedWidth, float fixedHeight, float scale)
		{
			ConfigRoot.Reload();
			while (true)
			{
				try { Settings = Serialize(ConfigRoot).ToObject<__pSettings>(); break; } catch { }

			}

			int albionRotationAngle = 135;

			List<Dungeon> dungeons = new List<Dungeon>();
			try { lock (eHandler.Dungeons) { dungeons = eHandler.Dungeons.ToList(); } } catch { }
			foreach (Dungeon d in dungeons)
			{
				string element_id = "dungeon_" + d.Id.ToString();
				if (d.Remove)
				{
					clearById(canvas, element_id);
					eHandler.RemoveEntity(d.Id);
					continue;
				}
				d.setMapCoords(fixedWidth, fixedHeight, scale, eHandler.LocalPlayer.X, eHandler.LocalPlayer.Y);
				Grid element = (Grid)LogicalTreeHelper.FindLogicalNode(canvas, element_id);
				if (element != null)
				{
					moveTo(element, d.XMap, d.YMap);
					continue;
				}

				d.Grid = new Grid();

				if (d.Texto.Contains("KPR"))
				{
					//el = CEllipseFill(6.0, 18.0, entiyX, enityY, Brushes.LightGreen);
					d.Grid.Children.Add(GridEllipse(6.0, 18.0, 0, 0, Brushes.Yellow));


				}
				else if (d.Texto.Contains("CORRUPT"))
				{
					d.Grid.Children.Add(GridEllipse(6.0, 18.0, 0, 0, Brushes.IndianRed));

				}
				else if (d.Texto.Contains("SOLO"))
				{
					d.Grid.Children.Add(GridEllipse(6.0, 18.0, 0, 0, Brushes.LightGreen));
				}
				else
				{
					d.Grid.Children.Add(GridEllipse(15.0, 20.0, 0, 0, Brushes.Aqua));

				}
				d.Grid.RenderTransform = new RotateTransform(135);

				d.Grid.Name = element_id;
				d.Grid.Margin = new Thickness(d.XMap, d.YMap, 0.0, 0.0);
				canvas.Children.Add(d.Grid);

			}




			List<Player> players = null;

			try
			{
				lock (eHandler.Players)
				{
					players = eHandler.Players.Values.ToList();
				}
			}
			catch { }



			




			//try
			//{
			//	var ActualMap = eHandler.LocalPlayer.mapInfo;

			//	if (ActualMap.Type != null)
			//	{
			//Console.WriteLine(ActualMap.Index);
			//switch (ActualMap)
			//{

			//	case "OPENPVP_YELLOW":
			//	case "HIDEOUT":
			//	case "SAFEAREA":
			//	case "PLAYERISLAND":
			//	case "GUILDISLAND":
			//	case "PLAYERCITY_BLACK_ROYAL_NOFURNITURE":
			//	case "PLAYERCITY_SAFEAREA_NOFURNITURE":
			//	case "PLAYERCITY_BLACK_ROYAL":
			//	case "PLAYERCITY_SAFEAREA_01":
			//	case "PLAYERCITY_SAFEAREA_02":
			//		Console.WriteLine("Zona Segura");
			//		break;
			//	case "OPENPVP_BLACK_1":
			//	case "OPENPVP_BLACK_2":
			//	case "OPENPVP_BLACK_3":
			//	case "OPENPVP_BLACK_4":
			//	case "OPENPVP_BLACK_5":
			//	case "OPENPVP_BLACK_6":
			//	case "DUNGEON_SAFEAREA":
			//	case "DUNGEON_RED":
			//	case "OPENPVP_RED":
			//	case "ARENA_STANDARD":

			//		foreach (Player p in players)
			//		{

			//			var fx = -6f;
			//			var fy = -12f;
			//			var ancho = 9;
			//			var alto = 11;

			//			string element_id = "player_" + p.Id.ToString();
			//			bool isEnemy = true;
			//			var isAllyG = napginfo.Contains(p.Guild);
			//			var isAllyA = napainfo.Contains(p.Alliance);


			//			if (p.Remove)
			//			{
			//				clearById(canvas, element_id);
			//				clearById(canvas, "itemlist_" + p.Id.ToString());
			//				eHandler.RemoveEntity(p.Id);
			//				continue;
			//			}

			//			if (!Settings.ShowChars)
			//			{
			//				clearById(canvas, element_id);
			//				clearById(canvas, "itemlist_" + p.Id.ToString());
			//				continue;

			//			}

			//			p.setMapCoords(fixedWidth, fixedHeight, scale, eHandler.LocalPlayer.X, eHandler.LocalPlayer.Y);

			//			Grid element = (Grid)LogicalTreeHelper.FindLogicalNode(canvas, element_id);
			//			if (element != null)
			//			{
			//				moveTo(element, p.XMap, p.YMap);
			//			}
			//			else
			//			{
			//				p.Grid = new Grid();
			//				p.Grid.Name = element_id;

			//				var iconString = "";
			//				if (Settings.ShowFlag || true)
			//				{
			//					switch (p.Flag)
			//					{
			//						case 1:
			//							iconString = "martlock.png";
			//							break;
			//						case 2:
			//							iconString = "lymhurst.png";
			//							break;
			//						case 3:
			//							iconString = "bridgewatch.png";
			//							break;
			//						case 4:
			//							iconString = "fortsterling.png";
			//							break;
			//						case 5:
			//							iconString = "thetford.png";
			//							break;
			//					}

			//				}

			//				Uri uriKey = null;
			//				if (iconString != "")
			//					try { uriKey = iconsDict[iconString]; } catch { }



			//				if (uriKey != null)
			//				{
			//					Image image = new Image();

			//					BitmapImage bitmap = new BitmapImage();
			//					bitmap.BeginInit();
			//					bitmap.CacheOption = BitmapCacheOption.OnLoad;
			//					bitmap.UriSource = uriKey;
			//					bitmap.EndInit();
			//					bitmap.Freeze();
			//					image.Source = bitmap;
			//					image.Width = ancho;
			//					image.Height = alto;
			//					//image.Margin = new Thickness(p.XMap - fx, p.YMap - fy, 0.0, 0.0);
			//					image.Margin = new Thickness(10, 15, 0, 0);
			//					image.RenderTransform = new RotateTransform(albionRotationAngle);



			//					//p.addImageIcon(image);
			//					p.Grid.Children.Add(image);
			//					Canvas.SetZIndex(image, 50);

			//				}
			//			}




			//			if (Settings.ShowRol && isAllyA == false && isAllyG == false)
			//			{


			//				Uri uriRolKey = null;
			//				FrameworkElement rol_icon = (FrameworkElement)LogicalTreeHelper.FindLogicalNode(canvas, "rolicon_" + p.Id.ToString());
			//				if (rol_icon == null)
			//				{
			//					List<ItemObject> items = new List<ItemObject>();
			//					foreach (var itemIndex in p.ItemIndexList)
			//					{
			//						try
			//						{
			//							ItemObject item = ItemInfo.itemDictionary[itemIndex.ToString()];
			//							if (item.TierAbsoluto > 0)
			//								items.Add(item);
			//						}
			//						catch (Exception ex) { }
			//					}
			//					if (items != null)
			//						try { uriRolKey = iconsDict[p.getRol(items) + ".png"]; } catch { }

			//				}
			//				if (uriRolKey != null)
			//				{


			//					if (rol_icon == null)
			//					{
			//						Image image = new Image();
			//						BitmapImage bitmap = new BitmapImage();
			//						bitmap.BeginInit();
			//						bitmap.CacheOption = BitmapCacheOption.OnLoad;
			//						bitmap.UriSource = uriRolKey;
			//						bitmap.EndInit();
			//						bitmap.Freeze();
			//						image.Source = bitmap;
			//						image.Width = ancho;
			//						image.Height = alto;
			//						//image.Margin = new Thickness(p.XMap, p.YMap, 0.0, 0.0);
			//						image.Margin = new Thickness(25, 10, 0, 0.0);
			//						image.RenderTransform = new RotateTransform(albionRotationAngle);
			//						//p.addImageIcon(image);
			//						image.Name = "rolicon_" + p.Id.ToString();
			//						p.Grid.Children.Add(image);
			//						Canvas.SetZIndex(image, 20);
			//					}

			//				}


			//			}
			//			else
			//			{
			//				clearById(p.Grid, "rolicon_" + p.Id.ToString());
			//			}
			//			//canvas.Children.Add(CEllipseFill(6.0, 6.0, p.X, p.Y, Brushes.Red));
			//			//p.addIcon(CEllipseFill(6.0, 6.0, p.XMap, p.YMap, Brushes.Red, "player_"+p.Id.ToString()));





			//			if (Settings.ShowEquip && isAllyG == false && isAllyA == false)
			//			{
			//				var lineas = "";
			//				foreach (var itemIndex in p.ItemIndexList)
			//				{
			//					try
			//					{
			//						ItemObject item = ItemInfo.itemDictionary[itemIndex.ToString()];
			//						if (item.TierAbsoluto > 0)
			//							lineas += item.Text(lang.GetLang()).ToUpper() + "\n";

			//					}
			//					catch (Exception ex)
			//					{

			//					}
			//				}

			//				var imtemList = Text(p.XMap, p.YMap, lineas, Colors.Red);
			//				imtemList.RenderTransform = new RotateTransform(albionRotationAngle);
			//				FrameworkElement itemlist_element = (FrameworkElement)LogicalTreeHelper.FindLogicalNode(canvas, "itemlist_" + p.Id.ToString());
			//				if (itemlist_element == null)
			//				{
			//					imtemList.Name = "itemlist_" + p.Id.ToString();
			//					canvas.Children.Add(imtemList);
			//				}
			//				else
			//				{
			//					moveTo(itemlist_element, p.XMap, p.YMap);
			//				}


			//			}
			//			else
			//			{
			//				clearById(canvas, "itemlist_" + p.Id.ToString());

			//			}


			//			FrameworkElement player_dot = (FrameworkElement)LogicalTreeHelper.FindLogicalNode(canvas, element_id);

			//			if (player_dot == null && isAllyG == false && isAllyA == false)
			//			{
			//				p.Grid.Children.Add(CEllipseFill(13, 13.0, 0, 0, Brushes.Red, element_id));




			//				if (p.sonar)
			//				{


			//					if (Settings.SoundChars)
			//						soundPlayer.Play();
			//					p.sonar = false;
			//				}


			//			}
			//			else if (player_dot == null && isAllyG == true)
			//			{
			//				p.Grid.Children.Add(CEllipseFill(13, 13.0, 0, 0, Brushes.Green, element_id));



			//			}
			//			else if (player_dot == null && isAllyA == true)
			//			{
			//				p.Grid.Children.Add(CEllipseFill(13, 13.0, 0, 0, Brushes.Purple, element_id));



			//			}

			//			FrameworkElement player_in_canva = (FrameworkElement)LogicalTreeHelper.FindLogicalNode(canvas, element_id);
			//			if (player_in_canva == null)
			//			{
			//				p.Grid.Margin = new Thickness(p.XMap - p.Grid.ActualHeight / 2, p.YMap - p.Grid.ActualWidth / 2, 0.0, 0.0);
			//				//p.Grid.Background = Brushes.AliceBlue;
			//				canvas.Children.Add(p.Grid);
			//			}



			//		}
			//		break;

			//}
			//	}

			//	else if (ActualMap.Type == null)
			//	{
			//		Console.WriteLine("null");

			//	}
			//}
			//catch { }




			

			foreach (Player p in players)
			{
				
				
				var fx = -6f;
				var fy = -12f;
				var ancho = 9;
				var alto = 11;

				string element_id = "player_" + p.Id.ToString();
				



				if (p.Remove)
				{
					clearById(canvas, element_id);
					clearById(canvas, "itemlist_" + p.Id.ToString());
					eHandler.RemoveEntity(p.Id);
					continue;
				}
			
				if (!Settings.ShowChars)
				{
					clearById(canvas, element_id);
					clearById(canvas, "itemlist_" + p.Id.ToString());
					continue;

				}

				p.setMapCoords(fixedWidth, fixedHeight, scale, eHandler.LocalPlayer.X, eHandler.LocalPlayer.Y);
				if(p.Grid == null)
                {
					Grid element = (Grid)LogicalTreeHelper.FindLogicalNode(canvas, element_id);
					if(element == null)
                    {
						p.Grid = new Grid();
						p.Grid.Name = element_id;

						var iconString = "";
						if (Settings.ShowFlag || true)
						{
							switch (p.Flag)
							{
								case 1:
									iconString = "martlock.png";
									break;
								case 2:
									iconString = "lymhurst.png";
									break;
								case 3:
									iconString = "bridgewatch.png";
									break;
								case 4:
									iconString = "fortsterling.png";
									break;
								case 5:
									iconString = "thetford.png";
									break;
							}

						}

						Uri uriKey = null;
						if (iconString != "")
							try { uriKey = iconsDict[iconString]; } catch { }



						if (uriKey != null)
						{
							Image image = new Image();

							BitmapImage bitmap = new BitmapImage();
							bitmap.BeginInit();
							bitmap.CacheOption = BitmapCacheOption.OnLoad;
							bitmap.UriSource = uriKey;
							bitmap.EndInit();
							bitmap.Freeze();
							image.Source = bitmap;
							image.Width = ancho;
							image.Height = alto;
							//image.Margin = new Thickness(p.XMap - fx, p.YMap - fy, 0.0, 0.0);
							image.Margin = new Thickness(10, 15, 0, 0);
							image.RenderTransform = new RotateTransform(albionRotationAngle);



							//p.addImageIcon(image);
							p.Grid.Children.Add(image);
							Canvas.SetZIndex(image, 50);

						}

						}
						else
						{
							p.Grid = element;
						}

                }

				moveTo(p.Grid, p.XMap, p.YMap);

				var isAllyA = alliesData["alliance_allies"].Contains(p.Alliance.ToLower());
				var isAllyG = alliesData["guild_allies"].Contains(p.Guild.ToLower());


				if (Settings.ShowRol && isAllyA == false && isAllyG == false)
				{


					Uri uriRolKey = null;
					FrameworkElement rol_icon = (FrameworkElement)LogicalTreeHelper.FindLogicalNode(canvas, "rolicon_" + p.Id.ToString());
					if (rol_icon == null)
					{
						List<ItemObject> items = new List<ItemObject>();
						foreach (var itemIndex in p.ItemIndexList)
						{
							try
							{
								ItemObject item = ItemInfo.itemDictionary[itemIndex.ToString()];
								if (item.TierAbsoluto > 0)
									items.Add(item);
							}
							catch (Exception ex) { }
						}
						if (items != null)
							try { uriRolKey = iconsDict[p.getRol(items) + ".png"]; } catch { }

					}
					if (uriRolKey != null)
					{


						if (rol_icon == null)
						{
							Image image = new Image();
							BitmapImage bitmap = new BitmapImage();
							bitmap.BeginInit();
							bitmap.CacheOption = BitmapCacheOption.OnLoad;
							bitmap.UriSource = uriRolKey;
							bitmap.EndInit();
							bitmap.Freeze();
							image.Source = bitmap;
							image.Width = ancho;
							image.Height = alto;
							//image.Margin = new Thickness(p.XMap, p.YMap, 0.0, 0.0);
							image.Margin = new Thickness(25, 10, 0, 0.0);
							image.RenderTransform = new RotateTransform(albionRotationAngle);
							//p.addImageIcon(image);
							image.Name = "rolicon_" + p.Id.ToString();
							p.Grid.Children.Add(image);
							Canvas.SetZIndex(image, 20);
						}

					}


				}
				else
				{
					clearById(p.Grid, "rolicon_" + p.Id.ToString());
				}
				//canvas.Children.Add(CEllipseFill(6.0, 6.0, p.X, p.Y, Brushes.Red));
				//p.addIcon(CEllipseFill(6.0, 6.0, p.XMap, p.YMap, Brushes.Red, "player_"+p.Id.ToString()));





				if (Settings.ShowEquip && isAllyG == false && isAllyA == false)
				{
					var lineas = "";
					foreach (var itemIndex in p.ItemIndexList)
					{
						try
						{
							ItemObject item = ItemInfo.itemDictionary[itemIndex.ToString()];
							if (item.TierAbsoluto > 0)
								lineas += item.Text(lang.GetLang()).ToUpper() + "\n";

						}
						catch (Exception ex)
						{

						}
					}

					var imtemList = Text(p.XMap, p.YMap, lineas, Colors.Red);
					imtemList.RenderTransform = new RotateTransform(albionRotationAngle);
					FrameworkElement itemlist_element = (FrameworkElement)LogicalTreeHelper.FindLogicalNode(canvas, "itemlist_" + p.Id.ToString());
					if (itemlist_element == null)
					{
						imtemList.Name = "itemlist_" + p.Id.ToString();
						canvas.Children.Add(imtemList);
					}
					else
					{
						moveTo(itemlist_element, p.XMap, p.YMap);
					}


				}
				else
				{
					clearById(canvas, "itemlist_" + p.Id.ToString());

				}

			
				//FrameworkElement player_dot = (FrameworkElement)LogicalTreeHelper.FindLogicalNode(canvas, element_id);
				
				if ((eHandler.LocalPlayer.Flag != 0 && eHandler.LocalPlayer.Flag != p.Flag && p.Flag != 0) || p.isPK == 255) {
					
					
				}
				else { 
				if (isAllyG == false && isAllyA == false)
				{
					try
					{
						p.Grid.Children.Add(CEllipse(10.5, 10.5, 0, 0, Brushes.Red, element_id));
					}
					catch { }




					if (p.sonar)
					{


						if (Settings.SoundChars)
							soundPlayer.Play();
						p.sonar = false;
					}


				}
				else if (isAllyG == true || isAllyG == true)
				{
					try
					{
						p.Grid.Children.Add(CEllipse(10.5, 10.5, 0, 0, Brushes.Green, element_id));
					}
					catch { }



				}
				
				}
				FrameworkElement player_in_canva = (FrameworkElement)LogicalTreeHelper.FindLogicalNode(canvas, element_id);
				if (player_in_canva == null)
				{
					p.Grid.Margin = new Thickness(p.XMap - p.Grid.ActualHeight / 2, p.YMap - p.Grid.ActualWidth / 2, 0.0, 0.0);
					//p.Grid.Background = Brushes.AliceBlue;
					canvas.Children.Add(p.Grid);
				}



			}
			

			List<Mob> mobs = new List<Mob>();
			try
			{
				lock (eHandler.Mobs)
				{
					mobs = eHandler.Mobs.Values.ToList();
				}
			}
			catch { }
			
			
			foreach (Mob i in mobs)
			{

				string element_id = "mob_" + i.Id.ToString();
				if (i.Remove)
				{
					clearById(canvas, element_id);
					eHandler.RemoveEntity(i.Id);
					continue;
				}

				Grid element = (Grid)LogicalTreeHelper.FindLogicalNode(canvas, element_id);
				if(i.MobInfo == null)
                {
					continue;
                }

				string type = i.MobInfo.MobType.ToString();
				bool ignorar = false;
				switch (type)
				{
					case "SKINNABLE":
						ignorar = !Settings.ShowSkinables;
						break;
					case "HARVESTABLE":
						ignorar = !Settings.ShowHarvestables;
						break;
					case "RESOURCE":
						ignorar = !Settings.ShowTreasures;
						break;
					case "OTHER":
						ignorar = !Settings.ShowOthers;
						break;

				}
				if (ignorar) { clearById(canvas, element_id); continue; }

				if (checkTierEnabled(i.MobInfo.Tier))
				{
					i.setMapCoords(fixedWidth, fixedHeight, scale, eHandler.LocalPlayer.X, eHandler.LocalPlayer.Y);
					if (element != null)
					{
						moveTo(element, i.XMap, i.YMap);
						continue;
					}

					i.Grid = new Grid();
					i.Grid.Height = 6;
					i.Grid.Width = 6;





					float mobX = fixedWidth / 2f + scale * (-1f * i.X + eHandler.LocalPlayer.X);
					float mobY = fixedHeight / 2f + scale * (i.Y - eHandler.LocalPlayer.Y);
					//string texto = i.MobInfo.MobType.ToString() + ", " + i.MobInfo.HarvestableMobType.ToString() + ", " + i.MobInfo.Tier;
					//string texto = i.Id + "\n" + i.X + " - " + i.Y + "\n" + mobX + " - " + mobY;
					//text = Text(mobX, mobY, texto, Colors.Blue);
					//text.RenderTransform = new RotateTransform(albionRotationAngle);
					//canvas.Children.Add(text);

					///

					///
					
					if (i.MobInfo.MobType == MobType.OTHER)
					{
						i.Grid.Children.Add(CEllipse(10.0, 10.0, 0, 0, Brushes.Black, "mob_" + i.Id.ToString()));

					}
					else if (i.MobInfo.MobType == MobType.RESOURCE)
					{
						i.Grid.Children.Add(CEllipse(10.0, 10.0, 0, 0, Brushes.Yellow, "mob_" + i.Id.ToString()));
						i.Grid.Children.Add(CEllipseFill(8.0, 8.0, 0,0, Brushes.Black, "mob_" + i.Id.ToString()));
					}
					else
					{

						if (i.MobInfo.Tier > 6)
						{
							if (i.MobInfo.HarvestableMobType.ToString().Contains("SMALL"))
							{
								i.Grid.Children.Add(CEllipse(10.0, 10.0, 0, 0, Brushes.GreenYellow, "mob_" + i.Id.ToString()));

							}
							else if (i.MobInfo.HarvestableMobType.ToString().Contains("BIG"))
							{
								i.Grid.Children.Add(CEllipse(13.0, 13.0, 0, 0, Brushes.GreenYellow, "mob_" + i.Id.ToString()));

							}
							else
							{
								i.Grid.Children.Add(CEllipseFill(13.0 ,13.0, 0, 0, Brushes.GreenYellow, "mob_" + i.Id.ToString()));

							}


						}
						else
						{
							var texto2 = i.MobInfo.HarvestableMobType;
							var texto3 = i.MobInfo.MobType;

							var texto4 = i.MobInfo.getMapStringInfo();
							//var elipse = CEllipseFill(13.0, 13.0, mobX, mobY, i.harvestBrushes[i.MobInfo.Tier]);

							//canvas.Children.Add(CEllipseFill(13.0, 13.0, mobX, mobY, i.harvestBrushes[i.MobInfo.Tier]));
							
							i.Grid.Children.Add(TextEllipse(15, 15, 0, 0, i.harvestBrushes[i.MobInfo.Tier], texto4,  i.fontPerColor[i.MobInfo.Tier], i.chargesBrushes[(int)(i.EnchantmentLevel)]));
							//text = Text(mobX + 8, mobY + 1, texto4, i.fontPerColor[i.MobInfo.Tier], true);

							//canvas.Children.Add(text);

							//text.RenderTransform = new RotateTransform(albionRotationAngle);


						}


					}

					i.Grid.Margin = new Thickness(i.XMap, i.YMap, 0.0, 0.0);
					i.Grid.Name = element_id;
					canvas.Children.Add(i.Grid);


				}
				else
				{
					if (element != null)
						clearById(canvas, element_id);
				}
			}

				List<FishSpot> fishSpots = new List<FishSpot>();
			lock (eHandler.FishSpots)
			{
				fishSpots = eHandler.FishSpots.ToList();
			}
			foreach(FishSpot fishSpot in fishSpots)
            {
				string element_id = "fish_" + fishSpot.Id.ToString();
				if (!Settings.ShowFish) { clearById(canvas, element_id); continue; }
				float fX = fixedWidth / 2f + scale * (-1f * fishSpot.X + eHandler.LocalPlayer.X);
				float fY = fixedHeight / 2f + scale * (fishSpot.Y - eHandler.LocalPlayer.Y);

				if (fishSpot.Remove)
				{
					clearById(canvas, element_id);
					eHandler.RemoveEntity(fishSpot.Id);
					continue;
				}
				fishSpot.setMapCoords(fixedWidth, fixedHeight, scale, eHandler.LocalPlayer.X, eHandler.LocalPlayer.Y);
				Grid element = (Grid)LogicalTreeHelper.FindLogicalNode(canvas, element_id);
				if (element != null)
				{
					moveTo(element, fishSpot.XMap, fishSpot.YMap);
					continue;
				}

				fishSpot.Grid = new Grid();


				try
				{
					string a = fishSpot.A.ToString();
					string b = fishSpot.B.ToString();
					string c = fishSpot.C.ToString();
					//Font fuente = new Font("Verdana", 7, FontStyle.Bold);
					if (c != "")
						fishSpot.Grid.Children.Add(TextEllipse(13.0, 13.0, 0, 0, Brushes.RoyalBlue, fishSpot.A.ToString(),Colors.White));
					
					else
						fishSpot.Grid.Children.Add(CEllipse(13.0, 13.0, 0, 0, Brushes.LightSteelBlue, "fish_" +fishSpot.Id.ToString()));


					var mensaje = a;

					// Translate to the point where you want the text
 					

					fishSpot.Grid.Margin = new Thickness(fishSpot.XMap, fishSpot.YMap, 0.0, 0.0);
					fishSpot.Grid.Name = element_id;
					canvas.Children.Add(fishSpot.Grid);

				}
				catch (Exception e)
				{
				}

			}

			List<Harvestable> harvestables = null;
			try
			{
				lock (eHandler.Harvestables)
				{
					harvestables = eHandler.Harvestables.Values.ToList();
				}
			}
			catch { }

			try
			{
				foreach (Harvestable h in harvestables)
				{
					string element_id = "harvestable_" + h.Id.ToString();
					Grid element = (Grid)LogicalTreeHelper.FindLogicalNode(canvas, element_id);
					if (h.Remove)
					{
						clearById(canvas, element_id);
						eHandler.RemoveEntity(h.Id);
						continue;
					}

					string iconStringKey = "";
					var ignorar = false;
					switch (h.getMapInfo())
					{
						case "W":
							iconStringKey = "T" + h.Tier + "_WOOD@" + h.Charges + ".png";
							ignorar = !Settings.ShowWood;
							break;
						case "R":
							iconStringKey = "T" + h.Tier + "_ROCK@" + h.Charges + ".png";
							ignorar = !Settings.ShowRock;
							break;
						case "F":
							iconStringKey = "T" + h.Tier + "_FIBER@" + h.Charges + ".png";
							ignorar = !Settings.ShowFiber;
							break;
						case "O":
							iconStringKey = "T" + h.Tier + "_ORE@" + h.Charges + ".png";
							ignorar = !Settings.ShowOre;
							break;
						case "H":
							iconStringKey = "T" + h.Tier + "_HIDE@" + h.Charges + ".png";
							ignorar = !Settings.ShowHarvestables;
							break;

					}
					if (ignorar) { clearById(canvas, element_id); continue; }



					if (checkTierEnabled(h.Tier))
					{
						h.setMapCoords(fixedWidth, fixedHeight, scale, eHandler.LocalPlayer.X, eHandler.LocalPlayer.Y);
						if (element == null)
						{
							h.Grid = new Grid();
							h.Grid.Name = element_id;



							
							
								if (h.Charges > 0 && h.Size > 0)
									h.Grid.Children.Add(TextEllipse(15, 15, 0, 0, h.harvestBrushes[h.Tier], h.getMapInfo(), h.fontPerColor[h.Tier], h.chargesBrushes[h.Charges]));
								else if (h.Size > 0)
								{
									h.Grid.Children.Add(TextEllipse(13.0, 13.0, 0, 0, h.harvestBrushes[h.Tier], h.getMapInfo(), h.fontPerColor[h.Tier]));
								}
								//canvas.Children.Add(CEllipseFill(13.0, 13.0, hX, hY, h.harvestBrushes[h.Tier]));
								//text = Text(hX + 8, hY +1, h.getMapInfo(), h.fontPerColor[h.Tier], true) ;//
								//text.RenderTransform = new RotateTransform(albionRotationAngle);
								//canvas.Children.Add(text);

								h.Grid.Margin = new Thickness(h.XMap, h.YMap, 0.0, 0.0);
								canvas.Children.Add(h.Grid);
							



						}
						else
						{
							moveTo(element, h.XMap, h.YMap);
						}

					}
					else
					{
						if (element != null)
							clearById(canvas, element_id);
					}


				}
			}
			catch { }
			albionRotationAngle = -135;
			canvas.RenderTransform = new RotateTransform(albionRotationAngle, fixedWidth / 2f, fixedHeight / 2f);
		}


		private bool checkTierEnabled(long tier)
		{
			switch (tier)
			{
				case 1:
					return Settings.OpcT1;
					break;
				case 2:
					return Settings.OpcT2;
					break;
				case 3:
					return Settings.OpcT3;
					break;
				case 4:
					return Settings.OpcT4;
					break;
				case 5:
					return Settings.OpcT5;
					break;
				case 6:
					return Settings.OpcT6;
					break;
				case 7:
					return Settings.OpcT7;
					break;
				case 8:
					return Settings.OpcT8;
					break;
			}
			return false;
		}


		private void __clearEntitys(object sender, HotkeyEventArgs e)
        {
			eHandler.EntityLeaveAll();
		}
		private void __alternarRadar(object sender, HotkeyEventArgs e)
        {
			dibujar = !dibujar;
		}
		private void __abrirMenu(object sender, HotkeyEventArgs e)
        {

			try
			{
				if (menuWindow == null)
                {
					menuWindow = new __MenuWindow(lang);
					menuWindow.ShowInTaskbar = false;
					menuWindow.Owner = this;
					menuWindow.Topmost = true;
					ventanaActiva = true;

				}

				if (!menuWindow.IsActive)
                {
					menuWindow.Activate();
				}
				else
                {

					menuWindow.Close();
					menuWindow = null;
					return;
				}
				menuWindow.Show();


				//menuWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
			}
			catch (Exception ex)
            {
				Console.WriteLine(ex);
            }
		}



        private void WindowOnSizeChanged(object sender, SizeChangedEventArgs e)
		{
			Window window = (Window)sender;
			if (e.NewSize.Width > e.NewSize.Height)
			{
				window.Width = e.NewSize.Height;
				window.Height = e.NewSize.Height;
			}
			else
			{
				window.Width = e.NewSize.Width;
				window.Height = e.NewSize.Width;
			}
			Settings.Width = window.Width;
			Settings.Height = window.Height;

		}

		private void Window_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
				DragMove();
		}





       
        public void BackgroundThread2()
		{
			__MainClass.BTest(eHandler, DeviceSelected, eHandler.Comunicador.namei()); 

		}


		public JToken Serialize(IConfiguration config)
		{
			JObject obj = new JObject();
			foreach (var child in config.GetChildren())
			{
				obj.Add(child.Key, Serialize(child));
			}

			if (!obj.HasValues && config is IConfigurationSection section)
				return new JValue(section.Value);

			return obj;
		}

		private ICaptureDevice SelectPacketDevice()
		{
			

			while (true)
            {
				List<ICaptureDevice> devices = CaptureDeviceList.Instance.ToList();
				if (Settings.SniffingDevice != null && devices.Any((ICaptureDevice x) => x.Description == Settings.SniffingDevice))
				{
					return devices.First((ICaptureDevice x) => x.Description == Settings.SniffingDevice);
				}
				for (int i = 0; i < devices.Count; i++)
				{
					ICaptureDevice device = devices[i];
					Console.WriteLine("{0} - {1}", i, device.Description);
				}
				Console.Write("Please select a device from the list above by typing its corresponding number : ");
				var deviceIndexResponse = Console.ReadLine();
				var deviceIndex = -1;
				if (int.TryParse(deviceIndexResponse, out deviceIndex))
				{
					if (deviceIndex > -1 && deviceIndex < devices.Count)
					{
						var selectedDevice = devices[deviceIndex];
						Settings.SniffingDevice = selectedDevice.Description;
						return selectedDevice;
					}

					Console.WriteLine("Incorrect device index supplied");
				}
				else
				{
					Console.WriteLine("Non integet value specified");
				}
			}

		}

		//public WriteableBitmap CreateImageSource2(string path)
		//{
		//	WriteableBitmap writeableBmp = BitmapFactory.New(64, 64);
		//	using (writeableBmp.GetBitmapContext())
		//	{

		//		var currentpath = Directory.GetCurrentDirectory();
		//		// Load an image from the calling Assembly's resources via the relative path
		//		writeableBmp = BitmapFactory.New(1, 1).FromResource(path);

		//		// Clear the WriteableBitmap with white color
		//		writeableBmp.Clear(Colors.White);

		//		// Green line from P1(1, 2) to P2(30, 40)
		//		//writeableBmp.DrawLine(1, 2, 30, 40, Colors.Green);

		//		// Line from P1(1, 2) to P2(30, 40) using the fastest draw line method 
		//		int w = writeableBmp.PixelWidth;
		//		int h = writeableBmp.PixelHeight;


		//		var bytesPerPixel = (writeableBmp.Format.BitsPerPixel + 7) / 8;
		//		var stride = w * bytesPerPixel;
		//		var bufferSize = h * stride;
		//		int[] pixels = new int[bufferSize];

		//		writeableBmp.CopyPixels(pixels, stride, 0);



		//	} // Invalidate and present in the Dispose call
		//	return writeableBmp;
		//	// Take snapshot




		//}


		private void moveTo(Grid grid, float x, float y)
		{
			try
			{
				((Grid)grid).Margin = new Thickness(x, y, 0, 0);
			}
			catch 
			{ 
			}
		}
		private void moveTo(FrameworkElement ellipse, float x, float y)
		{
			try
			{
				((FrameworkElement)ellipse).Margin = new Thickness(x, y, 0, 0);

			}
			catch { }
		}
	

		private void clearById(Panel canvas, string name)
        {
            try
			{ 
				var circle = (UIElement)LogicalTreeHelper.FindLogicalNode(canvas, name);
				if(circle!=null)
					canvas.Children.Remove(circle);
            } catch { }

		}










		private Tuple<float, float, float> getFixedWindowSize(double windowWidht,double windowHeight)
		{
			//MainWindow mainWindows = (MainWindow)(object)canvas.Parent;
			//double windowWidht = canvas.ActualWidth;
			//double windowHeight = canvas.ActualHeight;
			//double windowWidht = mainWindows.ActualWidth;
			//double windowHeight = mainWindows.ActualHeight;

			float centerX = Convert.ToSingle(windowWidht / 2.0);
			float centerY = Convert.ToSingle(windowHeight / 2.0);
			float width = Convert.ToSingle(windowWidht);
			float height = Convert.ToSingle(windowHeight);
			float scale = width * 0.008f;
			if (height > width)
			{
				height = Convert.ToSingle(windowHeight - (windowHeight - windowWidht));
				width = Convert.ToSingle(height);
			}
			else
			{
				height = Convert.ToSingle(windowWidht - (windowWidht - windowHeight));
				width = height;
			}
			//FixedHeight = height;
			//FixedWidth = width;
			//SCALE = scale;
			return Tuple.Create(width, height, scale);
		}



		
		private double Distance(float x1, float x2, float y1, float y2)
		{
			return Math.Sqrt(Math.Pow(x1 - x2, 2.0) + Math.Pow(y1 - y2, 2.0)) / 4.0;
		}

		private TextBlock Text(double x, double y, string text, Color color)
        {
			return Text(x, y, text, color, false);

		}
		private TextBlock Text(double x, double y, string text, Color color, bool negrita)
		{
			var fw = FontWeights.Regular;

			if (negrita)
				fw = FontWeights.Bold;

			return new TextBlock
			{
				Text = text,
				FontWeight = FontWeights.Regular,
				Foreground = new SolidColorBrush(color),
				FontFamily = new FontFamily("Verdana"),
				FontSize = 10.0,
				
				Margin = new Thickness(x, y, 0.0, 0.0)
			};
		}



		private Ellipse CEllipse(double width, double height, double desiredCenterX, double desiredCenterY, string name)
		{
			return CEllipse(width, height, desiredCenterX, desiredCenterY, Brushes.Black, name);
		}

		private Ellipse CEllipseFill(double width, double height, double desiredCenterX, double desiredCenterY, Brush color, string name)
        {
			Ellipse obj = new Ellipse
			{
				Width = width,
				Height = height
			};
			double left = desiredCenterX - width / 2.0;
			double top = desiredCenterY - height / 2.0;
			new SolidColorBrush();
			obj.StrokeThickness = 2.0;
			obj.Fill = color;
			obj.Margin = new Thickness(left, top, 0.0, 0.0);
			obj.Name = name;
			return obj;
		}

		private Grid playerGrid(double desiredCenterX, double desiredCenterY)
        {
			double width = 6;
			double height = 6;
			var grid = new Grid();

			return grid;

		}


		private Grid GridEllipse(double width, double height, double desiredCenterX, double desiredCenterY, Brush color)
        {
			var grid = new Grid();



			Ellipse ell = new Ellipse
			{
				Width = width,
				Height = height,
				StrokeThickness = 4.0,
				Fill = color
			};

			grid.Children.Add(ell);


			double left = desiredCenterX - width * - 1 / 2.0;
			double top = desiredCenterY - height * -1 / 2.0;
			grid.Margin = new Thickness(left, top, 0.0, 0.0);

			return grid;

		}

		private Grid TextEllipse(double width, double height, double desiredCenterX, double desiredCenterY, Brush color)
        {
			return TextEllipse(width, height, desiredCenterX, desiredCenterY, color, "", Colors.Transparent);

		}
		private Grid TextEllipse(double width, double height, double desiredCenterX, double desiredCenterY, Brush color, string text, Color textColor)
		{
			return TextEllipse(width, height, desiredCenterX, desiredCenterY, color, text, textColor,Brushes.Transparent);

		}
		private Grid TextEllipse(double width, double height, double desiredCenterX, double desiredCenterY, Brush color, string text, Color textColor, Brush enchantBrush)

		{
			int albionRotationAngle = 135;

			var grid = new Grid();
			

			Ellipse ell = new Ellipse
			{
				Width = width ,
				Height = height ,
				StrokeThickness = 2.5,
				Stroke = enchantBrush,
				Fill = color
			};

			grid.Children.Add(ell);


			var tb = new TextBlock
			{
				Text = text,
				FontWeight = FontWeights.Regular,
				Foreground = new SolidColorBrush(textColor),
				FontFamily = new FontFamily("Verdana"),
				FontSize = 10.0,
				HorizontalAlignment = HorizontalAlignment.Center,
				TextAlignment = TextAlignment.Center,
				VerticalAlignment = VerticalAlignment.Center,
				Margin = new Thickness(25, 14, 0.0, 0.0)
				//Margin = new Thickness(leftText, topText, 0.0, 0.0)
			};

			tb.RenderTransform = new RotateTransform(albionRotationAngle);

			grid.Children.Add(tb);
			double left = desiredCenterX - width * +1 / 2.0;
			double top = desiredCenterY - height * +1 / 2.0;
			grid.Margin = new Thickness(desiredCenterX - width, desiredCenterY - height, 0.0, 0.0);


			return grid;

		}
		private Ellipse CEllipse(double width, double height, double desiredCenterX, double desiredCenterY, Brush color, string name)
		{


			Ellipse obj = new Ellipse
			{
				Width = width,
				Height = height
			};
			double left = desiredCenterX - width / 2.0;
			double top = desiredCenterY - height / 2.0;
			new SolidColorBrush();
			obj.StrokeThickness = 2.0;
			obj.Stroke = color;
			obj.Margin = new Thickness(left, top, 0.0, 0.0);
			return obj;
		}






	}
}
