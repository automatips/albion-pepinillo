del PublicRelease\*.dll
del PublicRelease\*.exe
del PublicRelease\*.txt
del PublicRelease\*.json
del PublicRelease\images\*.png


xcopy Release\netcoreapp3.1\publish\Map.exe PublicRelease\


xcopy Release\netcoreapp3.1\publish\AOC.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Leaf.xNet.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Castle.Core.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Config.Net.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Config.Net.Json.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\DeviceId.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Map.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Microsoft.DotNet.PlatformAbstractions.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Microsoft.Extensions.Configuration.Abstractions.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Microsoft.Extensions.Configuration.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Microsoft.Extensions.Configuration.FileExtensions.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Microsoft.Extensions.Configuration.Json.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Microsoft.Extensions.FileProviders.Abstractions.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Microsoft.Extensions.FileProviders.Physical.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Microsoft.Extensions.FileSystemGlobbing.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Microsoft.Extensions.Primitives.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\Newtonsoft.Json.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\NHotkey.Wpf.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\NHotkey.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\PacketDotNet.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\RadialMenu.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\SharpPcap.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\System.Management.dll PublicRelease\
xcopy Release\netcoreapp3.1\publish\System.Text.Json.dll PublicRelease\

xcopy Release\netcoreapp3.1\items.json PublicRelease\
xcopy Release\netcoreapp3.1\mobs.json PublicRelease\
xcopy Release\netcoreapp3.1\world.json PublicRelease\

xcopy Release\netcoreapp3.1\publish\Map.deps.json PublicRelease\
xcopy Release\netcoreapp3.1\publish\Map.runtimeconfig.json PublicRelease\


mkdir PublicRelease\images
xcopy Release\netcoreapp3.1\images\* PublicRelease\images\


certutil -hashfile PublicRelease\Map.exe MD5 | find /v "hashfile comando completado correctamente." >> PublicRelease\hashes.txt
for %%H in (PublicRelease\*.dll) do @certutil -hashfile %%H MD5 | find /v "hashfile comando completado correctamente." >> PublicRelease\hashes.txt
for %%G in (PublicRelease\*.json) do @certutil -hashfile %%G MD5 | find /v "hashfile comando completado correctamente." >> PublicRelease\hashes.txt
for %%I in (PublicRelease\images\*.png) do @certutil -hashfile %%I MD5 | find /v "hashfile comando completado correctamente." >> PublicRelease\hashes.txt
