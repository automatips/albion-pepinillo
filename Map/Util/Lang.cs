﻿using Config.Net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Map.Util
{
    public class Lang
    {
        internal Dictionary<string, List<string>> data;
        private __IMySettings Settings;


        private int curr { get; set; }

        public Lang(int curr) 
        {
            var PATH = Process.GetCurrentProcess().MainModule.FileName.Replace("\\Map.exe", "");
            Settings = new ConfigurationBuilder<__IMySettings>().UseJsonFile(PATH + "\\appsettings.json").Build();
            if(Settings.Lang != 0 && Settings.Lang != null)
                SetLang((int)Settings.Lang);
          
            Reload();

        }
        public void Reload()
        {
            data = new Dictionary<string, List<string>>();
            loadStatic2DotsPData();
        }

        public void SetLang(int lang)
        {
            curr = lang;
            Settings.Lang =lang;

        }
        public int GetLang()
        {
            return curr;
        }

        //0 es
        //1 pt
        //2 en
        private void addText(string key, string es, string pt, string en)
        {
            data.Add(key,new List<string>() {es,pt,en });
        }

        private void loadStatic2DotsPData()
        {
            addText("lang_menu","Idioma","Lingua", "Language");
            addText("tier_menu", "Filtro de Tier", "Filtro de Tier", "Tier Filter");
            addText("players_menu", "Personajes", "Personagens", "Players");
            addText("players_menu_show", "Mostrar\nPersonajes", "Mostrar\nPersonagens", "Show\nPlayers");
            addText("players_menu_sound", "Sonido\nAlerta", "Alerta\nsonoro", "Alert\nSound");
            addText("players_menu_equip", "Mostrar\nEquipo", "Mostrar\nequipamento", "Show\nGear");
            addText("players_menu_rol", "Detectar\nRol", "Detectar\nRol", "Rol\nDetection");
            
            
            addText("ress_menu", "Recursos", "Recursos", "Rescources");
            addText("ress_menu_fiber", "Fibra", "Fibra", "Fiber");
            addText("ress_menu_wood", "Madera", "Madeira", "Wood");
            addText("ress_menu_rock", "Piedra", "Rocha", "Rock");
            addText("ress_menu_ore", "Mineral", "Mineral", "Ore");
            addText("ress_menu_fish", "Pezcao", "peixe", "Fish");
            addText("ress_menu_treasures", "Tesoros", "Tesouros", "Treasures");

            addText("mobs_menu", "Npc", "Npc", "Npc");
            addText("mobs_menu_skin", "Despellejables", "Descascável", "Skinnables");
            addText("mobs_menu_harvest", "Cosechable", "Colhível", "Harvestable");
            addText("mobs_menu_other", "Otros", "Npc", "Npc");
            
            
            addText("graph_menu", "Video", "Video", "Video");
            addText("graph_menu_icons", "Mostrar Iconos", "Mostrar Iconos", "Show Icons");

        }

        public string getString(string key)
        {
            return data[key][curr];

        }





        





    }
}
