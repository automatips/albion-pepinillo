﻿using AOC.Network.Packets.Implemented.Event;
using Map.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net.Http;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Map
{

    class ImageUtils
    {
        public static System.Drawing.Bitmap ImageSourceToBitmap(ImageSource imageSource)
        {
            BitmapSource m = (BitmapSource)imageSource;

            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(m.PixelWidth, m.PixelHeight, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);

            System.Drawing.Imaging.BitmapData data = bmp.LockBits(
            new System.Drawing.Rectangle(System.Drawing.Point.Empty, bmp.Size), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);

            m.CopyPixels(Int32Rect.Empty, data.Scan0, data.Height * data.Stride, data.Stride);
            bmp.UnlockBits(data);
            bmp.MakeTransparent();
            return bmp;
        }
    }
    class Extra
    {
        public static string Encode(string p)
        {
            return System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(p));
        }

    }
    class MobInfoLoad
    {
        [JsonProperty("Index")]
        public int Index { get; set; }

        [JsonProperty("Tier")]
        public int Tier { get; set; }

        [JsonProperty("UniqueName")]
        public string UniqueName { get; set; }

        [JsonProperty("Harvestable")]
        public Dictionary<string, string> Harvestable { get; set; }
    };


    class MapInfoLoad
    {
        [JsonProperty("Index")]
        public string Index;
        [JsonProperty("UniqueName")]
        public string UniqueName;
        [JsonProperty("Type")]
        public string Type;
        [JsonProperty("Enabled")]
        public bool Enabled;
    }


    class Utils
    {
        //[Obfuscation(Exclude = true, ApplyToMembers = true)]

        private static async System.Threading.Tasks.Task valAsync(__Join join)
        {
            HttpClient client = new HttpClient();
            var userName = Environment.UserName; // User name of user
            var hostName = Environment.MachineName;// Machine name
            var jugador = join.Jugador;
            var mapa = join.IdMap;
            try { mapa = MapInfo.getMapInfo(join.IdMap).UniqueName; } catch { }

            Dictionary<string, string> values = new Dictionary<string, string>
            {
                { "p1", jugador },
                { "p2", hostName },
                { "p3", userName},
                { "p4", mapa }
            };
            Dictionary<string, string> au = new Dictionary<string, string>
            {
                { "p1", jugador },
                { "p2", hostName },
            };

            var SERVER2 = "localhost:8080";
            bool rta = false;
            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync("http://" + SERVER2 + "/", content);
            var responseString = await response.Content.ReadAsStringAsync();

        }


        public static void LoadAlliesData(Dictionary<string, List<string>> dict)
        {
            var PATH = Process.GetCurrentProcess().MainModule.FileName.Replace("\\Map.exe", "");
            List<string> guild_allies = new List<string>();
            List<string> alliances_allies = new List<string>();
            foreach (string g in System.IO.File.ReadLines(PATH + "\\guild_allies.txt").ToList())
            {
                guild_allies.Add(g.ToLower());
            }
            foreach (string a in System.IO.File.ReadLines(PATH + "\\alliance_allies.txt").ToList())
            {
                alliances_allies.Add(a.ToLower());
            }
            dict.Add("guild_allies", guild_allies);
            dict.Add("alliance_allies", alliances_allies);


        }

        public static void LoadItems()
        {
            var PATH = Process.GetCurrentProcess().MainModule.FileName.Replace("\\Map.exe", "");

            Console.WriteLine("loading data");
            var basepath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly()!.Location);
            string contents = File.ReadAllText(PATH + "\\items.json");



            List<ItemObject> itemlist = JsonConvert.DeserializeObject<List<ItemObject>>(contents);
            foreach (ItemObject i in itemlist)
            {
                i.Inicializar();
                ItemInfo.itemDictionary.Add(i.Index, i);

            }

        }


        public static void LoadMaps()
        {
            var PATH = Process.GetCurrentProcess().MainModule.FileName.Replace("\\Map.exe", "");

            Console.WriteLine("loading data");
            var basepath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly()!.Location);
            string contents = File.ReadAllText(PATH + "\\world.json");
            List<MapInfoLoad> maplist = JsonConvert.DeserializeObject<List<MapInfoLoad>>(contents);
            foreach (MapInfoLoad ml in maplist)
            {
                MapInfo m = new MapInfo(ml.Index, ml.UniqueName, ml.Type, ml.Enabled);
                MapInfo.mapInfo.Add(m);
            }

        }
        public static void LoadMobs()
        {

            Console.WriteLine("loading data");
            var PATH = Process.GetCurrentProcess().MainModule.FileName.Replace("\\Map.exe", "");


            var basepath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly()!.Location);
            string contents = File.ReadAllText(PATH + "\\mobs.json");

            var olist = JsonConvert.DeserializeObject<List<MobInfoLoad>>(contents);
            //JArray olist = JArray.Parse(File.ReadAllText(json_file));
            int index = 0;
            foreach (MobInfoLoad o in olist)
            {
                var id = index;
                if (id == 73)
                {

                }
                Byte tier = (byte)o.Tier;
                var t = o.Index;
                if (o.Harvestable.Count > 0)
                {

                    var harvestableFirst = o.Harvestable.First();
                    MobType mtype = MobType.HARVESTABLE;
                    HarvestableMobType htype = new HarvestableMobType();
                    if (harvestableFirst.Key.Contains("HIDE"))
                        mtype = MobType.SKINNABLE;
                    else if (harvestableFirst.Key.Contains("ROCK"))
                        htype = HarvestableMobType.HIGHLAND;
                    else if (harvestableFirst.Key.Contains("FIBER"))
                        htype = HarvestableMobType.SWAMP;
                    //else if (harvestableFirst.Key.Contains("STEPPE"))
                    //htype = HarvestableMobType.STEPPE;
                    else if (harvestableFirst.Key.Contains("ORE"))
                        htype = HarvestableMobType.MOUNTAIN;
                    else if (harvestableFirst.Key.Contains("WOOD"))
                        htype = HarvestableMobType.FOREST;
                    else if (harvestableFirst.Key.Contains("ESSENCE"))
                        htype = HarvestableMobType.ESSENCE;
                    else if (harvestableFirst.Key.Contains("LOOT_CHEST"))
                        mtype = MobType.RESOURCE;
                    else if (harvestableFirst.Key.Contains("ROAMING"))
                        mtype = MobType.OTHER;
                    else if (harvestableFirst.Key.Contains("TRASH"))
                        mtype = MobType.OTHER;
                    else
                        mtype = MobType.OTHER;

                    if (mtype == MobType.SKINNABLE || mtype == MobType.OTHER || mtype == MobType.RESOURCE)
                        MobInfo.mobsInfo.Add(new MobInfo(id, tier, mtype));
                    else
                        MobInfo.mobsInfo.Add(new MobInfo(id, tier, mtype, htype));


                }
                else
                    MobInfo.mobsInfo.Add(new MobInfo(id, tier, MobType.OTHER));

                index = index + 1;

            }


        }

        internal static string GetMobo()
        {
            ManagementObjectSearcher searcher =
            new ManagementObjectSearcher("SELECT SerialNumber FROM Win32_BaseBoard");

            ManagementObjectCollection information = searcher.Get();
            string r = "";
            foreach (ManagementObject obj in information)
            {
                foreach (PropertyData data in obj.Properties)
                    r = data.Value.ToString();
                    //Console.WriteLine("{0} = {1}", data.Name, data.Value);
            }

            searcher.Dispose();

            return r;
        }
    }
}
