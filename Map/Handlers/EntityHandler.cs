﻿using Map.Model;
using Map.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;

namespace Map.Handlers
{
    class EntityHandler
	{
		private Dictionary<long, Player> players;
		private Dictionary<long, Harvestable> harvestables;
		private Dictionary<long, Mob> mobs;
		private List<FishSpot> fishSpots;
		private List<Dungeon> dungeons;
		//private List<Entity> entities;

        //public __IMySettings Settings { get; }

        private Player localPlayer;
		private Comunicador comunicador;
        private SoundPlayer soundPlayer;

		public Dictionary<long, Mob> Mobs { get => mobs; set => mobs = value; }
		public Dictionary<long, Player> Players { get => players; set => players = value; }
		public Dictionary<long, Harvestable> Harvestables { get => harvestables; set => harvestables = value; }
		
		public List<FishSpot> FishSpots { get => fishSpots; set => fishSpots = value; }

		public List<Dungeon> Dungeons { get => dungeons; set => dungeons = value; }


		public Player LocalPlayer { get => localPlayer; set => localPlayer = value; }
		public Comunicador Comunicador { get => comunicador; set => comunicador = value; }

		public EntityHandler()
		{
			localPlayer = new Player();
			players = new Dictionary<long, Player>();
			mobs = new Dictionary<long, Mob>();
			harvestables = new Dictionary<long, Harvestable>();
			fishSpots = new List<FishSpot>();
			dungeons = new List<Dungeon>();
			//entities = new List<Entity>();
			//Settings = settings;
			//UnmanagedMemoryStream s = __Resource.ding;
			//soundPlayer = new SoundPlayer(s);
		}

		public void ClearEntities()
		{
			players.Clear();
			mobs.Clear();
			harvestables.Clear();
			dungeons.Clear();
			fishSpots.Clear();
			//entities.Clear();
		}

		public void moveEntity(AOC.Network.Packets.Implemented.Event.__MoveEvent move)
		{
			long Id = move.Id;
			float[] Loc = move.Loc;

			bool found = false;
			foreach (Mob mob in mobs.Values)
			{
				try
				{
					_ = mob;
					mobs.Values.ToList().ForEach(delegate (Mob p)
					{
						if (p.Id == Id)
						{
							p.SetLoc(Loc);
							//p.Moved = true; //???
							found = true;
						}
					});
				}
				catch (Exception ex)
				{

				}


			}


			if (players.ContainsKey(Id))
			{
				players[Id].SetLoc(Loc);
				found = true;
			}





			//if (!found)
			//{


			//	entities.ForEach(delegate (Entity e)
			//	{
			//		if (e.Id == Id)
			//		{
			//			e.SetLoc(Loc);
			//			found = true;
			//		}
			//	});
			//	if (!found)
			//	{
			//		Entity ne = new Entity();
			//		ne.Id = Id;
			//		ne.SetLoc(Loc);
			//		entities.Add(ne);
			//	}
			//}
			//else
			//{
			//	try
			//	{
			//		entities.RemoveAll((Entity x) => x.Id == Id);
			//	}
			//	catch { }

			//}
		}

		internal void addHarvestable(Harvestable h)
		{
			if (!harvestables.ContainsKey(h.Id))
			{
				harvestables[h.Id] = h;
			}
			else
			{
				harvestables[h.Id] = h;
			}
		}

		public void EntityLeave(long id)
        {
			if (harvestables.ContainsKey(id)) { harvestables[id].Remove = true; }
			else if (mobs.ContainsKey(id)) { mobs[id].Remove = true; }
			else if (players.ContainsKey(id)) { players[id].Remove = true; }


			//if (entities.RemoveAll((Entity x) => x.Id == id) ) {  }
		}
		
		public void EntityLeaveAll()
        {
			dungeons.ToList().ForEach(x => x.Remove = true) ;
			fishSpots.ToList().ForEach(x => x.Remove = true);
			players.Values.ToList().ForEach(x => x.Remove = false) ;
			harvestables.Values.ToList().ForEach(x => x.Remove = true) ;
			mobs.Values.ToList().ForEach(x => x.Remove = true) ;

		}
		public bool RemoveEntity(long id)
		{
			
			if (players.ContainsKey(id)) { players.Remove(id); return true; }
			if (harvestables.ContainsKey(id)) { harvestables.Remove(id); return true; }
			if (mobs.ContainsKey(id)) { mobs.Remove(id); return true; }
			//if (entities.RemoveAll((Entity x) => x.Id == id) > 0) { return true; }

			return false;

		}



		internal void addPlayer(Player p)
		{

			//if (Settings.SoundChars)
			//	soundPlayer.Play();

			if (!players.ContainsKey(p.Id))
				players[p.Id] = p;
			else
				players[p.Id] = p;
		}




		internal void addMob(AOC.Network.Packets.Implemented.Event.__NewMob mob)
		{
			Mob i = new Mob();
			i.SetLoc(mob.LocB);
			i.Id = mob.Id;
			i.MobInfo = MobInfo.getMobInfo(mob.IdType);

			if (!players.ContainsKey(i.Id))
				mobs[i.Id] = i;
			else
				mobs[i.Id] = i;
		}


		internal void UpdateMobEnchantmentLevel(long mobId, float enchantmentLevel)
		{
			mobs[mobId].EnchantmentLevel = enchantmentLevel;
		}

		internal void HealthUpdate(object id, object nuevahp)
		{
			int retry = 2;
			while (true)
			{
				try
				{
					mobs.Values.ToList().ForEach(delegate (Mob mm)
					{
						if (mm.Id == int.Parse(id.ToString()))
						{
							mm.Health = int.Parse(nuevahp.ToString());
							if (mm.Health < 1)
							{
								EntityLeave(mm.Id);
							}
						}
					});
					break;
				}
				catch (InvalidOperationException)
				{
					retry--;
				}
				catch (Exception)
				{
					if (retry < 1)
					{
						return;
					}
				}
			}
		}
		

        internal void addFishSpot(AOC.Network.Packets.Implemented.Event.__NewFishingZoneObject newFishingZoneObject)
        {
			var fs = new FishSpot(
				newFishingZoneObject.Id,
				newFishingZoneObject.X,
				newFishingZoneObject.Y,
				newFishingZoneObject.Amount.ToString(),
				newFishingZoneObject.A,
				newFishingZoneObject.Type
				);
			FishSpots.Add(fs);

		}
        internal void addDungeon(AOC.Network.Packets.Implemented.Event.__NewRandomDungeonExit newRandomDungeonExit)
        {
			Dungeon d = new Dungeon(
				newRandomDungeonExit.Id,
				newRandomDungeonExit.X,
				newRandomDungeonExit.Y,
				newRandomDungeonExit.Name,
				newRandomDungeonExit.Bandera
				);
			dungeons.Add(d);
		}
    }


}
