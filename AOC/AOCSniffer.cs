﻿using AOC.Library.Configuration;
using AOC.Library.Util;
using AOC.Network.Core;
using AOC.Network.Data;
using AOC.Network.Packets.Implemented.Event;
using PacketDotNet;
using SharpPcap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

[Obfuscation(Exclude = true, ApplyToMembers = true)]
public class __AOCSniffer
{
	public delegate void opMoveHandler(object sender, __Move move);
	public delegate void opJoin(object sender, __Join join);
	
	public delegate void evJoinFinished(object sender, __JoinFinished ev);
	public delegate void evMobChangeState(object sender, __MobChangeState ev);
	
	public delegate void evNewRandomDungeonExit(object sender, __NewRandomDungeonExit ev);
	public delegate void evNewFishingZoneObject(object sender, __NewFishingZoneObject ev);
	public delegate void evHarvestFinished(object sender, __HarvestFinished ev);
	public delegate void evMoveHandler(object sender, __MoveEvent ev);
	public delegate void evNewCharacter(object sender, __NewCharacter ev);
	public delegate void evNewMob(object sender, __NewMob ev);
	public delegate void evUpdateFame(object sender, __UpdateFame ev);
	public delegate void evNewHarvestableObject(object sender, __NewHarvestableObject ev);
	public delegate void evNewSimpleHarvestableObjectList(object sender, __NewSimpleHarvestableObjectList ev);
	public delegate void evDied(object sender, __Died ev);
	public delegate void evLeave(object sender, __Leave ev);
	public delegate void evHealthUpdate(object sender, __HealthUpdate ev);

	//public delevate void evFishingMiniGame(object sender, Fishin ev);


	private readonly AOCPhotonParser photonParser;
	
	public event opMoveHandler Move;
	public event opJoin Join;
	public event evJoinFinished JoinFinished;
	public event evMobChangeState MobChangeState;
	public event evNewRandomDungeonExit NewRandomDungeonExit;
	public event evNewFishingZoneObject NewFishingZoneObject;
	public event evHarvestFinished HarvestFinished;
	public event evMoveHandler MoveEvent;
	public event evNewCharacter NewCharacter;
	public event evNewMob NewMob;
	public event evUpdateFame UpdateFame;
	public event evNewHarvestableObject NewHarvestableObject;
	public event evNewSimpleHarvestableObjectList NewSimpleHarvestableObjectList;
	public event evDied Died;
	public event evLeave Leave;
	public event evHealthUpdate HealthUpdate;




	public __AOCSniffer(string k)
	{
		photonParser = new AOCPhotonParser(k);

	}

	



	public void AddRequestHandler2(Action<object, __Move> action)
	{

		photonParser.AddRequestHandler(OperationCodes.Move, delegate (__Move move)
		{
			this.Move(action, move);

		});

	}

	public void Subscribe2()
	{

		photonParser.AddEventHandler(EventCodes.NewRandomDungeonExit, delegate (__NewRandomDungeonExit ev)
	
		{
			this.NewRandomDungeonExit(this, ev);
		});


	}


	public void __Subscribe()
	{
		photonParser.AddRequestHandler(OperationCodes.Move, delegate (__Move move)
		{
			this.Move(this, move);
		});
		photonParser.AddEventHandler(EventCodes.JoinFinished, delegate (__JoinFinished ev)
		{
			this.JoinFinished(this, ev);
		});
		photonParser.AddResponseHandler(OperationCodes.Join, delegate (__Join join)
		{
			this.Join(this, join);
		});
		photonParser.AddEventHandler(EventCodes.MobChangeState, delegate (__MobChangeState ev)
		{
			this.MobChangeState(this, ev);
		});
		photonParser.AddEventHandler(EventCodes.NewRandomDungeonExit, delegate (__NewRandomDungeonExit ev)
		{
			this.NewRandomDungeonExit(this, ev);
		});
		photonParser.AddEventHandler(EventCodes.NewFishingZoneObject, delegate (__NewFishingZoneObject ev)
		{
			this.NewFishingZoneObject(this, ev);
		});
		photonParser.AddEventHandler(EventCodes.HarvestFinished, delegate (__HarvestFinished ev)
		{
			this.HarvestFinished(this, ev);
		});
		photonParser.AddEventHandler(EventCodes.Move, delegate (__MoveEvent entityMove)
		{
			this.MoveEvent(this, entityMove);
		});
		photonParser.AddEventHandler(EventCodes.NewCharacter, delegate (__NewCharacter character)
		{
			this.NewCharacter(this, character);
		});
		photonParser.AddEventHandler(EventCodes.NewMob, delegate (__NewMob mob)
		{
			this.NewMob(this, mob);
		});
		photonParser.AddEventHandler(EventCodes.UpdateFame, delegate (__UpdateFame ev)
		{
			this.UpdateFame(this, ev);
		});
		photonParser.AddEventHandler(EventCodes.NewHarvestableObject, delegate (__NewHarvestableObject harvestable)
		{
			this.NewHarvestableObject(this, harvestable);
		});
		photonParser.AddEventHandler(EventCodes.NewSimpleHarvestableObjectList, delegate (__NewSimpleHarvestableObjectList harvestableList)
		{
			this.NewSimpleHarvestableObjectList(this, harvestableList);
		});
		photonParser.AddEventHandler(EventCodes.Died, delegate (__Died died)
		{
			this.Died(this, died);
		});
		photonParser.AddEventHandler(EventCodes.Leave, delegate (__Leave leave)
		{
			this.Leave(this, leave);
		});
		photonParser.AddEventHandler(EventCodes.HealthUpdate, delegate (__HealthUpdate update)
		{
			this.HealthUpdate(this, update);
		});





	}
	public void __StartListening()
    {
		__StartListening(null);

	}


	public void __StartListening(ICaptureDevice device)
	{

		if(device == null)
        {
			//return;
			device = SelectPacketDevice();
		}
		
		device.OnPacketArrival += OnPacketRecieved;
		int readTimeoutMilliseconds = 100;
		device.Open(DeviceMode.Promiscuous, readTimeoutMilliseconds);
		device.Open();
		string filter = device.Filter = "ip and udp";
		//Console.WriteLine("Packets are now being captured from '{0}' . Link type : {1}", device.Description, device.LinkType.ToString());
		//Console.WriteLine("-- Listening on {0}, hit 'Ctrl-C' to exit...", device.Description);
		device.Capture();
	}

	private void OnPacketRecieved(object sender, CaptureEventArgs e)
	{
		UdpPacket udp = Packet.ParsePacket(e.Packet.LinkLayerType, e.Packet.Data).Extract<UdpPacket>();
		_ = udp.Bytes;
		if (udp.SourcePort == 5056 || udp.DestinationPort == 5056)
		{
			photonParser.ReceivePacket(udp.PayloadData);
		}
	}

	private ICaptureDevice SelectPacketDevice()
	{
		List<ICaptureDevice> devices = CaptureDeviceList.Instance.ToList();
		if (ConfigManager.SniffingConfig.SniffingDevice != null && devices.Any((ICaptureDevice x) => x.Description == ConfigManager.SniffingConfig.SniffingDevice))
		{
			return devices.First((ICaptureDevice x) => x.Description == ConfigManager.SniffingConfig.SniffingDevice);
		}
		for (int i = 0; i < devices.Count; i++)
		{
			ICaptureDevice device = devices[i];
			Console.WriteLine("{0}) {1} {2}", i, device.Name);
		}
		Console.Write("Please select a device from the list above by typing its corresponding number : ");
		var deviceIndexResponse = Console.ReadLine();
		var deviceIndex = -1;
		if (int.TryParse(deviceIndexResponse, out deviceIndex))
		{
			if (deviceIndex > -1 && deviceIndex < devices.Count)
			{
				var selectedDevice = devices[deviceIndex];
				ConfigManager.SniffingConfig.SniffingDevice = selectedDevice.Description;
				ConfigManager.SniffingConfig.Save();
				return selectedDevice;
			}

			Console.WriteLine("Incorrect device index supplied");
		}
		else
		{
			Console.WriteLine("Non integet value specified");
		}
		return null;
	}


}
