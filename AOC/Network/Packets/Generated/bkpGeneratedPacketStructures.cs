using System.Collections.Generic;
using AOC.Library.Primitive;
using AOC.Util;
using AOC.Network.Packets.Base;
using AOC.Network.Data;
using System;
using System.Linq;

namespace AOC.Network.Packets.Generated
{

	//Event | BaseCode - 191
	public class GEventAccessStatus : BaseEvent
	{
		// From : "260"
		protected System.Int64 Parameter0 { get; set; }
		// From : "3"
		protected System.Int64 Parameter1 { get; set; }
		// From : "[\"@_Owner\",\"@_Guild\",\"@_Everyone\",\"@P_4fbfcb6d-ce5c-4bb2-9c6f-9caf55a2b4f1\",\"@G_d950b314-abdb-4c52-a229-15214238f0d7\",\"@G_5dde9529-3015-44e0-afe3-58de68b22183\",\"@P_1dd09765-57c8-47d9-8fcb-639edd55c2b0\",\"@P_40465ea0-bb01-4036-b48d-4784d9b32c0d\",\"@P_f6512288-7568-46b4-a3e6-0c03efcd0e98\",\"@P_3c7ff5bc-828c-4f36-a95d-8a77626a630f\"]"
		protected System.String[] Parameter4 { get; set; }
		// From : "[\"owner\",\"visitor\",\"noaccess\",\"coowner\",\"visitor\",\"visitor\",\"visitor\",\"coowner\",\"visitor\",\"coowner\"]"
		protected System.String[] Parameter5 { get; set; }
		// From : "[\"\",\"Sinister Kings\",\"\",\"ANNESTESIA\",\"HustlinHitmans\",\"Academia HustlinHitmans II\",\"Zumbie\",\"ElPepelui\",\"Svannia\",\"Krooocker\"]"
		protected System.String[] Parameter6 { get; set; }
		// From : "191"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventAccessStatus(Dictionary<byte, object> data) : base(EventCodes.AccessStatus, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter4 = SafeExtract(4).ToStringArray();
			Parameter5 = SafeExtract(5).ToStringArray();
			Parameter6 = SafeExtract(6).ToStringArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 139
	public class GEventAchievementProgressInfo : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "386"
		protected System.Int64 Parameter1 { get; set; }
		// From : "0.25742856"
		protected System.Double Parameter3 { get; set; }
		// From : "\"[[18025000]]\""
		protected System.String Parameter4 { get; set; }
		// From : "139"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventAchievementProgressInfo(Dictionary<byte, object> data) : base(EventCodes.AchievementProgressInfo, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 53
	public class GEventActionOnBuildingCancel : BaseEvent
	{
		// From : "2509982"
		protected System.Int64 Parameter0 { get; set; }
		// From : "2"
		protected System.Int64 Parameter1 { get; set; }
		// From : "53"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventActionOnBuildingCancel(Dictionary<byte, object> data) : base(EventCodes.ActionOnBuildingCancel, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 54
	public class GEventActionOnBuildingFinished : BaseEvent
	{
		// From : "2518566"
		protected System.Int64 Parameter0 { get; set; }
		// From : "637186256921527197"
		protected System.Int64 Parameter1 { get; set; }
		// From : "1751"
		protected System.Int64 Parameter2 { get; set; }
		// From : "22"
		protected System.Int64 Parameter3 { get; set; }
		// From : "6"
		protected System.Int64 Parameter4 { get; set; }
		// From : "54"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventActionOnBuildingFinished(Dictionary<byte, object> data) : base(EventCodes.ActionOnBuildingFinished, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 52
	public class GEventActionOnBuildingStart : BaseEvent
	{
		// From : "2517851"
		protected System.Int64 Parameter0 { get; set; }
		// From : "1751"
		protected System.Int64 Parameter1 { get; set; }
		// From : "637186256911125051"
		protected System.Int64 Parameter2 { get; set; }
		// From : "637186256941125051"
		protected System.Int64 Parameter3 { get; set; }
		// From : "22"
		protected System.Int64 Parameter4 { get; set; }
		// From : "6"
		protected System.Int64 Parameter5 { get; set; }
		// From : "52"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventActionOnBuildingStart(Dictionary<byte, object> data) : base(EventCodes.ActionOnBuildingStart, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 10
	public class GEventActiveSpellEffectsUpdate : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[929,703,2061,459]"
		protected System.Int64[] Parameter1 { get; set; }
		// From : "[100.0,100.0,397.4152,100.0]"
		protected System.Single[] Parameter2 { get; set; }
		// From : "[57168364,57113194,56677220,53689337]"
		protected System.Int64[] Parameter3 { get; set; }
		// From : "Original \"AQEBAQ==\" : Converted : [65,81,69,66,65,81,61,61]"
		protected System.Int16[] Parameter4 { get; set; }
		// From : "Original \"27YNWwA=\" : Converted : \"MjdZTld3QT0=\""
		protected System.Byte[] Parameter6 { get; set; }
		// From : "[3000,60000,60000,60000,60000,60000,60000,1800000,1800000]"
		protected System.Int64[] Parameter7 { get; set; }
		// From : "Original \"Dw==\" : Converted : [68,119,61,61]"
		protected System.Int16[] Parameter8 { get; set; }
		// From : "Original \"AAAAAA==\" : Converted : [65,65,65,65,65,65,61,61]"
		protected System.Int16[] Parameter9 { get; set; }
		// From : "10"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventActiveSpellEffectsUpdate(Dictionary<byte, object> data) : base(EventCodes.ActiveSpellEffectsUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToIntArray();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter3 = SafeExtract(3).ToIntArray();
			Parameter4 = SafeExtract(4).ToInt16Array();
			Parameter6 = SafeExtract(6).ToByteArray();
			Parameter7 = SafeExtract(7).ToIntArray();
			Parameter8 = SafeExtract(8).ToInt16Array();
			Parameter9 = SafeExtract(9).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 244
	public class GEventArenaRegistrationInfo : BaseEvent
	{
		// From : "4"
		protected System.Int64 Parameter2 { get; set; }
		// From : "637186174276955884"
		protected System.Int64 Parameter3 { get; set; }
		// From : "16"
		protected System.Int64 Parameter4 { get; set; }
		// From : "\"Palurdo\""
		protected System.String Parameter5 { get; set; }
		// From : "244"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventArenaRegistrationInfo(Dictionary<byte, object> data) : base(EventCodes.ArenaRegistrationInfo, data)
		{
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 88
	public class GEventAttachItemContainer : BaseEvent
	{
		// From : "269"
		protected System.Int64 Parameter0 { get; set; }
		// From : "Original \"cGpGxb6dYUmxTF31/RNdrg==\" : Converted : [99,71,112,71,120,98,54,100,89,85,109,120,84,70,51,49,47,82,78,100,114,103,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "Original \"Beq2qVTgxEurBYaZdu75/Q==\" : Converted : [66,101,113,50,113,86,84,103,120,69,117,114,66,89,97,90,100,117,55,53,47,81,61,61]"
		protected System.Int16[] Parameter2 { get; set; }
		// From : "Original \"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==\" : Converted : [65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,65,61,61]"
		protected System.Int16[] Parameter3 { get; set; }
		// From : "64"
		protected System.Int64 Parameter4 { get; set; }
		// From : "88"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventAttachItemContainer(Dictionary<byte, object> data) : base(EventCodes.AttachItemContainer, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToInt16Array();
			Parameter3 = SafeExtract(3).ToInt16Array();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 12
	public class GEventAttack : BaseEvent
	{
		// From : "949133"
		protected System.Int64 Parameter0 { get; set; }
		// From : "57107561"
		protected System.Int64 Parameter1 { get; set; }
		// From : "953314"
		protected System.Int64 Parameter2 { get; set; }
		// From : "2"
		protected System.Int64 Parameter3 { get; set; }
		// From : "57108409"
		protected System.Int64 Parameter4 { get; set; }
		// From : "57108668"
		protected System.Int64 Parameter5 { get; set; }
		// From : "12"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventAttack(Dictionary<byte, object> data) : base(EventCodes.Attack, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 114
	public class GEventBankVaultInfo : BaseEvent
	{
		// From : "5"
		protected System.Int64 Parameter0 { get; set; }
		// From : "\"0e860295-bb5c-4be1-89d0-9b1bbcba0b99@1001\""
		protected System.String Parameter1 { get; set; }
		// From : "[\"rS1IfFU7y0+LPSJeRa22JQ==\",\"0iXrfdizfUyg+id40usL2Q==\"]"
		protected System.String[] Parameter2 { get; set; }
		// From : "[\"equipo\",\"@BUILDINGS_T1_BANK\"]"
		protected System.String[] Parameter3 { get; set; }
		// From : "[\"icon_tag_helmet\",\"icon_tag_chest\"]"
		protected System.String[] Parameter4 { get; set; }
		// From : "[-1168500481,724314623]"
		protected System.Int64[] Parameter5 { get; set; }
		// From : "370"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventBankVaultInfo(Dictionary<byte, object> data) : base(EventCodes.BankVaultInfo, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToStringArray();
			Parameter3 = SafeExtract(3).ToStringArray();
			Parameter4 = SafeExtract(4).ToStringArray();
			Parameter5 = SafeExtract(5).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 14
	public class GEventCastCancel : BaseEvent
	{
		// From : "952078"
		protected System.Int64 Parameter0 { get; set; }
		// From : "true"
		protected System.Boolean Parameter1 { get; set; }
		// From : "1"
		protected System.Int64 Parameter2 { get; set; }
		// From : "14"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCastCancel(Dictionary<byte, object> data) : base(EventCodes.CastCancel, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToBoolean();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 16
	public class GEventCastFinished : BaseEvent
	{
		// From : "2518543"
		protected System.Int64 Parameter0 { get; set; }
		// From : "2518543"
		protected System.Int64 Parameter1 { get; set; }
		// From : "711"
		protected System.Int64 Parameter2 { get; set; }
		// From : "637186256849025051"
		protected System.Int64 Parameter3 { get; set; }
		// From : "16"
		protected System.Int64 Parameter4 { get; set; }
		// From : "1"
		protected System.Int64 Parameter5 { get; set; }
		// From : "637186256849025051"
		protected System.Int64 Parameter6 { get; set; }
		// From : "16"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCastFinished(Dictionary<byte, object> data) : base(EventCodes.CastFinished, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 18
	public class GEventCastHit : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "325"
		protected System.Int64 Parameter1 { get; set; }
		// From : "1971"
		protected System.Int64 Parameter2 { get; set; }
		// From : "1"
		protected System.Int64 Parameter3 { get; set; }
		// From : "0"
		protected System.Int64 Parameter4 { get; set; }
		// From : "18"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCastHit(Dictionary<byte, object> data) : base(EventCodes.CastHit, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 19
	public class GEventCastHits : BaseEvent
	{
		// From : "941099"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[9412,9412,9412]"
		protected System.Int64[] Parameter1 { get; set; }
		// From : "[718,689,703]"
		protected System.Int64[] Parameter2 { get; set; }
		// From : "\"AQEB\""
		protected System.String Parameter3 { get; set; }
		// From : "\"AAAA\""
		protected System.String Parameter4 { get; set; }
		// From : "19"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCastHits(Dictionary<byte, object> data) : base(EventCodes.CastHits, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToIntArray();
			Parameter2 = SafeExtract(2).ToIntArray();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 17
	public class GEventCastSpell : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "325"
		protected System.Int64 Parameter1 { get; set; }
		// From : "[204.46437,37.750008]"
		protected System.Single[] Parameter2 { get; set; }
		// From : "1971"
		protected System.Int64 Parameter3 { get; set; }
		// From : "57230274"
		protected System.Int64 Parameter4 { get; set; }
		// From : "57230274"
		protected System.Int64 Parameter5 { get; set; }
		// From : "0"
		protected System.Int64 Parameter6 { get; set; }
		// From : "17"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCastSpell(Dictionary<byte, object> data) : base(EventCodes.CastSpell, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 13
	public class GEventCastStart : BaseEvent
	{
		// From : "2518543"
		protected System.Int64 Parameter0 { get; set; }
		// From : "57135961"
		protected System.Int64 Parameter1 { get; set; }
		// From : "[0.0,0.0]"
		protected System.Single[] Parameter2 { get; set; }
		// From : "57135961"
		protected System.Int64 Parameter3 { get; set; }
		// From : "711"
		protected System.Int64 Parameter4 { get; set; }
		// From : "-1"
		protected System.Int64 Parameter5 { get; set; }
		// From : "2518543"
		protected System.Int64 Parameter6 { get; set; }
		// From : "1"
		protected System.Int64 Parameter7 { get; set; }
		// From : "13"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCastStart(Dictionary<byte, object> data) : base(EventCodes.CastStart, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 15
	public class GEventCastTimeUpdate : BaseEvent
	{
		// From : "942872"
		protected System.Int64 Parameter0 { get; set; }
		// From : "637186255827539242"
		protected System.Int64 Parameter1 { get; set; }
		// From : "1"
		protected System.Int64 Parameter3 { get; set; }
		// From : "15"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCastTimeUpdate(Dictionary<byte, object> data) : base(EventCodes.CastTimeUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 147
	public class GEventChangeAvatar : BaseEvent
	{
		// From : "2373602"
		protected System.Int64 Parameter0 { get; set; }
		// From : "27"
		protected System.Int64 Parameter1 { get; set; }
		// From : "147"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventChangeAvatar(Dictionary<byte, object> data) : base(EventCodes.ChangeAvatar, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 73
	public class GEventChangeFlaggingFinished : BaseEvent
	{
		// From : "2518543"
		protected System.Int64 Parameter0 { get; set; }
		// From : "2"
		protected System.Int64 Parameter1 { get; set; }
		// From : "329"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventChangeFlaggingFinished(Dictionary<byte, object> data) : base(EventCodes.ChangeFlaggingFinished, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 20
	public class GEventChannelingEnded : BaseEvent
	{
		// From : "953689"
		protected System.Int64 Parameter0 { get; set; }
		// From : "3"
		protected System.Int64 Parameter1 { get; set; }
		// From : "9"
		protected System.Int64 Parameter2 { get; set; }
		// From : "20"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventChannelingEnded(Dictionary<byte, object> data) : base(EventCodes.ChannelingEnded, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 79
	public class GEventCharacterEquipmentChanged : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "57148319"
		protected System.Int64 Parameter1 { get; set; }
		// From : "[1883,0,2583,2733,2204,1790,1562,1895,0,0]"
		protected System.Int64[] Parameter2 { get; set; }
		// From : "[-1,-1,-1,1732,1810,1885,-1,-1,-1,-1,-1,-1,2208,2061]"
		protected System.Int64[] Parameter5 { get; set; }
		// From : "true"
		protected System.Boolean Parameter6 { get; set; }
		// From : "79"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCharacterEquipmentChanged(Dictionary<byte, object> data) : base(EventCodes.CharacterEquipmentChanged, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToIntArray();
			Parameter5 = SafeExtract(5).ToIntArray();
			Parameter6 = SafeExtract(6).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 132
	public class GEventCharacterStats : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "\"Fhancuz\""
		protected System.String Parameter1 { get; set; }
		// From : "\"\""
		protected System.String Parameter2 { get; set; }
		// From : "0"
		protected System.Int64 Parameter3 { get; set; }
		// From : "\"\""
		protected System.String Parameter4 { get; set; }
		// From : "\"ZYN ZYN ZYN\""
		protected System.String Parameter5 { get; set; }
		// From : "41454401974"
		protected System.Int64 Parameter6 { get; set; }
		// From : "365170583537"
		protected System.Int64 Parameter7 { get; set; }
		// From : "18070.46"
		protected System.Double Parameter8 { get; set; }
		// From : "185"
		protected System.Int64 Parameter9 { get; set; }
		// From : "16907311327"
		protected System.Int64 Parameter10 { get; set; }
		// From : "41974"
		protected System.Int64 Parameter11 { get; set; }
		// From : "342494413085"
		protected System.Int64 Parameter12 { get; set; }
		// From : "3585420000"
		protected System.Int64 Parameter13 { get; set; }
		// From : "541190000"
		protected System.Int64 Parameter15 { get; set; }
		// From : "1168270000"
		protected System.Int64 Parameter16 { get; set; }
		// From : "4"
		protected System.Int64 Parameter23 { get; set; }
		// From : "20"
		protected System.Int64 Parameter24 { get; set; }
		// From : "8"
		protected System.Int64 Parameter26 { get; set; }
		// From : "4"
		protected System.Int64 Parameter27 { get; set; }
		// From : "132"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCharacterStats(Dictionary<byte, object> data) : base(EventCodes.CharacterStats, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToString();
			Parameter5 = SafeExtract(5).ToString();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToDouble();
			Parameter9 = SafeExtract(9).ToInt();
			Parameter10 = SafeExtract(10).ToInt();
			Parameter11 = SafeExtract(11).ToInt();
			Parameter12 = SafeExtract(12).ToInt();
			Parameter13 = SafeExtract(13).ToInt();
			Parameter15 = SafeExtract(15).ToInt();
			Parameter16 = SafeExtract(16).ToInt();
			Parameter23 = SafeExtract(23).ToInt();
			Parameter24 = SafeExtract(24).ToInt();
			Parameter26 = SafeExtract(26).ToInt();
			Parameter27 = SafeExtract(27).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 134
	public class GEventCharacterStatsDeathHistory : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "\"Zigvalt\""
		protected System.String Parameter1 { get; set; }
		// From : "[\"@MOB_T5_MOB_DEMON_IMP_YELLOW_VETERAN\",\"Zigvalt\",\"Emirk\",\"Joakiga\",\"@MOB_T4_MOB_DEMON_SPIKED\",\"@MOB_T6_MOB_HRD_HERETIC_SCAVENGER_VETERAN\"]"
		protected System.String[] Parameter2 { get; set; }
		// From : "[198065700,449526962,521181663,618588663,123665400,1488367888]"
		protected System.Int64[] Parameter3 { get; set; }
		// From : "[\"VJyITC+FV0CksgCg3LErlw==\",\"kFGUY75/NEygwDmG+dBq8A==\",\"ej3TCfwfIE+FdUxDcburKg==\",\"Y3VDsZaiYkiK+s5w2pOT9g==\",\"dbMiTnwCd06vn9SntYy0BQ==\",\"FDfZyQCogkeFTIdyDtpujQ==\"]"
		protected System.String[] Parameter4 { get; set; }
		// From : "134"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCharacterStatsDeathHistory(Dictionary<byte, object> data) : base(EventCodes.CharacterStatsDeathHistory, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToStringArray();
			Parameter3 = SafeExtract(3).ToIntArray();
			Parameter4 = SafeExtract(4).ToStringArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 133
	public class GEventCharacterStatsKillHistory : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "\"Fhancuz\""
		protected System.String Parameter1 { get; set; }
		// From : "[\"Ganek\",\"Artee\",\"SirCondorX\",\"Daltin\",\"jewzelda\",\"Fhancuz\",\"Skuga\",\"DanBarz\",\"satanboy\",\"Antidepreska\",\"Saola\",\"Taroinha\",\"antivlad\",\"SyrRiall\",\"nobleblood\",\"LordZueira\",\"Joakiga\"]"
		protected System.String[] Parameter2 { get; set; }
		// From : "[1157338701,91260000,346161600,29520000,1029810519,71640000,386712611,38160000,599858153,193751100,50765400,24660000,303041322,270816318,1422834849,98910000,719554908]"
		protected System.Int64[] Parameter3 { get; set; }
		// From : "[\"LSOHtlJGxkGTr6lXl3hbAg==\",\"pM5a8BsV20KlLD5TNLPbRg==\",\"eF4+lSffukO6F+aSAIqgJg==\",\"m9dp3mU3ikaV2TwrSODVoA==\",\"/DMSEiAhd0C50sb3ELXi/g==\",\"4Q7DKVjNPUy3XsyvH9uIwQ==\",\"P2GpPedikE6D7lXXsgwxZw==\",\"V2kzyLok9kylZQg89oOLQA==\",\"sXOUVbp4l0SSraRCLGvi2w==\",\"yvGd1CjDdEyrttIJvo68sQ==\",\"P8sHtjQBqEuH7VPUju1Jgw==\",\"RQqcwT+Wqk27ImfIYbGamA==\",\"huk8tvVrMkmgAQzMvOxKtg==\",\"ZIsAwjBBUE2PKteijCIXyg==\",\"hicjgXpJX0u5EBGjHXzzIg==\",\"+wo7ajFfBUWo1I0BVaKrSQ==\",\"dNi1EkXgdUC/8iBdmIZDrw==\"]"
		protected System.String[] Parameter4 { get; set; }
		// From : "133"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCharacterStatsKillHistory(Dictionary<byte, object> data) : base(EventCodes.CharacterStatsKillHistory, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToStringArray();
			Parameter3 = SafeExtract(3).ToIntArray();
			Parameter4 = SafeExtract(4).ToStringArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 64
	public class GEventChatSay : BaseEvent
	{
		// From : "2516845"
		protected System.Int64 Parameter0 { get; set; }
		// From : "\"archiduiiin\""
		protected System.String Parameter1 { get; set; }
		// From : "\"selling 16 Rough Logs\""
		protected System.String Parameter2 { get; set; }
		// From : "0"
		protected System.Int64 Parameter5 { get; set; }
		// From : "64"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventChatSay(Dictionary<byte, object> data) : base(EventCodes.ChatSay, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToString();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 129
	public class GEventClusterInfoUpdate : BaseEvent
	{
		// From : "\"4216\""
		protected System.String Parameter0 { get; set; }
		// From : "\"\""
		protected System.String Parameter2 { get; set; }
		// From : "Original \"Aw==\" : Converted : [65,119,61,61]"
		protected System.Int16[] Parameter19 { get; set; }
		// From : "[9223372036854775807]"
		protected System.Int64[] Parameter20 { get; set; }
		// From : "[1]"
		protected System.Int64[] Parameter21 { get; set; }
		// From : "[]"
		protected System.Single[] Parameter22 { get; set; }
		// From : "[]"
		protected System.Single[] Parameter23 { get; set; }
		// From : "\"\""
		protected System.String Parameter24 { get; set; }
		// From : "\"\""
		protected System.String Parameter25 { get; set; }
		// From : "129"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventClusterInfoUpdate(Dictionary<byte, object> data) : base(EventCodes.ClusterInfoUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToString();
			Parameter2 = SafeExtract(2).ToString();
			Parameter19 = SafeExtract(19).ToInt16Array();
			Parameter20 = SafeExtract(20).ToIntArray();
			Parameter21 = SafeExtract(21).ToIntArray();
			Parameter22 = SafeExtract(22).ToFloatArray();
			Parameter23 = SafeExtract(23).ToFloatArray();
			Parameter24 = SafeExtract(24).ToString();
			Parameter25 = SafeExtract(25).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 38
	public class GEventCraftBuildingInfo : BaseEvent
	{
		// From : "1735"
		protected System.Int64 Parameter0 { get; set; }
		// From : "\"\""
		protected System.String Parameter5 { get; set; }
		// From : "\"\""
		protected System.String Parameter6 { get; set; }
		// From : "38"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCraftBuildingInfo(Dictionary<byte, object> data) : base(EventCodes.CraftBuildingInfo, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter5 = SafeExtract(5).ToString();
			Parameter6 = SafeExtract(6).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 9
	public class GEventCraftingFocusUpdate : BaseEvent
	{
		// From : "2505796"
		protected System.Int64 Parameter0 { get; set; }
		// From : "56820947"
		protected System.Int64 Parameter1 { get; set; }
		// From : "30000.0"
		protected System.Double Parameter3 { get; set; }
		// From : "9"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCraftingFocusUpdate(Dictionary<byte, object> data) : base(EventCodes.CraftingFocusUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 61
	public class GEventCraftItemFinished : BaseEvent
	{
		// From : "2505796"
		protected System.Int64 Parameter0 { get; set; }
		// From : "1752"
		protected System.Int64 Parameter1 { get; set; }
		// From : "[2506309]"
		protected System.Int64[] Parameter2 { get; set; }
		// From : "\"\""
		protected System.String Parameter3 { get; set; }
		// From : "\"\""
		protected System.String Parameter4 { get; set; }
		// From : "61"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCraftItemFinished(Dictionary<byte, object> data) : base(EventCodes.CraftItemFinished, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToIntArray();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 111
	public class GEventCustomizationChanged : BaseEvent
	{
		// From : "2479010"
		protected System.Int64 Parameter0 { get; set; }
		// From : "Original \"AgAAAAY=\" : Converted : \"QWdBQUFBWT0=\""
		protected System.Byte[] Parameter1 { get; set; }
		// From : "Original \"AAABAAU=\" : Converted : \"QUFBQkFBVT0=\""
		protected System.Byte[] Parameter2 { get; set; }
		// From : "367"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventCustomizationChanged(Dictionary<byte, object> data) : base(EventCodes.CustomizationChanged, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToByteArray();
			Parameter2 = SafeExtract(2).ToByteArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 8
	public class GEventDamageShieldUpdate : BaseEvent
	{
		// From : "2481310"
		protected System.Int64 Parameter0 { get; set; }
		// From : "8"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventDamageShieldUpdate(Dictionary<byte, object> data) : base(EventCodes.DamageShieldUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 23
	public class GEventDefenseUnitAttackBegin : BaseEvent
	{
		// From : "4593"
		protected System.Int64 Parameter0 { get; set; }
		// From : "52953648"
		protected System.Int64 Parameter1 { get; set; }
		// From : "63429"
		protected System.Int64 Parameter2 { get; set; }
		// From : "279"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventDefenseUnitAttackBegin(Dictionary<byte, object> data) : base(EventCodes.DefenseUnitAttackBegin, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 25
	public class GEventDefenseUnitAttackDamage : BaseEvent
	{
		// From : "4593"
		protected System.Int64 Parameter0 { get; set; }
		// From : "52955948"
		protected System.Int64 Parameter1 { get; set; }
		// From : "63429"
		protected System.Int64 Parameter2 { get; set; }
		// From : "52956659"
		protected System.Int64 Parameter3 { get; set; }
		// From : "281"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventDefenseUnitAttackDamage(Dictionary<byte, object> data) : base(EventCodes.DefenseUnitAttackDamage, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 24
	public class GEventDefenseUnitAttackEnd : BaseEvent
	{
		// From : "4593"
		protected System.Int64 Parameter0 { get; set; }
		// From : "52956390"
		protected System.Int64 Parameter1 { get; set; }
		// From : "63429"
		protected System.Int64 Parameter2 { get; set; }
		// From : "280"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventDefenseUnitAttackEnd(Dictionary<byte, object> data) : base(EventCodes.DefenseUnitAttackEnd, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 89
	public class GEventDetachItemContainer : BaseEvent
	{
		// From : "Original \"cGpGxb6dYUmxTF31/RNdrg==\" : Converted : [99,71,112,71,120,98,54,100,89,85,109,120,84,70,51,49,47,82,78,100,114,103,61,61]"
		protected System.Int16[] Parameter0 { get; set; }
		// From : "89"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventDetachItemContainer(Dictionary<byte, object> data) : base(EventCodes.DetachItemContainer, data)
		{
			Parameter0 = SafeExtract(0).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 151
	public class GEventDied : BaseEvent
	{
		// From : "[0.0,0.0]"
		protected System.Single[] Parameter0 { get; set; }
		// From : "1157274"
		protected System.Int64 Parameter1 { get; set; }
		// From : "\"Tempesty\""
		protected System.String Parameter2 { get; set; }
		// From : "1157274"
		protected System.Int64 Parameter3 { get; set; }
		// From : "\"Tempesty\""
		protected System.String Parameter4 { get; set; }
		// From : "\"A ordem dos assassinos\""
		protected System.String Parameter5 { get; set; }
		// From : "true"
		protected System.Boolean Parameter6 { get; set; }
		// From : "151"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventDied(Dictionary<byte, object> data) : base(EventCodes.Died, data)
		{
			Parameter0 = SafeExtract(0).ToFloatArray();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToString();
			if (SafeExtract(5) == null) { Parameter5 = null; } else { Parameter5 = SafeExtract(5).ToString(); }
			Parameter6 = SafeExtract(6).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 170
	public class GEventDuelEnded : BaseEvent
	{
		// From : "2456241"
		protected System.Int64 Parameter0 { get; set; }
		// From : "3"
		protected System.Int64 Parameter1 { get; set; }
		// From : "170"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventDuelEnded(Dictionary<byte, object> data) : base(EventCodes.DuelEnded, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 172
	public class GEventDuelLeftArea : BaseEvent
	{
		// From : "63429"
		protected System.Int64 Parameter0 { get; set; }
		// From : "637186209685277907"
		protected System.Int64 Parameter1 { get; set; }
		// From : "172"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventDuelLeftArea(Dictionary<byte, object> data) : base(EventCodes.DuelLeftArea, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 173
	public class GEventDuelReEnteredArea : BaseEvent
	{
		// From : "63508"
		protected System.Int64 Parameter0 { get; set; }
		// From : "173"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventDuelReEnteredArea(Dictionary<byte, object> data) : base(EventCodes.DuelReEnteredArea, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 169
	public class GEventDuelStarted : BaseEvent
	{
		// From : "2502112"
		protected System.Int64 Parameter0 { get; set; }
		// From : "2470439"
		protected System.Int64 Parameter1 { get; set; }
		// From : "169"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventDuelStarted(Dictionary<byte, object> data) : base(EventCodes.DuelStarted, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 86
	public class GEventDurabilityChanged : BaseEvent
	{
		// From : "[953322,953318,953323,953317,953320,953321]"
		protected System.Int64[] Parameter0 { get; set; }
		// From : "[107800000,242800000,202300000,98800000,134800000,119800000]"
		protected System.Int64[] Parameter1 { get; set; }
		// From : "86"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventDurabilityChanged(Dictionary<byte, object> data) : base(EventCodes.DurabilityChanged, data)
		{
			Parameter0 = SafeExtract(0).ToIntArray();
			Parameter1 = SafeExtract(1).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 7
	public class GEventEnergyUpdate : BaseEvent
	{
		// From : "2518543"
		protected System.Int64 Parameter0 { get; set; }
		// From : "57136005"
		protected System.Int64 Parameter1 { get; set; }
		// From : "275.0"
		protected System.Double Parameter3 { get; set; }
		// From : "7"
		protected System.Int64 Parameter4 { get; set; }
		// From : "2518543"
		protected System.Int64 Parameter5 { get; set; }
		// From : "711"
		protected System.Int64 Parameter6 { get; set; }
		// From : "7"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventEnergyUpdate(Dictionary<byte, object> data) : base(EventCodes.EnergyUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 246
	public class GEventEnteringArenaCancel : BaseEvent
	{
		// From : "1165287"
		protected System.Int64 Parameter0 { get; set; }
		// From : "true"
		protected System.Boolean Parameter1 { get; set; }
		// From : "246"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventEnteringArenaCancel(Dictionary<byte, object> data) : base(EventCodes.EnteringArenaCancel, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 248
	public class GEventEnteringArenaLockCancel : BaseEvent
	{
		// From : "1164101"
		protected System.Int64 Parameter0 { get; set; }
		// From : "248"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventEnteringArenaLockCancel(Dictionary<byte, object> data) : base(EventCodes.EnteringArenaLockCancel, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 247
	public class GEventEnteringArenaLockStart : BaseEvent
	{
		// From : "1165287"
		protected System.Int64 Parameter0 { get; set; }
		// From : "247"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventEnteringArenaLockStart(Dictionary<byte, object> data) : base(EventCodes.EnteringArenaLockStart, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 245
	public class GEventEnteringArenaStart : BaseEvent
	{
		// From : "1165287"
		protected System.Int64 Parameter0 { get; set; }
		// From : "245"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventEnteringArenaStart(Dictionary<byte, object> data) : base(EventCodes.EnteringArenaStart, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 242
	public class GEventEnteringExpeditionCancel : BaseEvent
	{
		// From : "2506271"
		protected System.Int64 Parameter0 { get; set; }
		// From : "true"
		protected System.Boolean Parameter1 { get; set; }
		// From : "242"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventEnteringExpeditionCancel(Dictionary<byte, object> data) : base(EventCodes.EnteringExpeditionCancel, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 241
	public class GEventEnteringExpeditionStart : BaseEvent
	{
		// From : "2506271"
		protected System.Int64 Parameter0 { get; set; }
		// From : "241"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventEnteringExpeditionStart(Dictionary<byte, object> data) : base(EventCodes.EnteringExpeditionStart, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 231
	public class GEventExitEnterFinished : BaseEvent
	{
		// From : "948974"
		protected System.Int64 Parameter0 { get; set; }
		// From : "231"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventExitEnterFinished(Dictionary<byte, object> data) : base(EventCodes.ExitEnterFinished, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 229
	public class GEventExitEnterStart : BaseEvent
	{
		// From : "828058"
		protected System.Int64 Parameter0 { get; set; }
		// From : "48394686"
		protected System.Int64 Parameter1 { get; set; }
		// From : "48396686"
		protected System.Int64 Parameter2 { get; set; }
		// From : "801062"
		protected System.Int64 Parameter3 { get; set; }
		// From : "229"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventExitEnterStart(Dictionary<byte, object> data) : base(EventCodes.ExitEnterStart, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 37
	public class GEventFactionBuildingInfo : BaseEvent
	{
		// From : "1752"
		protected System.Int64 Parameter0 { get; set; }
		// From : "37"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventFactionBuildingInfo(Dictionary<byte, object> data) : base(EventCodes.FactionBuildingInfo, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 183
	public class GEventFarmableObjectInfo : BaseEvent
	{
		// From : "302"
		protected System.Int64 Parameter0 { get; set; }
		// From : "792000000"
		protected System.Int64 Parameter1 { get; set; }
		// From : "637078703417161457"
		protected System.Int64 Parameter2 { get; set; }
		// From : "true"
		protected System.Boolean Parameter3 { get; set; }
		// From : "Original \"RQ==\" : Converted : [82,81,61,61]"
		protected System.Int16[] Parameter6 { get; set; }
		// From : "Original \"AQ==\" : Converted : [65,81,61,61]"
		protected System.Int16[] Parameter7 { get; set; }
		// From : "Original \"AA==\" : Converted : [65,65,61,61]"
		protected System.Int16[] Parameter8 { get; set; }
		// From : "[637078703417161457]"
		protected System.Int64[] Parameter9 { get; set; }
		// From : "\"\""
		protected System.String Parameter10 { get; set; }
		// From : "\"\""
		protected System.String Parameter11 { get; set; }
		// From : "0.5"
		protected System.Double Parameter12 { get; set; }
		// From : "-9223372036854775808"
		protected System.Int64 Parameter13 { get; set; }
		// From : "183"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventFarmableObjectInfo(Dictionary<byte, object> data) : base(EventCodes.FarmableObjectInfo, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToBoolean();
			Parameter6 = SafeExtract(6).ToInt16Array();
			Parameter7 = SafeExtract(7).ToInt16Array();
			Parameter8 = SafeExtract(8).ToInt16Array();
			Parameter9 = SafeExtract(9).ToIntArray();
			Parameter10 = SafeExtract(10).ToString();
			Parameter11 = SafeExtract(11).ToString();
			Parameter12 = SafeExtract(12).ToDouble();
			Parameter13 = SafeExtract(13).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 138
	public class GEventFinishedAchievement : BaseEvent
	{
		// From : "2383826"
		protected System.Int64 Parameter0 { get; set; }
		// From : "18"
		protected System.Int64 Parameter1 { get; set; }
		// From : "7"
		protected System.Int64 Parameter2 { get; set; }
		// From : "138"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventFinishedAchievement(Dictionary<byte, object> data) : base(EventCodes.FinishedAchievement, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 67
	public class GEventFishingCancel : BaseEvent
	{
		// From : "953314"
		protected System.Int64 Parameter0 { get; set; }
		// From : "12"
		protected System.Int64 Parameter1 { get; set; }
		// From : "1"
		protected System.Int64 Parameter2 { get; set; }
		// From : "323"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventFishingCancel(Dictionary<byte, object> data) : base(EventCodes.FishingCancel, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 70
	public class GEventFishingMiniGame : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "6"
		protected System.Int64 Parameter1 { get; set; }
		// From : "637186257792158003"
		protected System.Int64 Parameter2 { get; set; }
		// From : "1.0"
		protected System.Double Parameter3 { get; set; }
		// From : "0.5"
		protected System.Double Parameter4 { get; set; }
		// From : "3.5"
		protected System.Double Parameter5 { get; set; }
		// From : "0.8"
		protected System.Double Parameter6 { get; set; }
		// From : "1.1"
		protected System.Double Parameter7 { get; set; }
		// From : "2.2"
		protected System.Double Parameter8 { get; set; }
		// From : "1.0"
		protected System.Double Parameter10 { get; set; }
		// From : "3.0"
		protected System.Double Parameter11 { get; set; }
		// From : "0.75"
		protected System.Double Parameter12 { get; set; }
		// From : "8.0"
		protected System.Double Parameter13 { get; set; }
		// From : "4.0"
		protected System.Double Parameter15 { get; set; }
		// From : "326"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventFishingMiniGame(Dictionary<byte, object> data) : base(EventCodes.FishingMiniGame, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToDouble();
			Parameter6 = SafeExtract(6).ToDouble();
			Parameter7 = SafeExtract(7).ToDouble();
			Parameter8 = SafeExtract(8).ToDouble();
			Parameter10 = SafeExtract(10).ToDouble();
			Parameter11 = SafeExtract(11).ToDouble();
			Parameter12 = SafeExtract(12).ToDouble();
			Parameter13 = SafeExtract(13).ToDouble();
			Parameter15 = SafeExtract(15).ToDouble();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 63
	public class GEventFishingStart : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "327"
		protected System.Int64 Parameter1 { get; set; }
		// From : "1883"
		protected System.Int64 Parameter2 { get; set; }
		// From : "10"
		protected System.Int64 Parameter3 { get; set; }
		// From : "637186257588285051"
		protected System.Int64 Parameter4 { get; set; }
		// From : "319"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventFishingStart(Dictionary<byte, object> data) : base(EventCodes.FishingStart, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 130
	public class GEventForcedMovement : BaseEvent
	{
		// From : "953689"
		protected System.Int64 Parameter0 { get; set; }
		// From : "2"
		protected System.Int64 Parameter1 { get; set; }
		// From : "637186255257624989"
		protected System.Int64 Parameter2 { get; set; }
		// From : "56977049"
		protected System.Int64 Parameter3 { get; set; }
		// From : "11.0"
		protected System.Double Parameter4 { get; set; }
		// From : "[336.6773,62.07404,338.8891,60.876625]"
		protected System.Single[] Parameter5 { get; set; }
		// From : "2859"
		protected System.Int64 Parameter6 { get; set; }
		// From : "1"
		protected System.Int64 Parameter7 { get; set; }
		// From : "130"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventForcedMovement(Dictionary<byte, object> data) : base(EventCodes.ForcedMovement, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToFloatArray();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 131
	public class GEventForcedMovementCancel : BaseEvent
	{
		// From : "2505219"
		protected System.Int64 Parameter0 { get; set; }
		// From : "131"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventForcedMovementCancel(Dictionary<byte, object> data) : base(EventCodes.ForcedMovementCancel, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 16
	public class GEventFriendOnlineStatus : BaseEvent
	{
		// From : "true"
		protected System.Boolean Parameter0 { get; set; }
		// From : "Original \"vPV/PIyCNk+pXYp3YmpjDw==\" : Converted : [118,80,86,47,80,73,121,67,78,107,43,112,88,89,112,51,89,109,112,106,68,119,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "\"Krooocker\""
		protected System.String Parameter2 { get; set; }
		// From : "272"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventFriendOnlineStatus(Dictionary<byte, object> data) : base(EventCodes.FriendOnlineStatus, data)
		{
			Parameter0 = SafeExtract(0).ToBoolean();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 13
	public class GEventFriendRequestInfos : BaseEvent
	{
		// From : "[\"dvT+Jb71sE2dSIxY8pesIg==\"]"
		protected System.String[] Parameter1 { get; set; }
		// From : "[\"XlunaX\"]"
		protected System.String[] Parameter2 { get; set; }
		// From : "[\"Blessed\"]"
		protected System.String[] Parameter3 { get; set; }
		// From : "Original \"Ag==\" : Converted : [65,103,61,61]"
		protected System.Int16[] Parameter4 { get; set; }
		// From : "Original \"CA==\" : Converted : [67,65,61,61]"
		protected System.Int16[] Parameter5 { get; set; }
		// From : "Original \"AQ==\" : Converted : [65,81,61,61]"
		protected System.Int16[] Parameter6 { get; set; }
		// From : "269"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventFriendRequestInfos(Dictionary<byte, object> data) : base(EventCodes.FriendRequestInfos, data)
		{
			Parameter1 = SafeExtract(1).ToStringArray();
			Parameter2 = SafeExtract(2).ToStringArray();
			Parameter3 = SafeExtract(3).ToStringArray();
			Parameter4 = SafeExtract(4).ToInt16Array();
			Parameter5 = SafeExtract(5).ToInt16Array();
			Parameter6 = SafeExtract(6).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 137
	public class GEventFullAchievementInfo : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[419,379,330,140,406,261,413,97,24,4,414,235,407,0,415,2,75,416,300,380,103,5,417,3,418,76,1,370,360,35,25,70,109,141,37,28,388,72,73,74,110,137,138,50,124,52]"
		protected System.Int64[] Parameter1 { get; set; }
		// From : "[6,11,16,17,18,19,26,27,29,30,31,32,33,34,36,38,39,40,45,46,47,48,49,51,53,54,55,60,61,62,63,64,65,71,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,98,99,100,101,102,104,105,106,107,108,111,112,113,114,115,116,117,118,119,125,126,127,128,129,130,131,132,133,134,135,136,139,142,152,154,162,164,165,166,167,168,169,174,184,194,199,209,214,262,267,272,273,277,282,283,284,301,305,309,313,317,321,325,331,332,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,361,362,363,364,365,366,367,368,369,371,381,386,389,390,394,400,408,409,410]"
		protected System.Int64[] Parameter2 { get; set; }
		// From : "Original \"AAAgAAgAEwUBQwsHCQwPFwEAAQEBAAEtPDwAUAEBAQEASz8EHgAfSwkaAAYZAQAFASoDAAAAKgAQAAApAAQDBwk5AEYCKQMIAAIACwAAAAABAAAAAAUAAAAABwAAAwAAAAAAAAAAAAABAAAMCAAAAAAAAAAAAABYBQAMAQAHCBEJYSEAABQAAAAALQAACQAAAAAEAAAAAQAAAAYEAA==\" : Converted : [65,65,65,103,65,65,103,65,69,119,85,66,81,119,115,72,67,81,119,80,70,119,69,65,65,81,69,66,65,65,69,116,80,68,119,65,85,65,69,66,65,81,69,65,83,122,56,69,72,103,65,102,83,119,107,97,65,65,89,90,65,81,65,70,65,83,111,68,65,65,65,65,75,103,65,81,65,65,65,112,65,65,81,68,66,119,107,53,65,69,89,67,75,81,77,73,65,65,73,65,67,119,65,65,65,65,65,66,65,65,65,65,65,65,85,65,65,65,65,65,66,119,65,65,65,119,65,65,65,65,65,65,65,65,65,65,65,65,65,66,65,65,65,77,67,65,65,65,65,65,65,65,65,65,65,65,65,65,66,89,66,81,65,77,65,81,65,72,67,66,69,74,89,83,69,65,65,66,81,65,65,65,65,65,76,81,65,65,67,81,65,65,65,65,65,69,65,65,65,65,65,81,65,65,65,65,89,69,65,65,61,61]"
		protected System.Int16[] Parameter3 { get; set; }
		// From : "[true,false,true,true,true,false,true,true,true,true,true,true,true,true,true,true,true,false,true,true,true,false,true,true,true,true,true,true,true,true,true,true,false,true,true,true,true,true,true,true,true,true,true,true,true,true,false,true,true,false,true,false,false,true,true,true,true,true,false,true,true,true,true,true,true,true,false,true,true,true,true,true,false,true,false,true,true,false,false,false,true,false,false,false,false,true,true,false,true,false,true,false,false,true,false,false,false,true,true,true,true,false,true,true,true,false,false,true,true,true,false,false,true,false,false,false,false,true,false,true,true,false,true,true,true,true,true,true,true,true,true,false,false,true,false,false,true,false,true,false,false,true,false,false,false,false,true,false,true,true,true,false,true,true,true,true,true]"
		protected System.Boolean[] Parameter4 { get; set; }
		// From : "[false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true,false,false,false,false,false,false,false,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]"
		protected System.Boolean[] Parameter5 { get; set; }
		// From : "137"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventFullAchievementInfo(Dictionary<byte, object> data) : base(EventCodes.FullAchievementInfo, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToIntArray();
			Parameter2 = SafeExtract(2).ToIntArray();
			Parameter3 = SafeExtract(3).ToInt16Array();
			Parameter4 = SafeExtract(4).ToBooleanArray();
			Parameter5 = SafeExtract(5).ToBooleanArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 140
	public class GEventFullAchievementProgressInfo : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[364,369,381,386,394,400,408,410]"
		protected System.Int64[] Parameter1 { get; set; }
		// From : "Original \"CQQAAAAABgA=\" : Converted : \"Q1FRQUFBQUFCZ0E9\""
		protected System.Byte[] Parameter2 { get; set; }
		// From : "[0.5123967,0.13482143,0.0072,0.256,0.656,0.0017777778,0.0066696308,0.056382503]"
		protected System.Single[] Parameter3 { get; set; }
		// From : "[\"[[14880004]]\",\"[[3020001]]\",\"[[2160000]]\",\"[[17925000]]\",\"[[14760000]]\",\"[[45000]]\",\"[[450000]]\",\"[[289434375]]\"]"
		protected System.String[] Parameter4 { get; set; }
		// From : "140"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventFullAchievementProgressInfo(Dictionary<byte, object> data) : base(EventCodes.FullAchievementProgressInfo, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToIntArray();
			Parameter2 = SafeExtract(2).ToByteArray();
			Parameter3 = SafeExtract(3).ToFloatArray();
			Parameter4 = SafeExtract(4).ToStringArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 234
	public class GEventFullQuestInfo : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "-1"
		protected System.Int64 Parameter1 { get; set; }
		// From : "\"\""
		protected System.String Parameter3 { get; set; }
		// From : "0"
		protected System.Int64 Parameter4 { get; set; }
		// From : "255"
		protected System.Int64 Parameter6 { get; set; }
		// From : "-9223372036854775808"
		protected System.Int64 Parameter7 { get; set; }
		// From : "1"
		protected System.Int64 Parameter8 { get; set; }
		// From : "\"\""
		protected System.String Parameter9 { get; set; }
		// From : "234"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventFullQuestInfo(Dictionary<byte, object> data) : base(EventCodes.FullQuestInfo, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToInt();
			Parameter9 = SafeExtract(9).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 149
	public class GEventGameEvent : BaseEvent
	{
		// From : "29"
		protected System.Int64 Parameter0 { get; set; }
		// From : "1"
		protected System.Int64 Parameter1 { get; set; }
		// From : "\"{\\\"reason\\\":\\\"ChallengeeKnockedDownByChallenger\\\",\\\"challenger\\\":\\\"Bakedluka\\\",\\\"challengee\\\":\\\"Majkeel\\\",\\\"wager\\\":\\\"20000\\\"}\""
		protected System.String Parameter2 { get; set; }
		// From : "637186255906016292"
		protected System.Int64 Parameter3 { get; set; }
		// From : "149"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventGameEvent(Dictionary<byte, object> data) : base(EventCodes.GameEvent, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 185
	public class GEventGuildLogoObjectUpdate : BaseEvent
	{
		// From : "268"
		protected System.Int64 Parameter0 { get; set; }
		// From : "7"
		protected System.Int64 Parameter1 { get; set; }
		// From : "9"
		protected System.Int64 Parameter2 { get; set; }
		// From : "2"
		protected System.Int64 Parameter3 { get; set; }
		// From : "8"
		protected System.Int64 Parameter4 { get; set; }
		// From : "15775037"
		protected System.Int64 Parameter5 { get; set; }
		// From : "0.75"
		protected System.Double Parameter6 { get; set; }
		// From : "0.1"
		protected System.Double Parameter7 { get; set; }
		// From : "185"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventGuildLogoObjectUpdate(Dictionary<byte, object> data) : base(EventCodes.GuildLogoObjectUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToDouble();
			Parameter7 = SafeExtract(7).ToDouble();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 177
	public class GEventGuildLogoUpdate : BaseEvent
	{
		// From : "Original \"G8xFBvj8wEGi4nEGJ3nDbw==\" : Converted : [71,56,120,70,66,118,106,56,119,69,71,105,52,110,69,71,74,51,110,68,98,119,61,61]"
		protected System.Int16[] Parameter0 { get; set; }
		// From : "8"
		protected System.Int64 Parameter2 { get; set; }
		// From : "2"
		protected System.Int64 Parameter3 { get; set; }
		// From : "66"
		protected System.Int64 Parameter4 { get; set; }
		// From : "16777215"
		protected System.Int64 Parameter5 { get; set; }
		// From : "0.65000004"
		protected System.Double Parameter6 { get; set; }
		// From : "177"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventGuildLogoUpdate(Dictionary<byte, object> data) : base(EventCodes.GuildLogoUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt16Array();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToDouble();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 127
	public class GEventGuildMemberTerritoryUpdate : BaseEvent
	{
		// From : "\"3a90c837-96de-40e4-95ab-9d19131bfa58@1313\""
		protected System.String Parameter0 { get; set; }
		// From : "true"
		protected System.Boolean Parameter1 { get; set; }
		// From : "127"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventGuildMemberTerritoryUpdate(Dictionary<byte, object> data) : base(EventCodes.GuildMemberTerritoryUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToString();
			Parameter1 = SafeExtract(1).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 95
	public class GEventGuildMemberWorldUpdate : BaseEvent
	{
		// From : "Original \"n/hYzrG/QEKxooIWbbEiBA==\" : Converted : [110,47,104,89,122,114,71,47,81,69,75,120,111,111,73,87,98,98,69,105,66,65,61,61]"
		protected System.Int16[] Parameter0 { get; set; }
		// From : "Original \"vhq+Ww44FEyqgwIvRT7GGQ==\" : Converted : [118,104,113,43,87,119,52,52,70,69,121,113,103,119,73,118,82,84,55,71,71,81,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "63429"
		protected System.Int64 Parameter2 { get; set; }
		// From : "\"Sinister Kings\""
		protected System.String Parameter3 { get; set; }
		// From : "6"
		protected System.Int64 Parameter4 { get; set; }
		// From : "31"
		protected System.Int64 Parameter5 { get; set; }
		// From : "11"
		protected System.Int64 Parameter6 { get; set; }
		// From : "15"
		protected System.Int64 Parameter7 { get; set; }
		// From : "16777215"
		protected System.Int64 Parameter8 { get; set; }
		// From : "0.55"
		protected System.Double Parameter9 { get; set; }
		// From : "Original \"bMcwYf4DDUil7cjRviSGJg==\" : Converted : [98,77,99,119,89,102,52,68,68,85,105,108,55,99,106,82,118,105,83,71,74,103,61,61]"
		protected System.Int16[] Parameter11 { get; set; }
		// From : "\"BACON\""
		protected System.String Parameter12 { get; set; }
		// From : "95"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventGuildMemberWorldUpdate(Dictionary<byte, object> data) : base(EventCodes.GuildMemberWorldUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt16Array();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToInt();
			Parameter9 = SafeExtract(9).ToDouble();
			Parameter11 = SafeExtract(11).ToInt16Array();
			Parameter12 = SafeExtract(12).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 93
	public class GEventGuildPlayerUpdated : BaseEvent
	{
		// From : "Original \"G4zC2YpQcES6ktoOz+1xQA==\" : Converted : [71,52,122,67,50,89,112,81,99,69,83,54,107,116,111,79,122,43,49,120,81,65,61,61]"
		protected System.Int16[] Parameter0 { get; set; }
		// From : "\"Woozyyy\""
		protected System.String Parameter1 { get; set; }
		// From : "true"
		protected System.Boolean Parameter2 { get; set; }
		// From : "637186258760333662"
		protected System.Int64 Parameter3 { get; set; }
		// From : "1"
		protected System.Int64 Parameter4 { get; set; }
		// From : "26"
		protected System.Int64 Parameter5 { get; set; }
		// From : "16"
		protected System.Int64 Parameter6 { get; set; }
		// From : "0"
		protected System.Int64 Parameter8 { get; set; }
		// From : "93"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventGuildPlayerUpdated(Dictionary<byte, object> data) : base(EventCodes.GuildPlayerUpdated, data)
		{
			Parameter0 = SafeExtract(0).ToInt16Array();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToBoolean();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter8 = SafeExtract(8).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 135
	public class GEventGuildStats : BaseEvent
	{
		// From : "950383"
		protected System.Int64 Parameter0 { get; set; }
		// From : "\"Sinister Kings\""
		protected System.String Parameter1 { get; set; }
		// From : "Original \"n/hYzrG/QEKxooIWbbEiBA==\" : Converted : [110,47,104,89,122,114,71,47,81,69,75,120,111,111,73,87,98,98,69,105,66,65,61,61]"
		protected System.Int16[] Parameter2 { get; set; }
		// From : "\"Going HAM\""
		protected System.String Parameter3 { get; set; }
		// From : "180"
		protected System.Int64 Parameter4 { get; set; }
		// From : "\"fivefoot\""
		protected System.String Parameter5 { get; set; }
		// From : "1"
		protected System.Int64 Parameter6 { get; set; }
		// From : "46670087455"
		protected System.Int64 Parameter7 { get; set; }
		// From : "6782939286971"
		protected System.Int64 Parameter8 { get; set; }
		// From : "4789"
		protected System.Int64 Parameter9 { get; set; }
		// From : "6"
		protected System.Int64 Parameter15 { get; set; }
		// From : "936315633705"
		protected System.Int64 Parameter17 { get; set; }
		// From : "5110678240754"
		protected System.Int64 Parameter18 { get; set; }
		// From : "135131265000"
		protected System.Int64 Parameter19 { get; set; }
		// From : "520113961272"
		protected System.Int64 Parameter20 { get; set; }
		// From : "6"
		protected System.Int64 Parameter26 { get; set; }
		// From : "31"
		protected System.Int64 Parameter27 { get; set; }
		// From : "11"
		protected System.Int64 Parameter28 { get; set; }
		// From : "15"
		protected System.Int64 Parameter29 { get; set; }
		// From : "16777215"
		protected System.Int64 Parameter30 { get; set; }
		// From : "0.55"
		protected System.Double Parameter31 { get; set; }
		// From : "6767"
		protected System.Int64 Parameter33 { get; set; }
		// From : "135"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventGuildStats(Dictionary<byte, object> data) : base(EventCodes.GuildStats, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToInt16Array();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToString();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToInt();
			Parameter9 = SafeExtract(9).ToInt();
			Parameter15 = SafeExtract(15).ToInt();
			Parameter17 = SafeExtract(17).ToInt();
			Parameter18 = SafeExtract(18).ToInt();
			Parameter19 = SafeExtract(19).ToInt();
			Parameter20 = SafeExtract(20).ToInt();
			Parameter26 = SafeExtract(26).ToInt();
			Parameter27 = SafeExtract(27).ToInt();
			Parameter28 = SafeExtract(28).ToInt();
			Parameter29 = SafeExtract(29).ToInt();
			Parameter30 = SafeExtract(30).ToInt();
			Parameter31 = SafeExtract(31).ToDouble();
			Parameter33 = SafeExtract(33).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 92
	public class GEventGuildUpdate : BaseEvent
	{
		// From : "2"
		protected System.Int64 Parameter9 { get; set; }
		// From : "46675913795"
		protected System.Int64 Parameter10 { get; set; }
		// From : "800"
		protected System.Int64 Parameter13 { get; set; }
		// From : "\"BACON\""
		protected System.String Parameter15 { get; set; }
		// From : "Original \"bMcwYf4DDUil7cjRviSGJg==\" : Converted : [98,77,99,119,89,102,52,68,68,85,105,108,55,99,106,82,118,105,83,71,74,103,61,61]"
		protected System.Int16[] Parameter16 { get; set; }
		// From : "{\"0\":68300000}"
		protected System.Object Parameter18 { get; set; }
		// From : "6"
		protected System.Int64 Parameter19 { get; set; }
		// From : "31"
		protected System.Int64 Parameter20 { get; set; }
		// From : "11"
		protected System.Int64 Parameter21 { get; set; }
		// From : "15"
		protected System.Int64 Parameter22 { get; set; }
		// From : "16777215"
		protected System.Int64 Parameter23 { get; set; }
		// From : "0.55"
		protected System.Double Parameter24 { get; set; }
		// From : "6767"
		protected System.Int64 Parameter27 { get; set; }
		// From : "0"
		protected System.Int64 Parameter29 { get; set; }
		// From : "92"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventGuildUpdate(Dictionary<byte, object> data) : base(EventCodes.GuildUpdate, data)
		{
			Parameter9 = SafeExtract(9).ToInt();
			Parameter10 = SafeExtract(10).ToInt();
			Parameter13 = SafeExtract(13).ToInt();
			Parameter15 = SafeExtract(15).ToString();
			Parameter16 = SafeExtract(16).ToInt16Array();
			Parameter18 = SafeExtract(18);
			Parameter19 = SafeExtract(19).ToInt();
			Parameter20 = SafeExtract(20).ToInt();
			Parameter21 = SafeExtract(21).ToInt();
			Parameter22 = SafeExtract(22).ToInt();
			Parameter23 = SafeExtract(23).ToInt();
			Parameter24 = SafeExtract(24).ToDouble();
			Parameter27 = SafeExtract(27).ToInt();
			Parameter29 = SafeExtract(29).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 60
	public class GEventGvgSeasonUpdate : BaseEvent
	{
		// From : "8"
		protected System.Int64 Parameter0 { get; set; }
		// From : "7"
		protected System.Int64 Parameter1 { get; set; }
		// From : "Original \"7d9vs7I27EKHradQpe+LDg==\" : Converted : [55,100,57,118,115,55,73,50,55,69,75,72,114,97,100,81,112,101,43,76,68,103,61,61]"
		protected System.Int16[] Parameter2 { get; set; }
		// From : "\"ERROR 4O4\""
		protected System.String Parameter3 { get; set; }
		// From : "Original \"wqgahVsvDk+o44R0bsmIUw==\" : Converted : [119,113,103,97,104,86,115,118,68,107,43,111,52,52,82,48,98,115,109,73,85,119,61,61]"
		protected System.Int16[] Parameter4 { get; set; }
		// From : "\"EQMS Praise Be Archersnon\""
		protected System.String Parameter5 { get; set; }
		// From : "\"ARCH\""
		protected System.String Parameter6 { get; set; }
		// From : "2"
		protected System.Int64 Parameter8 { get; set; }
		// From : "5"
		protected System.Int64 Parameter9 { get; set; }
		// From : "82"
		protected System.Int64 Parameter10 { get; set; }
		// From : "13483934"
		protected System.Int64 Parameter11 { get; set; }
		// From : "0.7"
		protected System.Double Parameter12 { get; set; }
		// From : "0.2"
		protected System.Double Parameter13 { get; set; }
		// From : "300404"
		protected System.Int64 Parameter14 { get; set; }
		// From : "1"
		protected System.Int64 Parameter15 { get; set; }
		// From : "316"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventGvgSeasonUpdate(Dictionary<byte, object> data) : base(EventCodes.GvgSeasonUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt16Array();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToInt16Array();
			Parameter5 = SafeExtract(5).ToString();
			Parameter6 = SafeExtract(6).ToString();
			Parameter8 = SafeExtract(8).ToInt();
			Parameter9 = SafeExtract(9).ToInt();
			Parameter10 = SafeExtract(10).ToInt();
			Parameter11 = SafeExtract(11).ToInt();
			Parameter12 = SafeExtract(12).ToDouble();
			Parameter13 = SafeExtract(13).ToDouble();
			Parameter14 = SafeExtract(14).ToInt();
			Parameter15 = SafeExtract(15).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 35
	public class GEventHarvestableChangeState : BaseEvent
	{
		// From : "7416"
		protected System.Int64 Parameter0 { get; set; }
		// From : "0"
		protected System.Int64 Parameter2 { get; set; }
		// From : "35"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventHarvestableChangeState(Dictionary<byte, object> data) : base(EventCodes.HarvestableChangeState, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 49
	public class GEventHarvestCancel : BaseEvent
	{
		// From : "124900"
		protected System.Int64 Parameter0 { get; set; }
		// From : "637186175439695051"
		protected System.Int64 Parameter1 { get; set; }
		// From : "49"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventHarvestCancel(Dictionary<byte, object> data) : base(EventCodes.HarvestCancel, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 50
	public class GEventHarvestFinished : BaseEvent
	{
		// From : "953315"
		protected System.Int64 Parameter0 { get; set; }
		// From : "637186256553165051"
		protected System.Int64 Parameter1 { get; set; }
		// From : "637186256592972715"
		protected System.Int64 Parameter2 { get; set; }
		// From : "7416"
		protected System.Int64 Parameter3 { get; set; }
		// From : "2"
		protected System.Int64 Parameter4 { get; set; }
		// From : "1"
		protected System.Int64 Parameter6 { get; set; }
		// From : "1"
		protected System.Int64 Parameter7 { get; set; }
		// From : "[]"
		protected System.Single[] Parameter8 { get; set; }
		// From : "\"\""
		protected System.String Parameter9 { get; set; }
		// From : "50"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventHarvestFinished(Dictionary<byte, object> data) : base(EventCodes.HarvestFinished, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToFloatArray();
			Parameter9 = SafeExtract(9).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 48
	public class GEventHarvestStart : BaseEvent
	{
		// From : "953315"
		protected System.Int64 Parameter0 { get; set; }
		// From : "637186256593005051"
		protected System.Int64 Parameter1 { get; set; }
		// From : "637186256593005051"
		protected System.Int64 Parameter2 { get; set; }
		// From : "7416"
		protected System.Int64 Parameter3 { get; set; }
		// From : "8"
		protected System.Int64 Parameter4 { get; set; }
		// From : "3.98"
		protected System.Double Parameter5 { get; set; }
		// From : "953349"
		protected System.Int64 Parameter6 { get; set; }
		// From : "1822"
		protected System.Int64 Parameter7 { get; set; }
		// From : "48"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventHarvestStart(Dictionary<byte, object> data) : base(EventCodes.HarvestStart, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToDouble();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 6
	public class GEventHealthUpdate : BaseEvent
	{
		// From : "953314"
		protected System.Int64 Parameter0 { get; set; }
		// From : "57052089"
		protected System.Int64 Parameter1 { get; set; }
		// From : "-66.0"
		protected System.Double Parameter2 { get; set; }
		// From : "1543.0"
		protected System.Double Parameter3 { get; set; }
		// From : "1"
		protected System.Int64 Parameter4 { get; set; }
		// From : "0"
		protected System.Int64 Parameter5 { get; set; }
		// From : "948510"
		protected System.Int64 Parameter6 { get; set; }
		// From : "-1"
		protected System.Int64 Parameter7 { get; set; }
		// From : "6"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventHealthUpdate(Dictionary<byte, object> data) : base(EventCodes.HealthUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 251
	public class GEventInCombatStateUpdate : BaseEvent
	{
		// From : "953314"
		protected System.Int64 Parameter0 { get; set; }
		// From : "251"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventInCombatStateUpdate(Dictionary<byte, object> data) : base(EventCodes.InCombatStateUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 23
	public class GEventInventoryDeleteItem : BaseEvent
	{
		// From : "2505808"
		protected System.Int64 Parameter0 { get; set; }
		// From : "2"
		protected System.Int64 Parameter1 { get; set; }
		// From : "23"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventInventoryDeleteItem(Dictionary<byte, object> data) : base(EventCodes.InventoryDeleteItem, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 22
	public class GEventInventoryPutItem : BaseEvent
	{
		// From : "342"
		protected System.Int64 Parameter0 { get; set; }
		// From : "4"
		protected System.Int64 Parameter1 { get; set; }
		// From : "Original \"5gLc6K9WLk+4JYrAQmgLug==\" : Converted : [53,103,76,99,54,75,57,87,76,107,43,52,74,89,114,65,81,109,103,76,117,103,61,61]"
		protected System.Int16[] Parameter2 { get; set; }
		// From : "22"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventInventoryPutItem(Dictionary<byte, object> data) : base(EventCodes.InventoryPutItem, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 57
	public class GEventItemRerollQualityFinished : BaseEvent
	{
		// From : "2508239"
		protected System.Int64 Parameter0 { get; set; }
		// From : "56934583"
		protected System.Int64 Parameter1 { get; set; }
		// From : "1750"
		protected System.Int64 Parameter2 { get; set; }
		// From : "-1"
		protected System.Int64 Parameter3 { get; set; }
		// From : "57"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventItemRerollQualityFinished(Dictionary<byte, object> data) : base(EventCodes.ItemRerollQualityFinished, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 55
	public class GEventItemRerollQualityStart : BaseEvent
	{
		// From : "2508239"
		protected System.Int64 Parameter0 { get; set; }
		// From : "56934433"
		protected System.Int64 Parameter1 { get; set; }
		// From : "56934583"
		protected System.Int64 Parameter2 { get; set; }
		// From : "1750"
		protected System.Int64 Parameter3 { get; set; }
		// From : "55"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventItemRerollQualityStart(Dictionary<byte, object> data) : base(EventCodes.ItemRerollQualityStart, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 2
	public class GEventJoinFinished : BaseEvent
	{
		// From : "2"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventJoinFinished(Dictionary<byte, object> data) : base(EventCodes.JoinFinished, data)
		{
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 150
	public class GEventKilledPlayer : BaseEvent
	{
		// From : "77485"
		protected System.Int64 Parameter0 { get; set; }
		// From : "77485"
		protected System.Int64 Parameter1 { get; set; }
		// From : "\"Palurdo\""
		protected System.String Parameter2 { get; set; }
		// From : "1132378472"
		protected System.Int64 Parameter3 { get; set; }
		// From : "150"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventKilledPlayer(Dictionary<byte, object> data) : base(EventCodes.KilledPlayer, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 152
	public class GEventKnockedDown : BaseEvent
	{
		// From : "2470439"
		protected System.Int64 Parameter0 { get; set; }
		// From : "56601530"
		protected System.Int64 Parameter1 { get; set; }
		// From : "2456241"
		protected System.Int64 Parameter2 { get; set; }
		// From : "\"Treewok\""
		protected System.String Parameter3 { get; set; }
		// From : "true"
		protected System.Boolean Parameter5 { get; set; }
		// From : "152"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventKnockedDown(Dictionary<byte, object> data) : base(EventCodes.KnockedDown, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToString();
			Parameter5 = SafeExtract(5).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 1
	public class GEventLeave : BaseEvent
	{
		// From : "346"
		protected System.Int64 Parameter0 { get; set; }
		// From : "1"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventLeave(Dictionary<byte, object> data) : base(EventCodes.Leave, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 202
	public class GEventLootEquipmentChanged : BaseEvent
	{
		// From : "2355815"
		protected System.Int64 Parameter0 { get; set; }
		// From : "Original \"AAAAAAAAAAAAAA==\" : Converted : [65,65,65,65,65,65,65,65,65,65,65,65,65,65,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "202"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventLootEquipmentChanged(Dictionary<byte, object> data) : base(EventCodes.LootEquipmentChanged, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 165
	public class GEventMiniMapPing : BaseEvent
	{
		// From : "[115.701004,-122.29211]"
		protected System.Single[] Parameter0 { get; set; }
		// From : "0"
		protected System.Int64 Parameter1 { get; set; }
		// From : "165"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventMiniMapPing(Dictionary<byte, object> data) : base(EventCodes.MiniMapPing, data)
		{
			Parameter0 = SafeExtract(0).ToFloatArray();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 45
	public class GEventMinimapZergs : BaseEvent
	{
		// From : "[350.7475,4.4990754]"
		protected System.Single[] Parameter0 { get; set; }
		// From : "Original \"Dg==\" : Converted : [68,103,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "Original \"/w==\" : Converted : [47,119,61,61]"
		protected System.Int16[] Parameter2 { get; set; }
		// From : "301"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventMinimapZergs(Dictionary<byte, object> data) : base(EventCodes.MinimapZergs, data)
		{
			Parameter0 = SafeExtract(0).ToFloatArray();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 36
	public class GEventMobChangeState : BaseEvent
	{
		// From : "942872"
		protected System.Int64 Parameter0 { get; set; }
		// From : "0"
		protected System.Int64 Parameter1 { get; set; }
		// From : "36"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventMobChangeState(Dictionary<byte, object> data) : base(EventCodes.MobChangeState, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 194
	public class GEventMountCancel : BaseEvent
	{
		// From : "1154752"
		protected System.Int64 Parameter0 { get; set; }
		// From : "194"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventMountCancel(Dictionary<byte, object> data) : base(EventCodes.MountCancel, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 30
	public class GEventMountCooldownUpdate : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "637186257248271022"
		protected System.Int64 Parameter1 { get; set; }
		// From : "58295971"
		protected System.Int64 Parameter2 { get; set; }
		// From : "286"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventMountCooldownUpdate(Dictionary<byte, object> data) : base(EventCodes.MountCooldownUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 192
	public class GEventMounted : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "57168364"
		protected System.Int64 Parameter1 { get; set; }
		// From : "527.0"
		protected System.Double Parameter4 { get; set; }
		// From : "6.69"
		protected System.Double Parameter5 { get; set; }
		// From : "637186257172905872"
		protected System.Int64 Parameter6 { get; set; }
		// From : "true"
		protected System.Boolean Parameter7 { get; set; }
		// From : "true"
		protected System.Boolean Parameter8 { get; set; }
		// From : "104.69832"
		protected System.Double Parameter10 { get; set; }
		// From : "192"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventMounted(Dictionary<byte, object> data) : base(EventCodes.Mounted, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToDouble();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToBoolean();
			Parameter8 = SafeExtract(8).ToBoolean();
			Parameter10 = SafeExtract(10).ToDouble();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 29
	public class GEventMountHealthUpdate : BaseEvent
	{
		// From : "953314"
		protected System.Int64 Parameter0 { get; set; }
		// From : "57111404"
		protected System.Int64 Parameter1 { get; set; }
		// From : "-77.0"
		protected System.Double Parameter2 { get; set; }
		// From : "202.0"
		protected System.Double Parameter3 { get; set; }
		// From : "1"
		protected System.Int64 Parameter4 { get; set; }
		// From : "2"
		protected System.Int64 Parameter5 { get; set; }
		// From : "949133"
		protected System.Int64 Parameter6 { get; set; }
		// From : "2865"
		protected System.Int64 Parameter7 { get; set; }
		// From : "285"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventMountHealthUpdate(Dictionary<byte, object> data) : base(EventCodes.MountHealthUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 193
	public class GEventMountStart : BaseEvent
	{
		// From : "2518566"
		protected System.Int64 Parameter0 { get; set; }
		// From : "57143494"
		protected System.Int64 Parameter1 { get; set; }
		// From : "1927"
		protected System.Int64 Parameter2 { get; set; }
		// From : "true"
		protected System.Boolean Parameter3 { get; set; }
		// From : "193"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventMountStart(Dictionary<byte, object> data) : base(EventCodes.MountStart, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 106
	public class GEventMutePlayerUpdate : BaseEvent
	{
		// From : "0"
		protected System.Int64 Parameter1 { get; set; }
		// From : "0"
		protected System.Int64 Parameter2 { get; set; }
		// From : "[\"Sarkoso\"]"
		protected System.String[] Parameter3 { get; set; }
		// From : "362"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventMutePlayerUpdate(Dictionary<byte, object> data) : base(EventCodes.MutePlayerUpdate, data)
		{
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToStringArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 34
	public class GEventNewBuilding : BaseEvent
	{
		// From : "268"
		protected System.Int64 Parameter0 { get; set; }
		// From : "Original \"+AWVJYXw+U6TWjQDyrymJA==\" : Converted : [43,65,87,86,74,89,88,119,43,85,54,84,87,106,81,68,121,114,121,109,74,65,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "592"
		protected System.Int64 Parameter2 { get; set; }
		// From : "\"UNIQUE_FURNITUREITEM_KNIGHT_THRONE_A\""
		protected System.String Parameter3 { get; set; }
		// From : "[214.0,86.0]"
		protected System.Single[] Parameter4 { get; set; }
		// From : "90.0"
		protected System.Double Parameter5 { get; set; }
		// From : "Original \"CcxeEDirHk+YRdRgo8iwPg==\" : Converted : [67,99,120,101,69,68,105,114,72,107,43,89,82,100,82,103,111,56,105,119,80,103,61,61]"
		protected System.Int16[] Parameter6 { get; set; }
		// From : "Original \"n/hYzrG/QEKxooIWbbEiBA==\" : Converted : [110,47,104,89,122,114,71,47,81,69,75,120,111,111,73,87,98,98,69,105,66,65,61,61]"
		protected System.Int16[] Parameter7 { get; set; }
		// From : "\"System\""
		protected System.String Parameter8 { get; set; }
		// From : "\"Palurdo\""
		protected System.String Parameter9 { get; set; }
		// From : "2700000"
		protected System.Int64 Parameter16 { get; set; }
		// From : "636971035762547209"
		protected System.Int64 Parameter17 { get; set; }
		// From : "637186256926666026"
		protected System.Int64 Parameter19 { get; set; }
		// From : "-1"
		protected System.Int64 Parameter20 { get; set; }
		// From : "637186256926666026"
		protected System.Int64 Parameter21 { get; set; }
		// From : "637186256926666026"
		protected System.Int64 Parameter23 { get; set; }
		// From : "true"
		protected System.Boolean Parameter26 { get; set; }
		// From : "0"
		protected System.Int64 Parameter30 { get; set; }
		// From : "34"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewBuilding(Dictionary<byte, object> data) : base(EventCodes.NewBuilding, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToFloatArray();
			Parameter5 = SafeExtract(5).ToDouble();
			Parameter6 = SafeExtract(6).ToInt16Array();
			Parameter7 = SafeExtract(7).ToInt16Array();
			Parameter8 = SafeExtract(8).ToString();
			Parameter9 = SafeExtract(9).ToString();
			Parameter16 = SafeExtract(16).ToInt();
			Parameter17 = SafeExtract(17).ToInt();
			Parameter19 = SafeExtract(19).ToInt();
			Parameter20 = SafeExtract(20).ToInt();
			Parameter21 = SafeExtract(21).ToInt();
			Parameter23 = SafeExtract(23).ToInt();
			Parameter26 = SafeExtract(26).ToBoolean();
			Parameter30 = SafeExtract(30).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 103
	public class GEventNewChainSpell : BaseEvent
	{
		// From : "954800"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[346.02,129.03]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "155"
		protected System.Int64 Parameter2 { get; set; }
		// From : "0"
		protected System.Int64 Parameter3 { get; set; }
		// From : "10"
		protected System.Int64 Parameter4 { get; set; }
		// From : "953689"
		protected System.Int64 Parameter5 { get; set; }
		// From : "57039255"
		protected System.Int64 Parameter6 { get; set; }
		// From : "[950730]"
		protected System.Int64[] Parameter7 { get; set; }
		// From : "103"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewChainSpell(Dictionary<byte, object> data) : base(EventCodes.NewChainSpell, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 24
	public class GEventNewCharacter : BaseEvent
	{
		// From : "2519206"
		protected System.Int64 Parameter0 { get; set; }
		// From : "\"Ciebel\""
		protected System.String Parameter1 { get; set; }
		// From : "28"
		protected System.Int64 Parameter2 { get; set; }
		// From : "1"
		protected System.Int64 Parameter4 { get; set; }
		// From : "Original \"AQABAQ==\" : Converted : [65,81,65,66,65,81,61,61]"
		protected System.Int16[] Parameter5 { get; set; }
		// From : "Original \"AAQKBQ==\" : Converted : [65,65,81,75,66,81,61,61]"
		protected System.Int16[] Parameter6 { get; set; }
		// From : "Original \"qf+5ObfX706tsmi6QVbZZw==\" : Converted : [113,102,43,53,79,98,102,88,55,48,54,116,115,109,105,54,81,86,98,90,90,119,61,61]"
		protected System.Int16[] Parameter7 { get; set; }
		protected System.String Parameter8 { get; set; }
		protected System.Int16[] Parameter9 { get; set; }

		// From : "\"\""
		protected System.Int64[] Parameter10 { get; set; }
		// From : "\"\""
		protected System.String Parameter11 { get; set; }
		// From : "[-20.0,-20.0]"
		protected System.Single[] Parameter12 { get; set; }
		// From : "[-20.0,-20.0]"
		protected System.Single[] Parameter13 { get; set; }
		// From : "57143478"
		protected System.Int64 Parameter14 { get; set; }
		// From : "180.0"
		protected System.Double Parameter15 { get; set; }
		// From : "9.075"
		protected System.Double Parameter16 { get; set; }
		// From : "2170.0"
		protected System.Double Parameter18 { get; set; }
		// From : "2170.0"
		protected System.Double Parameter19 { get; set; }
		// From : "144.49364"
		protected System.Double Parameter20 { get; set; }
		// From : "57143479"
		protected System.Int64 Parameter21 { get; set; }
		// From : "312.0"
		protected System.Double Parameter22 { get; set; }
		// From : "312.0"
		protected System.Double Parameter23 { get; set; }
		// From : "4.374981"
		protected System.Double Parameter24 { get; set; }
		// From : "57143478"
		protected System.Int64 Parameter25 { get; set; }
		// From : "669.0"
		protected System.Double Parameter26 { get; set; }
		// From : "669.0"
		protected System.Double Parameter27 { get; set; }
		// From : "6.69"
		protected System.Double Parameter28 { get; set; }
		// From : "57143478"
		protected System.Int64 Parameter29 { get; set; }
		// From : "12187.346"
		protected System.Double Parameter30 { get; set; }
		// From : "637135093361358065"
		protected System.Int64 Parameter31 { get; set; }
		// From : "0"
		protected System.Int64 Parameter32 { get; set; }
		// From : "[4977,1407,3015,3037,3119,1794,1742,1895,0,0]"
		protected System.Int64[] Parameter33 { get; set; }
		// From : "[1155,1156,1174,1704,1789,1847,-1,-1,-1,-1,-1,-1,2209,1980]"
		protected System.Int64[] Parameter35 { get; set; }
		// From : "\"\""
		protected System.String Parameter43 { get; set; }
		// From : "0"
		protected System.Int64 Parameter45 { get; set; }
		// From : "[-1,-1,25,55,73,-1,81]"
		protected System.Int64[] Parameter48 { get; set; }
		// From : "24"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewCharacter(Dictionary<byte, object> data) : base(EventCodes.NewCharacter, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt16Array();
			Parameter6 = SafeExtract(6).ToInt16Array();
			Parameter7 = SafeExtract(7).ToInt16Array();

			if (SafeExtract(8) == null)
				Parameter8 = "";
			else
				Parameter8 = SafeExtract(8).ToString();


			if (SafeExtract(9) == null)
				Parameter9 = new short[0];
			else
				Parameter9 = SafeExtract(9).ToInt16Array();

			if (SafeExtract(10) != "")
            {
				var ea = SafeExtract(10);
				try { Parameter10 = SafeExtract(10).ToIntArray(); }
				catch (InvalidCastException ex)
				{
					short[] a = SafeExtract(10).ToInt16Array();
					Parameter10 = a.ToList().Select(item => (long)item).ToArray();
				}
			}
			else
				Parameter10 = new long[0];

				
			Parameter11 = SafeExtract(11).ToString();
			Parameter12 = SafeExtract(12).ToFloatArray();
			Parameter13 = SafeExtract(13).ToFloatArray();
			Parameter14 = SafeExtract(14).ToInt();
			Parameter15 = SafeExtract(15).ToDouble();
			Parameter16 = SafeExtract(16).ToDouble();
			Parameter18 = SafeExtract(18).ToDouble();
			Parameter19 = SafeExtract(19).ToDouble();
			Parameter20 = SafeExtract(20).ToDouble();
			Parameter21 = SafeExtract(21).ToInt();
			Parameter22 = SafeExtract(22).ToDouble();
			Parameter23 = SafeExtract(23).ToDouble();
			Parameter24 = SafeExtract(24).ToDouble();
			Parameter25 = SafeExtract(25).ToInt();
			Parameter26 = SafeExtract(26).ToDouble();
			Parameter27 = SafeExtract(27).ToDouble();
			Parameter28 = SafeExtract(28).ToDouble();
			Parameter29 = SafeExtract(29).ToInt();
			Parameter30 = SafeExtract(30).ToDouble();
			Parameter31 = SafeExtract(31).ToInt();
			Parameter32 = SafeExtract(32).ToInt();
            try
            {
				Parameter33 = SafeExtract(33).ToIntArray();
			}catch(InvalidCastException ex)
            {
				short[] a = SafeExtract(33).ToInt16Array();
				Parameter33 = a.ToList().Select(item => (long)item).ToArray();
			}
			Parameter43 = SafeExtract(43).ToString();
			Parameter45 = SafeExtract(45).ToInt();
			Parameter48 = SafeExtract(48).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 168
	public class GEventNewDuellingPost : BaseEvent
	{
		// From : "2510621"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[-53.3927,23.496984]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "168"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewDuellingPost(Dictionary<byte, object> data) : base(EventCodes.NewDuellingPost, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 97
	public class GEventNewDynamicGuildLogo : BaseEvent
	{
		// From : "\"GVG_SEASON_07#3\""
		protected System.String Parameter0 { get; set; }
		// From : "\"SCHEMA_01\""
		protected System.String Parameter1 { get; set; }
		// From : "\"GUILDSYMBOL_EOS\""
		protected System.String Parameter2 { get; set; }
		// From : "9"
		protected System.Int64 Parameter3 { get; set; }
		// From : "7"
		protected System.Int64 Parameter4 { get; set; }
		// From : "15775037"
		protected System.Int64 Parameter5 { get; set; }
		// From : "0.5"
		protected System.Double Parameter6 { get; set; }
		// From : "-0.05"
		protected System.Double Parameter7 { get; set; }
		// From : "true"
		protected System.Boolean Parameter8 { get; set; }
		// From : "353"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewDynamicGuildLogo(Dictionary<byte, object> data) : base(EventCodes.NewDynamicGuildLogo, data)
		{
			Parameter0 = SafeExtract(0).ToString();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToDouble();
			Parameter7 = SafeExtract(7).ToDouble();
			Parameter8 = SafeExtract(8).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 25
	public class GEventNewEquipmentItem : BaseEvent
	{
		// From : "327"
		protected System.Int64 Parameter0 { get; set; }
		// From : "1883"
		protected System.Int64 Parameter1 { get; set; }
		// From : "1"
		protected System.Int64 Parameter2 { get; set; }
		// From : "\"\""
		protected System.String Parameter3 { get; set; }
		// From : "2"
		protected System.Int64 Parameter4 { get; set; }
		// From : "2980000"
		protected System.Int64 Parameter5 { get; set; }
		// From : "\"\""
		protected System.String Parameter6 { get; set; }
		// From : "\"\""
		protected System.String Parameter7 { get; set; }
		// From : "25"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewEquipmentItem(Dictionary<byte, object> data) : base(EventCodes.NewEquipmentItem, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToString();
			Parameter7 = SafeExtract(7).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 197
	public class GEventNewExit : BaseEvent
	{
		// From : "2"
		protected System.Int64 Parameter0 { get; set; }
		// From : "Original \"tKxLQS76cESjw6j8jr6CVA==\" : Converted : [116,75,120,76,81,83,55,54,99,69,83,106,119,54,106,56,106,114,54,67,86,65,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "[-20.0,-10.0]"
		protected System.Single[] Parameter2 { get; set; }
		// From : "197"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewExit(Dictionary<byte, object> data) : base(EventCodes.NewExit, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 31
	public class GEventNewExpeditionAgent : BaseEvent
	{
		// From : "22"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[18.5,-26.5]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "Original \"lvXJo5hQ9EyHeExJbBb2Tg==\" : Converted : [108,118,88,74,111,53,104,81,57,69,121,72,101,69,120,74,98,66,98,50,84,103,61,61]"
		protected System.Int16[] Parameter2 { get; set; }
		// From : "4"
		protected System.Int64 Parameter3 { get; set; }
		// From : "\"Recruiter Parker\""
		protected System.String Parameter4 { get; set; }
		// From : "287"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewExpeditionAgent(Dictionary<byte, object> data) : base(EventCodes.NewExpeditionAgent, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToInt16Array();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 69
	public class GEventNewFishingZoneObject : BaseEvent
	{
		// From : "253"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[208.0,31.1]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "4"
		protected System.Int64 Parameter2 { get; set; }
		// From : "1"
		protected System.Int64 Parameter3 { get; set; }
		// From : "\"FishingNodeOceanSwarm\""
		protected System.String Parameter4 { get; set; }
		// From : "325"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewFishingZoneObject(Dictionary<byte, object> data) : base(EventCodes.NewFishingZoneObject, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 68
	public class GEventNewFloatObject : BaseEvent
	{
		// From : "346"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[210.0373,29.078363]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "147.27275"
		protected System.Double Parameter2 { get; set; }
		// From : "325"
		protected System.Int64 Parameter3 { get; set; }
		// From : "5"
		protected System.Int64 Parameter4 { get; set; }
		// From : "324"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewFloatObject(Dictionary<byte, object> data) : base(EventCodes.NewFloatObject, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 27
	public class GEventNewFurnitureItem : BaseEvent
	{
		// From : "1550129"
		protected System.Int64 Parameter0 { get; set; }
		// From : "1980"
		protected System.Int64 Parameter1 { get; set; }
		// From : "1"
		protected System.Int64 Parameter2 { get; set; }
		// From : "\"SYSTEM\""
		protected System.String Parameter3 { get; set; }
		// From : "2700000"
		protected System.Int64 Parameter4 { get; set; }
		// From : "27"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewFurnitureItem(Dictionary<byte, object> data) : base(EventCodes.NewFurnitureItem, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 32
	public class GEventNewHarvestableObject : BaseEvent
	{
		// From : "949864"
		protected System.Int64 Parameter0 { get; set; }
		// From : "947913"
		protected System.Int64 Parameter1 { get; set; }
		// From : "637186252465102788"
		protected System.Int64 Parameter2 { get; set; }
		// From : "Original \"H28okCwsDkKlZlDJ1bbLlQ==\" : Converted : [72,50,56,111,107,67,119,115,68,107,75,108,90,108,68,74,49,98,98,76,108,81,61,61]"
		protected System.Int16[] Parameter3 { get; set; }
		// From : "23"
		protected System.Int64 Parameter5 { get; set; }
		// From : "31"
		protected System.Int64 Parameter6 { get; set; }
		// From : "4"
		protected System.Int64 Parameter7 { get; set; }
		// From : "[318.0701,-41.571766]"
		protected System.Single[] Parameter8 { get; set; }
		// From : "132.68182"
		protected System.Double Parameter9 { get; set; }
		protected System.Int64 Parameter10 { get; set; }
		// From : "0"
		protected System.Int64 Parameter11 { get; set; }
		// From : "32"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewHarvestableObject(Dictionary<byte, object> data) : base(EventCodes.NewHarvestableObject, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			if (SafeExtract(3) == null) { Parameter3 = null; } else { Parameter3 = SafeExtract(3).ToInt16Array(); }
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToFloatArray();
			Parameter9 = SafeExtract(9).ToDouble();
			if (SafeExtract(10) == null) { Parameter10 = 0; } else { Parameter10 = SafeExtract(10).ToInt(); }
			Parameter11 = SafeExtract(11).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 120
	public class GEventNewHideoutObject : BaseEvent
	{
		// From : "4978"
		protected System.Int64 Parameter0 { get; set; }
		// From : "Original \"iieNlK5l2EGtpzEpM2ZoMw==\" : Converted : [105,105,101,78,108,75,53,108,50,69,71,116,112,122,69,112,77,50,90,111,77,119,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "\"HIDEOUT_STEPPE_DEAD\""
		protected System.String Parameter3 { get; set; }
		// From : "[-300.0,120.0]"
		protected System.Single[] Parameter4 { get; set; }
		// From : "Original \"bv1FiTkzxU+l25XQU+eEnA==\" : Converted : [98,118,49,70,105,84,107,122,120,85,43,108,50,53,88,81,85,43,101,69,110,65,61,61]"
		protected System.Int16[] Parameter6 { get; set; }
		// From : "\"Killer Instinct\""
		protected System.String Parameter7 { get; set; }
		// From : "Original \"qExXyPnpkEiPNKoSR0oUVQ==\" : Converted : [113,69,120,88,121,80,110,112,107,69,105,80,78,75,111,83,82,48,111,85,86,81,61,61]"
		protected System.Int16[] Parameter8 { get; set; }
		// From : "\"RANG\""
		protected System.String Parameter9 { get; set; }
		// From : "\"Leyos\""
		protected System.String Parameter10 { get; set; }
		// From : "637162173444558873"
		protected System.Int64 Parameter11 { get; set; }
		// From : "1"
		protected System.Int64 Parameter12 { get; set; }
		// From : "4"
		protected System.Int64 Parameter13 { get; set; }
		// From : "2"
		protected System.Int64 Parameter14 { get; set; }
		// From : "0"
		protected System.Int64 Parameter16 { get; set; }
		// From : "637185504000000000"
		protected System.Int64 Parameter17 { get; set; }
		// From : "637166525248308359"
		protected System.Int64 Parameter19 { get; set; }
		// From : "9223372036854775807"
		protected System.Int64 Parameter20 { get; set; }
		// From : "637185687308970016"
		protected System.Int64 Parameter21 { get; set; }
		// From : "376"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewHideoutObject(Dictionary<byte, object> data) : base(EventCodes.NewHideoutObject, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToFloatArray();
			Parameter6 = SafeExtract(6).ToInt16Array();
			Parameter7 = SafeExtract(7).ToString();
			Parameter8 = SafeExtract(8).ToInt16Array();
			Parameter9 = SafeExtract(9).ToString();
			Parameter10 = SafeExtract(10).ToString();
			Parameter11 = SafeExtract(11).ToInt();
			Parameter12 = SafeExtract(12).ToInt();
			Parameter13 = SafeExtract(13).ToInt();
			Parameter14 = SafeExtract(14).ToInt();
			Parameter16 = SafeExtract(16).ToInt();
			Parameter17 = SafeExtract(17).ToInt();
			Parameter19 = SafeExtract(19).ToInt();
			Parameter20 = SafeExtract(20).ToInt();
			Parameter21 = SafeExtract(21).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 96
	public class GEventNewInformationProvider : BaseEvent
	{
		// From : "25"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[-28.0,60.0]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "\"@HALLOFFAME_SEASONSTORY_07\""
		protected System.String Parameter2 { get; set; }
		// From : "8.0"
		protected System.Double Parameter3 { get; set; }
		// From : "7"
		protected System.Int64 Parameter5 { get; set; }
		// From : "352"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewInformationProvider(Dictionary<byte, object> data) : base(EventCodes.NewInformationProvider, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 196
	public class GEventNewIslandAccessPoint : BaseEvent
	{
		// From : "15"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[-53.0,0.0]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "90.0"
		protected System.Double Parameter2 { get; set; }
		// From : "Original \"8QeoNME66Eah2RhqVrBEPg==\" : Converted : [56,81,101,111,78,77,69,54,54,69,97,104,50,82,104,113,86,114,66,69,80,103,61,61]"
		protected System.Int16[] Parameter3 { get; set; }
		// From : "196"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewIslandAccessPoint(Dictionary<byte, object> data) : base(EventCodes.NewIslandAccessPoint, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 207
	public class GEventNewIslandManagement : BaseEvent
	{
		// From : "260"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[176.5,65.5]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "Original \"CcxeEDirHk+YRdRgo8iwPg==\" : Converted : [67,99,120,101,69,68,105,114,72,107,43,89,82,100,82,103,111,56,105,119,80,103,61,61]"
		protected System.Int16[] Parameter2 { get; set; }
		// From : "Original \"n/hYzrG/QEKxooIWbbEiBA==\" : Converted : [110,47,104,89,122,114,71,47,81,69,75,120,111,111,73,87,98,98,69,105,66,65,61,61]"
		protected System.Int16[] Parameter3 { get; set; }
		// From : "\"Palurdo\""
		protected System.String Parameter4 { get; set; }
		// From : "207"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewIslandManagement(Dictionary<byte, object> data) : base(EventCodes.NewIslandManagement, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToInt16Array();
			Parameter3 = SafeExtract(3).ToInt16Array();
			Parameter4 = SafeExtract(4).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 28
	public class GEventNewJournalItem : BaseEvent
	{
		// From : "1550102"
		protected System.Int64 Parameter0 { get; set; }
		// From : "4101"
		protected System.Int64 Parameter1 { get; set; }
		// From : "1"
		protected System.Int64 Parameter2 { get; set; }
		// From : "\"DarkolokinhoMeo\""
		protected System.String Parameter3 { get; set; }
		// From : "10000"
		protected System.Int64 Parameter4 { get; set; }
		// From : "13950000"
		protected System.Int64 Parameter6 { get; set; }
		// From : "28"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewJournalItem(Dictionary<byte, object> data) : base(EventCodes.NewJournalItem, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 87
	public class GEventNewLoot : BaseEvent
	{
		// From : "2459106"
		protected System.Int64 Parameter0 { get; set; }
		// From : "1"
		protected System.Int64 Parameter1 { get; set; }
		// From : "2458698"
		protected System.Int64 Parameter2 { get; set; }
		// From : "\"PurpleNChill\""
		protected System.String Parameter3 { get; set; }
		// From : "[-28.65694,-23.293032]"
		protected System.Single[] Parameter4 { get; set; }
		// From : "249.1734"
		protected System.Double Parameter5 { get; set; }
		// From : "true"
		protected System.Boolean Parameter6 { get; set; }
		// From : "1"
		protected System.Int64 Parameter7 { get; set; }
		// From : "Original \"I8DLO4qEXUypJS7XOuc+iQ==\" : Converted : [73,56,68,76,79,52,113,69,88,85,121,112,74,83,55,88,79,117,99,43,105,81,61,61]"
		protected System.Int16[] Parameter8 { get; set; }
		// From : "637186241728067767"
		protected System.Int64 Parameter9 { get; set; }
		// From : "[]"
		protected System.Single[] Parameter14 { get; set; }
		// From : "Original \"itIULCIYn024ip+mX8E24Q==\" : Converted : [105,116,73,85,76,67,73,89,110,48,50,52,105,112,43,109,88,56,69,50,52,81,61,61]"
		protected System.Int16[] Parameter15 { get; set; }
		// From : "Original \"7PkyNy1gf0u3ENYaFtO04A==\" : Converted : [55,80,107,121,78,121,49,103,102,48,117,51,69,78,89,97,70,116,79,48,52,65,61,61]"
		protected System.Int16[] Parameter16 { get; set; }
		// From : "0"
		protected System.Int64 Parameter18 { get; set; }
		// From : "3"
		protected System.Int64 Parameter19 { get; set; }
		// From : "87"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewLoot(Dictionary<byte, object> data) : base(EventCodes.NewLoot, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToFloatArray();
			Parameter5 = SafeExtract(5).ToDouble();
			Parameter6 = SafeExtract(6).ToBoolean();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToInt16Array();
			Parameter9 = SafeExtract(9).ToInt();
			Parameter14 = SafeExtract(14).ToFloatArray();
			Parameter15 = SafeExtract(15).ToInt16Array();
			Parameter16 = SafeExtract(16).ToInt16Array();
			Parameter18 = SafeExtract(18).ToInt();
			Parameter19 = SafeExtract(19).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 113
	public class GEventNewMob : BaseEvent
	{
		// From : "317"
		protected System.Int64 Parameter0 { get; set; }
		// From : "27"
		protected System.Int64 Parameter1 { get; set; }
		// From : "255"
		protected System.Int64 Parameter2 { get; set; }
		// From : "\"\""
		protected System.String Parameter6 { get; set; }
		// From : "[172.96603,83.766266]"
		protected System.Single[] Parameter7 { get; set; }
		// From : "[173.10606,83.500946]"
		protected System.Single[] Parameter8 { get; set; }
		// From : "57267714"
		protected System.Int64 Parameter9 { get; set; }
		// From : "152.1759"
		protected System.Double Parameter10 { get; set; }
		// From : "3.0"
		protected System.Double Parameter11 { get; set; }
		// From : "20.0"
		protected System.Double Parameter13 { get; set; }
		// From : "20.0"
		protected System.Double Parameter14 { get; set; }
		// From : "57143731"
		protected System.Int64 Parameter16 { get; set; }
		// From : "57143731"
		protected System.Int64 Parameter20 { get; set; }
		// From : "0"
		protected System.Int64 Parameter28 { get; set; }
		// From : "113"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewMob(Dictionary<byte, object> data) : base(EventCodes.NewMob, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter6 = SafeExtract(6).ToString();
			Parameter7 = SafeExtract(7).ToFloatArray();
			Parameter8 = SafeExtract(8).ToFloatArray();
			Parameter9 = SafeExtract(9).ToInt();
			Parameter10 = SafeExtract(10).ToDouble();
			Parameter11 = SafeExtract(11).ToDouble();
			Parameter13 = SafeExtract(13).ToDouble();
			Parameter14 = SafeExtract(14).ToDouble();
			Parameter16 = SafeExtract(16).ToInt();
			Parameter20 = SafeExtract(20).ToInt();
			Parameter28 = SafeExtract(28).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 98
	public class GEventNewMonolithObject : BaseEvent
	{
		// From : "19"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[10.0,16.0]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "Original \"7LNG7VHmoUalgOuiEV1nuw==\" : Converted : [55,76,78,71,55,86,72,109,111,85,97,108,103,79,117,105,69,86,49,110,117,119,61,61]"
		protected System.Int16[] Parameter3 { get; set; }
		// From : "\"June\""
		protected System.String Parameter4 { get; set; }
		// From : "Original \"mDhrtRso+U2QNtYy5Z4tww==\" : Converted : [109,68,104,114,116,82,115,111,43,85,50,81,78,116,89,121,53,90,52,116,119,119,61,61]"
		protected System.Int16[] Parameter6 { get; set; }
		// From : "0"
		protected System.Int64 Parameter7 { get; set; }
		// From : "80080000"
		protected System.Int64 Parameter9 { get; set; }
		// From : "0"
		protected System.Int64 Parameter11 { get; set; }
		// From : "6"
		protected System.Int64 Parameter13 { get; set; }
		// From : "83"
		protected System.Int64 Parameter14 { get; set; }
		// From : "8"
		protected System.Int64 Parameter15 { get; set; }
		// From : "2"
		protected System.Int64 Parameter16 { get; set; }
		// From : "16777215"
		protected System.Int64 Parameter17 { get; set; }
		// From : "1.5"
		protected System.Double Parameter18 { get; set; }
		// From : "-0.2"
		protected System.Double Parameter19 { get; set; }
		// From : "0"
		protected System.Int64 Parameter20 { get; set; }
		// From : "98"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewMonolithObject(Dictionary<byte, object> data) : base(EventCodes.NewMonolithObject, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter3 = SafeExtract(3).ToInt16Array();
			Parameter4 = SafeExtract(4).ToString();
			Parameter6 = SafeExtract(6).ToInt16Array();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter9 = SafeExtract(9).ToInt();
			Parameter11 = SafeExtract(11).ToInt();
			Parameter13 = SafeExtract(13).ToInt();
			Parameter14 = SafeExtract(14).ToInt();
			Parameter15 = SafeExtract(15).ToInt();
			Parameter16 = SafeExtract(16).ToInt();
			Parameter17 = SafeExtract(17).ToInt();
			Parameter18 = SafeExtract(18).ToDouble();
			Parameter19 = SafeExtract(19).ToDouble();
			Parameter20 = SafeExtract(20).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 28
	public class GEventNewMountObject : BaseEvent
	{
		// From : "339"
		protected System.Int64 Parameter0 { get; set; }
		// From : "1895"
		protected System.Int64 Parameter1 { get; set; }
		// From : "-1"
		protected System.Int64 Parameter2 { get; set; }
		// From : "[197.7389,51.834328]"
		protected System.Single[] Parameter3 { get; set; }
		// From : "104.69832"
		protected System.Double Parameter4 { get; set; }
		// From : "Original \"CcxeEDirHk+YRdRgo8iwPg==\" : Converted : [67,99,120,101,69,68,105,114,72,107,43,89,82,100,82,103,111,56,105,119,80,103,61,61]"
		protected System.Int16[] Parameter5 { get; set; }
		// From : "0"
		protected System.Int64 Parameter6 { get; set; }
		// From : "3"
		protected System.Int64 Parameter7 { get; set; }
		// From : "Original \"n/hYzrG/QEKxooIWbbEiBA==\" : Converted : [110,47,104,89,122,114,71,47,81,69,75,120,111,111,73,87,98,98,69,105,66,65,61,61]"
		protected System.Int16[] Parameter9 { get; set; }
		// From : "Original \"bMcwYf4DDUil7cjRviSGJg==\" : Converted : [98,77,99,119,89,102,52,68,68,85,105,108,55,99,106,82,118,105,83,71,74,103,61,61]"
		protected System.Int16[] Parameter10 { get; set; }
		// From : "6"
		protected System.Int64 Parameter11 { get; set; }
		// From : "11"
		protected System.Int64 Parameter12 { get; set; }
		// From : "15"
		protected System.Int64 Parameter13 { get; set; }
		// From : "31"
		protected System.Int64 Parameter14 { get; set; }
		// From : "16777215"
		protected System.Int64 Parameter15 { get; set; }
		// From : "0.55"
		protected System.Double Parameter17 { get; set; }
		// From : "284"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewMountObject(Dictionary<byte, object> data) : base(EventCodes.NewMountObject, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToFloatArray();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToInt16Array();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter9 = SafeExtract(9).ToInt16Array();
			Parameter10 = SafeExtract(10).ToInt16Array();
			Parameter11 = SafeExtract(11).ToInt();
			Parameter12 = SafeExtract(12).ToInt();
			Parameter13 = SafeExtract(13).ToInt();
			Parameter14 = SafeExtract(14).ToInt();
			Parameter15 = SafeExtract(15).ToInt();
			Parameter17 = SafeExtract(17).ToDouble();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 100
	public class GEventNewOrbObject : BaseEvent
	{
		// From : "13"
		protected System.Int64 Parameter0 { get; set; }
		// From : "\"Orb\""
		protected System.String Parameter1 { get; set; }
		// From : "\"ORB_GVG\""
		protected System.String Parameter2 { get; set; }
		// From : "[-150.0,495.0]"
		protected System.Single[] Parameter3 { get; set; }
		// From : "Original \"7LNG7VHmoUalgOuiEV1nuw==\" : Converted : [55,76,78,71,55,86,72,109,111,85,97,108,103,79,117,105,69,86,49,110,117,119,61,61]"
		protected System.Int16[] Parameter4 { get; set; }
		// From : "100"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewOrbObject(Dictionary<byte, object> data) : base(EventCodes.NewOrbObject, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToFloatArray();
			Parameter4 = SafeExtract(4).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 74
	public class GEventNewOutpostObject : BaseEvent
	{
		// From : "6500"
		protected System.Int64 Parameter0 { get; set; }
		// From : "\"FOREST_RED_FACTION_WARFARE_OUTPOST_TOWER\""
		protected System.String Parameter1 { get; set; }
		// From : "\"OUTPOST_FOREST\""
		protected System.String Parameter2 { get; set; }
		// From : "[84.0,246.0]"
		protected System.Single[] Parameter3 { get; set; }
		// From : "2"
		protected System.Int64 Parameter4 { get; set; }
		// From : "2"
		protected System.Int64 Parameter5 { get; set; }
		// From : "9223372036854775807"
		protected System.Int64 Parameter6 { get; set; }
		// From : "330"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewOutpostObject(Dictionary<byte, object> data) : base(EventCodes.NewOutpostObject, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToFloatArray();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 39
	public class GEventNewPortalEntrance : BaseEvent
	{
		// From : "4"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[-26.0,26.0]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "Original \"3804ziJEG0CcXcl7nIXqAg==\" : Converted : [51,56,48,52,122,105,74,69,71,48,67,99,88,99,108,55,110,73,88,113,65,103,61,61]"
		protected System.Int16[] Parameter2 { get; set; }
		// From : "295"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewPortalEntrance(Dictionary<byte, object> data) : base(EventCodes.NewPortalEntrance, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 40
	public class GEventNewPortalExit : BaseEvent
	{
		// From : "3277"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[-240.0,-240.0]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "Original \"WN3lTpT15ESdNif5rW01sQ==\" : Converted : [87,78,51,108,84,112,84,49,53,69,83,100,78,105,102,53,114,87,48,49,115,81,61,61]"
		protected System.Int16[] Parameter2 { get; set; }
		// From : "296"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewPortalExit(Dictionary<byte, object> data) : base(EventCodes.NewPortalExit, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 41
	public class GEventNewRandomDungeonExit : BaseEvent
	{
		// From : "937169"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[304.0,22.0]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "\"SHARED_RANDOM_EXIT_10x10_PORTAL_SOLO\""
		protected System.String Parameter3 { get; set; }
		// From : "true"
		protected System.Boolean Parameter4 { get; set; }
		// From : "true"
		protected System.Boolean Parameter6 { get; set; }
		// From : "297"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewRandomDungeonExit(Dictionary<byte, object> data) : base(EventCodes.NewRandomDungeonExit, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter3 = SafeExtract(3).ToString();
			Parameter4 = SafeExtract(4).ToBoolean();
			Parameter6 = SafeExtract(6).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 174
	public class GEventNewRealEstate : BaseEvent
	{
		// From : "1353"
		protected System.Int64 Parameter0 { get; set; }
		// From : "Original \"rDG7IWh+5E6Jt2SKfBAhMw==\" : Converted : [114,68,71,55,73,87,104,43,53,69,54,74,116,50,83,75,102,66,65,104,77,119,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "[-65.0,-45.0]"
		protected System.Single[] Parameter2 { get; set; }
		// From : "[20.0,20.0]"
		protected System.Single[] Parameter3 { get; set; }
		// From : "Original \"E9M88knFzEi/WV1/RACrGw==\" : Converted : [69,57,77,56,56,107,110,70,122,69,105,47,87,86,49,47,82,65,67,114,71,119,61,61]"
		protected System.Int16[] Parameter4 { get; set; }
		// From : "\"Zzy\""
		protected System.String Parameter5 { get; set; }
		// From : "0"
		protected System.Int64 Parameter7 { get; set; }
		// From : "\"June\""
		protected System.String Parameter9 { get; set; }
		// From : "-10000"
		protected System.Int64 Parameter10 { get; set; }
		// From : "571521400000"
		protected System.Int64 Parameter11 { get; set; }
		// From : "636359039297583732"
		protected System.Int64 Parameter12 { get; set; }
		// From : "174"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewRealEstate(Dictionary<byte, object> data) : base(EventCodes.NewRealEstate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter3 = SafeExtract(3).ToFloatArray();
			Parameter4 = SafeExtract(4).ToInt16Array();
			Parameter5 = SafeExtract(5).ToString();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter9 = SafeExtract(9).ToString();
			Parameter10 = SafeExtract(10).ToInt();
			Parameter11 = SafeExtract(11).ToInt();
			Parameter12 = SafeExtract(12).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 33
	public class GEventNewSilverObject : BaseEvent
	{
		// From : "954796"
		protected System.Int64 Parameter0 { get; set; }
		// From : "947896"
		protected System.Int64 Parameter1 { get; set; }
		// From : "true"
		protected System.Boolean Parameter2 { get; set; }
		// From : "637186255879744281"
		protected System.Int64 Parameter3 { get; set; }
		// From : "Original \"SBmRJzE1yk6QDRV0KxR9Hw==\" : Converted : [83,66,109,82,74,122,69,49,121,107,54,81,68,82,86,48,75,120,82,57,72,119,61,61]"
		protected System.Int16[] Parameter4 { get; set; }
		// From : "[319.0733,224.86153]"
		protected System.Single[] Parameter5 { get; set; }
		// From : "30"
		protected System.Int64 Parameter6 { get; set; }
		// From : "true"
		protected System.Boolean Parameter7 { get; set; }
		// From : "0"
		protected System.Int64 Parameter12 { get; set; }
		// From : "0"
		protected System.Int64 Parameter13 { get; set; }
		// From : "33"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewSilverObject(Dictionary<byte, object> data) : base(EventCodes.NewSilverObject, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToBoolean();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt16Array();
			Parameter5 = SafeExtract(5).ToFloatArray();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToBoolean();
			Parameter12 = SafeExtract(12).ToInt();
			Parameter13 = SafeExtract(13).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 31
	public class GEventNewSimpleHarvestableObjectList : BaseEvent
	{
		// From : "Original \"Fys=\" : Converted : \"RnlzPQ==\""
		protected System.Int32[] Parameter0 { get; set; }
		protected System.Int16[] Parameter01 { get; set; }
		// From : "Original \"AAA=\" : Converted : \"QUFBPQ==\""
		protected System.Byte[] Parameter1 { get; set; }
		// From : "Original \"AQE=\" : Converted : \"QVFFPQ==\""
		protected System.Byte[] Parameter2 { get; set; }
		// From : "[237.5,85.5,235.5,84.5]"
		protected System.Single[] Parameter3 { get; set; }
		// From : "Original \"AgI=\" : Converted : \"QWdJPQ==\""
		protected System.Byte[] Parameter4 { get; set; }
		// From : "31"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewSimpleHarvestableObjectList(Dictionary<byte, object>               data) : base(EventCodes.NewSimpleHarvestableObjectList, data)
		{
			try
			{
				List<int> a0 = new List<int>();
				if (data[0].GetType() == typeof(System.Byte[]))
				{
					System.Byte[] typeListByte = (System.Byte[])data[0]; //list of types
					foreach (System.Byte b in typeListByte)
					{
						a0.Add(b);
					}
				}
				else if (data[0].GetType() == typeof(System.Int16[]))
				{
					System.Int16[] typeListByte = (System.Int16[])data[0]; //list of types
					foreach (System.Int16 b in typeListByte)
					{
						a0.Add(b);
					}
				}
				else
				{
				}
				Parameter0 = a0.ToArray();

			}
			catch { Parameter0 = null; }
			Parameter1 = SafeExtract(1).ToByteArray();
			Parameter2 = SafeExtract(2).ToByteArray();
			Parameter3 = SafeExtract(3).ToFloatArray();
			if (SafeExtract(4) == null) { Parameter4 = null; } else{Parameter4 = SafeExtract(4).ToByteArray(); }
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 26
	public class GEventNewSimpleItem : BaseEvent
	{
		// From : "342"
		protected System.Int64 Parameter0 { get; set; }
		// From : "115"
		protected System.Int64 Parameter1 { get; set; }
		// From : "1"
		protected System.Int64 Parameter2 { get; set; }
		// From : "26"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewSimpleItem(Dictionary<byte, object> data) : base(EventCodes.NewSimpleItem, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 102
	public class GEventNewSpellEffectArea : BaseEvent
	{
		// From : "9413"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[387.0,0.0]"
		protected System.Single[] Parameter2 { get; set; }
		// From : "717"
		protected System.Int64 Parameter4 { get; set; }
		// From : "1"
		protected System.Int64 Parameter5 { get; set; }
		// From : "179895"
		protected System.Int64 Parameter6 { get; set; }
		// From : "179895"
		protected System.Int64 Parameter7 { get; set; }
		// From : "0"
		protected System.Int64 Parameter8 { get; set; }
		// From : "102"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewSpellEffectArea(Dictionary<byte, object> data) : base(EventCodes.NewSpellEffectArea, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 195
	public class GEventNewTravelpoint : BaseEvent
	{
		// From : "259"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[177.0,53.0]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "Original \"17TK5uyQuUuuaWU/HQG8KA==\" : Converted : [49,55,84,75,53,117,121,81,117,85,117,117,97,87,85,47,72,81,71,56,75,65,61,61]"
		protected System.Int16[] Parameter2 { get; set; }
		// From : "195"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewTravelpoint(Dictionary<byte, object> data) : base(EventCodes.NewTravelpoint, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 105
	public class GEventNewTreasureChest : BaseEvent
	{
		// From : "6163"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[-225.0,-235.0]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "\"9b28a3b5-64ef-4cf3-81dd-46ff902947dd@1211\""
		protected System.String Parameter2 { get; set; }
		// From : "0"
		protected System.Int64 Parameter3 { get; set; }
		// From : "105"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewTreasureChest(Dictionary<byte, object> data) : base(EventCodes.NewTreasureChest, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 184
	public class GEventNewUnreadMails : BaseEvent
	{
		// From : "true"
		protected System.Boolean Parameter0 { get; set; }
		// From : "184"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventNewUnreadMails(Dictionary<byte, object> data) : base(EventCodes.NewUnreadMails, data)
		{
			Parameter0 = SafeExtract(0).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 252
	public class GEventOtherGrabbedLoot : BaseEvent
	{
		// From : "953689"
		protected System.Int64 Parameter0 { get; set; }
		// From : "\"JokerCRI\""
		protected System.String Parameter2 { get; set; }
		// From : "true"
		protected System.Boolean Parameter3 { get; set; }
		// From : "598500"
		protected System.Int64 Parameter5 { get; set; }
		// From : "252"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventOtherGrabbedLoot(Dictionary<byte, object> data) : base(EventCodes.OtherGrabbedLoot, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToBoolean();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 75
	public class GEventOutpostUpdate : BaseEvent
	{
		// From : "6500"
		protected System.Int64 Parameter0 { get; set; }
		// From : "2"
		protected System.Int64 Parameter1 { get; set; }
		// From : "2"
		protected System.Int64 Parameter2 { get; set; }
		// From : "2"
		protected System.Int64 Parameter3 { get; set; }
		// From : "9223372036854775807"
		protected System.Int64 Parameter4 { get; set; }
		// From : "2"
		protected System.Int64 Parameter5 { get; set; }
		// From : "10000000"
		protected System.Int64 Parameter6 { get; set; }
		// From : "10000000"
		protected System.Int64 Parameter7 { get; set; }
		// From : "637185687280520558"
		protected System.Int64 Parameter8 { get; set; }
		// From : "331"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventOutpostUpdate(Dictionary<byte, object> data) : base(EventCodes.OutpostUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 78
	public class GEventOverChargeEnd : BaseEvent
	{
		// From : "2510825"
		protected System.Int64 Parameter0 { get; set; }
		// From : "1"
		protected System.Int64 Parameter1 { get; set; }
		// From : "2"
		protected System.Int64 Parameter2 { get; set; }
		// From : "334"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventOverChargeEnd(Dictionary<byte, object> data) : base(EventCodes.OverChargeEnd, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 79
	public class GEventOverChargeStatus : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[-9223372036854775808,-9223372036854775808,-9223372036854775808,-9223372036854775808,-9223372036854775808]"
		protected System.Int64[] Parameter2 { get; set; }
		// From : "335"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventOverChargeStatus(Dictionary<byte, object> data) : base(EventCodes.OverChargeStatus, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter2 = SafeExtract(2).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 212
	public class GEventPartyDisbanded : BaseEvent
	{
		// From : "212"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPartyDisbanded(Dictionary<byte, object> data) : base(EventCodes.PartyDisbanded, data)
		{
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 81
	public class GEventPartyFinderUpdate : BaseEvent
	{
		// From : "3"
		protected System.Int64 Parameter0 { get; set; }
		// From : "1"
		protected System.Int64 Parameter1 { get; set; }
		// From : "Original \"CcxeEDirHk+YRdRgo8iwPg==\" : Converted : [67,99,120,101,69,68,105,114,72,107,43,89,82,100,82,103,111,56,105,119,80,103,61,61]"
		protected System.Int16[] Parameter2 { get; set; }
		// From : "\"Palurdo\""
		protected System.String Parameter4 { get; set; }
		// From : "[\"Palurdo\"]"
		protected System.String[] Parameter5 { get; set; }
		// From : "Original \"HA==\" : Converted : [72,65,61,61]"
		protected System.Int16[] Parameter6 { get; set; }
		// From : "Original \"GQ==\" : Converted : [71,81,61,61]"
		protected System.Int16[] Parameter7 { get; set; }
		// From : "Original \"AA==\" : Converted : [65,65,61,61]"
		protected System.Int16[] Parameter8 { get; set; }
		// From : "[0.0]"
		protected System.Single[] Parameter9 { get; set; }
		// From : "[[1,0,0]]"
		protected System.Int64[][] Parameter10 { get; set; }
		// From : "0"
		protected System.Int64 Parameter11 { get; set; }
		// From : "20"
		protected System.Int64 Parameter13 { get; set; }
		// From : "\"\""
		protected System.String Parameter15 { get; set; }
		// From : "\"\""
		protected System.String Parameter16 { get; set; }
		// From : "0"
		protected System.Int64 Parameter17 { get; set; }
		// From : "337"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPartyFinderUpdate(Dictionary<byte, object> data) : base(EventCodes.PartyFinderUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt16Array();
			Parameter4 = SafeExtract(4).ToString();
			Parameter5 = SafeExtract(5).ToStringArray();
			Parameter6 = SafeExtract(6).ToInt16Array();
			Parameter7 = SafeExtract(7).ToInt16Array();
			Parameter8 = SafeExtract(8).ToInt16Array();
			Parameter9 = SafeExtract(9).ToFloatArray();
			Parameter10 = SafeExtract(10).ToInt2DArray();
			Parameter11 = SafeExtract(11).ToInt();
			Parameter13 = SafeExtract(13).ToInt();
			Parameter15 = SafeExtract(15).ToString();
			Parameter16 = SafeExtract(16).ToString();
			Parameter17 = SafeExtract(17).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 210
	public class GEventPartyInvitation : BaseEvent
	{
		// From : "\"Krooocker\""
		protected System.String Parameter0 { get; set; }
		// From : "29973"
		protected System.Int64 Parameter1 { get; set; }
		// From : "210"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPartyInvitation(Dictionary<byte, object> data) : base(EventCodes.PartyInvitation, data)
		{
			Parameter0 = SafeExtract(0).ToString();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 211
	public class GEventPartyJoined : BaseEvent
	{
		// From : "29973"
		protected System.Int64 Parameter0 { get; set; }
		// From : "1"
		protected System.Int64 Parameter2 { get; set; }
		// From : "Original \"vPV/PIyCNk+pXYp3YmpjDw==\" : Converted : [118,80,86,47,80,73,121,67,78,107,43,112,88,89,112,51,89,109,112,106,68,119,61,61]"
		protected System.Int16[] Parameter3 { get; set; }
		// From : "[\"vPV/PIyCNk+pXYp3YmpjDw==\",\"CcxeEDirHk+YRdRgo8iwPg==\"]"
		protected System.String[] Parameter4 { get; set; }
		// From : "[\"Krooocker\",\"Palurdo\"]"
		protected System.String[] Parameter5 { get; set; }
		// From : "Original \"AAA=\" : Converted : \"QUFBPQ==\""
		protected System.Byte[] Parameter6 { get; set; }
		// From : "Original \"Fxw=\" : Converted : \"Rnh3PQ==\""
		protected System.Byte[] Parameter7 { get; set; }
		// From : "Original \"DRk=\" : Converted : \"RFJrPQ==\""
		protected System.Byte[] Parameter8 { get; set; }
		// From : "[-1,-1]"
		protected System.Int64[] Parameter9 { get; set; }
		// From : "[true,true]"
		protected System.Boolean[] Parameter10 { get; set; }
		// From : "211"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPartyJoined(Dictionary<byte, object> data) : base(EventCodes.PartyJoined, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt16Array();
			Parameter4 = SafeExtract(4).ToStringArray();
			Parameter5 = SafeExtract(5).ToStringArray();
			Parameter6 = SafeExtract(6).ToByteArray();
			Parameter7 = SafeExtract(7).ToByteArray();
			Parameter8 = SafeExtract(8).ToByteArray();
			Parameter9 = SafeExtract(9).ToIntArray();
			Parameter10 = SafeExtract(10).ToBooleanArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 216
	public class GEventPartyLeaderChanged : BaseEvent
	{
		// From : "21968"
		protected System.Int64 Parameter0 { get; set; }
		// From : "Original \"58WDEdGvZEiBgKWEfHf31A==\" : Converted : [53,56,87,68,69,100,71,118,90,69,105,66,103,75,87,69,102,72,102,51,49,65,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "216"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPartyLeaderChanged(Dictionary<byte, object> data) : base(EventCodes.PartyLeaderChanged, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 221
	public class GEventPartyMarkedObjectsUpdated : BaseEvent
	{
		// From : "21968"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[0,0,0,0,77362,0]"
		protected System.Int64[] Parameter1 { get; set; }
		// From : "221"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPartyMarkedObjectsUpdated(Dictionary<byte, object> data) : base(EventCodes.PartyMarkedObjectsUpdated, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 222
	public class GEventPartyOnClusterPartyJoined : BaseEvent
	{
		// From : "[\"CcxeEDirHk+YRdRgo8iwPg==\"]"
		protected System.String[] Parameter0 { get; set; }
		// From : "Original \"Bg==\" : Converted : [66,103,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "\"AAAAAAAA\""
		protected System.String Parameter2 { get; set; }
		// From : "222"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPartyOnClusterPartyJoined(Dictionary<byte, object> data) : base(EventCodes.PartyOnClusterPartyJoined, data)
		{
			Parameter0 = SafeExtract(0).ToStringArray();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 213
	public class GEventPartyPlayerJoined : BaseEvent
	{
		// From : "21968"
		protected System.Int64 Parameter0 { get; set; }
		// From : "Original \"SwSakdVlAkW9AFHyI9yAyg==\" : Converted : [83,119,83,97,107,100,86,108,65,107,87,57,65,70,72,121,73,57,121,65,121,103,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "\"MortonBeliever\""
		protected System.String Parameter2 { get; set; }
		// From : "0"
		protected System.Int64 Parameter3 { get; set; }
		// From : "6"
		protected System.Int64 Parameter4 { get; set; }
		// From : "16"
		protected System.Int64 Parameter5 { get; set; }
		// From : "-1"
		protected System.Int64 Parameter6 { get; set; }
		// From : "213"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPartyPlayerJoined(Dictionary<byte, object> data) : base(EventCodes.PartyPlayerJoined, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToString();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 215
	public class GEventPartyPlayerLeft : BaseEvent
	{
		// From : "29973"
		protected System.Int64 Parameter0 { get; set; }
		// From : "Original \"CcxeEDirHk+YRdRgo8iwPg==\" : Converted : [67,99,120,101,69,68,105,114,72,107,43,89,82,100,82,103,111,56,105,119,80,103,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "215"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPartyPlayerLeft(Dictionary<byte, object> data) : base(EventCodes.PartyPlayerLeft, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 223
	public class GEventPartySetRoleFlag : BaseEvent
	{
		// From : "-1"
		protected System.Int64 Parameter0 { get; set; }
		// From : "Original \"atohhloO10SIkaqVDVFfpQ==\" : Converted : [97,116,111,104,104,108,111,79,49,48,83,73,107,97,113,86,68,86,70,102,112,81,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "223"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPartySetRoleFlag(Dictionary<byte, object> data) : base(EventCodes.PartySetRoleFlag, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 218
	public class GEventPartySilverGained : BaseEvent
	{
		// From : "63701"
		protected System.Int64 Parameter0 { get; set; }
		// From : "52966259"
		protected System.Int64 Parameter1 { get; set; }
		// From : "65264"
		protected System.Int64 Parameter2 { get; set; }
		// From : "248400"
		protected System.Int64 Parameter3 { get; set; }
		// From : "270000"
		protected System.Int64 Parameter4 { get; set; }
		// From : "218"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPartySilverGained(Dictionary<byte, object> data) : base(EventCodes.PartySilverGained, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 46
	public class GEventPaymentTransactions : BaseEvent
	{
		// From : "[\"dPJmLHnrsUut7rlcQwO5wA==\",\"NuH3oLuUEkS5nIUFxTpvkg==\",\"DQeAgb14oEabtvZO9cZoJw==\"]"
		protected System.String[] Parameter0 { get; set; }
		// From : "[\"Palurdo\",\"Palurdo\",\"Palurdo\"]"
		protected System.String[] Parameter1 { get; set; }
		// From : "[\"generic-shop-transaction\",\"referral-season-reward-game-transaction\",\"anniversary-2\"]"
		protected System.String[] Parameter2 { get; set; }
		// From : "[false,false,false]"
		protected System.Boolean[] Parameter3 { get; set; }
		// From : "[\"\",\"\",\"\"]"
		protected System.String[] Parameter4 { get; set; }
		// From : "302"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPaymentTransactions(Dictionary<byte, object> data) : base(EventCodes.PaymentTransactions, data)
		{
			Parameter0 = SafeExtract(0).ToStringArray();
			Parameter1 = SafeExtract(1).ToStringArray();
			Parameter2 = SafeExtract(2).ToStringArray();
			Parameter3 = SafeExtract(3).ToBooleanArray();
			Parameter4 = SafeExtract(4).ToStringArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 86
	public class GEventPersonalSeasonPointsGained : BaseEvent
	{
		// From : "94868"
		protected System.Int64 Parameter0 { get; set; }
		// From : "342"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPersonalSeasonPointsGained(Dictionary<byte, object> data) : base(EventCodes.PersonalSeasonPointsGained, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 67
	public class GEventPlayEmote : BaseEvent
	{
		// From : "954992"
		protected System.Int64 Parameter0 { get; set; }
		// From : "12"
		protected System.Int64 Parameter1 { get; set; }
		// From : "57108504"
		protected System.Int64 Parameter3 { get; set; }
		// From : "67"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPlayEmote(Dictionary<byte, object> data) : base(EventCodes.PlayEmote, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 250
	public class GEventPlayerCounts : BaseEvent
	{
		// From : "1"
		protected System.Int64 Parameter0 { get; set; }
		// From : "-1"
		protected System.Int64 Parameter1 { get; set; }
		// From : "250"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPlayerCounts(Dictionary<byte, object> data) : base(EventCodes.PlayerCounts, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 43
	public class GEventPlayerMovementRateUpdate : BaseEvent
	{
		// From : "100"
		protected System.Int64 Parameter0 { get; set; }
		// From : "299"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPlayerMovementRateUpdate(Dictionary<byte, object> data) : base(EventCodes.PlayerMovementRateUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 164
	public class GEventPlayerTradeAcceptChange : BaseEvent
	{
		// From : "4005"
		protected System.Int64 Parameter0 { get; set; }
		// From : "164"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPlayerTradeAcceptChange(Dictionary<byte, object> data) : base(EventCodes.PlayerTradeAcceptChange, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 161
	public class GEventPlayerTradeCancel : BaseEvent
	{
		// From : "4005"
		protected System.Int64 Parameter0 { get; set; }
		// From : "161"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPlayerTradeCancel(Dictionary<byte, object> data) : base(EventCodes.PlayerTradeCancel, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 163
	public class GEventPlayerTradeFinished : BaseEvent
	{
		// From : "3629"
		protected System.Int64 Parameter0 { get; set; }
		// From : "163"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPlayerTradeFinished(Dictionary<byte, object> data) : base(EventCodes.PlayerTradeFinished, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 162
	public class GEventPlayerTradeUpdate : BaseEvent
	{
		// From : "4005"
		protected System.Int64 Parameter0 { get; set; }
		// From : "9"
		protected System.Int64 Parameter1 { get; set; }
		// From : "10000000000"
		protected System.Int64 Parameter2 { get; set; }
		// From : "\"\""
		protected System.String Parameter6 { get; set; }
		// From : "\"\""
		protected System.String Parameter7 { get; set; }
		// From : "\"\""
		protected System.String Parameter8 { get; set; }
		// From : "[]"
		protected System.Single[] Parameter9 { get; set; }
		// From : "\"\""
		protected System.String Parameter10 { get; set; }
		// From : "\"\""
		protected System.String Parameter11 { get; set; }
		// From : "\"\""
		protected System.String Parameter12 { get; set; }
		// From : "[[-1]]"
		protected System.Int64[][] Parameter13 { get; set; }
		// From : "[[-1]]"
		protected System.Int64[][] Parameter14 { get; set; }
		// From : "\"\""
		protected System.String Parameter15 { get; set; }
		// From : "\"\""
		protected System.String Parameter16 { get; set; }
		// From : "\"\""
		protected System.String Parameter17 { get; set; }
		// From : "[]"
		protected System.Single[] Parameter18 { get; set; }
		// From : "\"\""
		protected System.String Parameter19 { get; set; }
		// From : "\"\""
		protected System.String Parameter20 { get; set; }
		// From : "\"\""
		protected System.String Parameter21 { get; set; }
		// From : "[[-1]]"
		protected System.Int64[][] Parameter22 { get; set; }
		// From : "[[-1]]"
		protected System.Int64[][] Parameter23 { get; set; }
		// From : "\"\""
		protected System.String Parameter24 { get; set; }
		// From : "162"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventPlayerTradeUpdate(Dictionary<byte, object> data) : base(EventCodes.PlayerTradeUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter6 = SafeExtract(6).ToString();
			Parameter7 = SafeExtract(7).ToString();
			Parameter8 = SafeExtract(8).ToString();
			Parameter9 = SafeExtract(9).ToFloatArray();
			Parameter10 = SafeExtract(10).ToString();
			Parameter11 = SafeExtract(11).ToString();
			Parameter12 = SafeExtract(12).ToString();
			Parameter13 = SafeExtract(13).ToInt2DArray();
			Parameter14 = SafeExtract(14).ToInt2DArray();
			Parameter15 = SafeExtract(15).ToString();
			Parameter16 = SafeExtract(16).ToString();
			Parameter17 = SafeExtract(17).ToString();
			Parameter18 = SafeExtract(18).ToFloatArray();
			Parameter19 = SafeExtract(19).ToString();
			Parameter20 = SafeExtract(20).ToString();
			Parameter21 = SafeExtract(21).ToString();
			Parameter22 = SafeExtract(22).ToInt2DArray();
			Parameter23 = SafeExtract(23).ToInt2DArray();
			Parameter24 = SafeExtract(24).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 236
	public class GEventQuestGiverInfoForPlayer : BaseEvent
	{
		// From : "1752"
		protected System.Int64 Parameter0 { get; set; }
		// From : "Original \"FAcIM+NJB0aFo7jx+ARp3w==\" : Converted : [70,65,99,73,77,43,78,74,66,48,97,70,111,55,106,120,43,65,82,112,51,119,61,61]"
		protected System.Int16[] Parameter1 { get; set; }
		// From : "20"
		protected System.Int64 Parameter2 { get; set; }
		// From : "0"
		protected System.Int64 Parameter3 { get; set; }
		// From : "637185687285671942"
		protected System.Int64 Parameter4 { get; set; }
		// From : "236"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventQuestGiverInfoForPlayer(Dictionary<byte, object> data) : base(EventCodes.QuestGiverInfoForPlayer, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt16Array();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 143
	public class GEventQuestGiverQuestOffered : BaseEvent
	{
		// From : "1752"
		protected System.Int64 Parameter0 { get; set; }
		// From : "-1"
		protected System.Int64 Parameter2 { get; set; }
		// From : "11"
		protected System.Int64 Parameter3 { get; set; }
		// From : "\"\""
		protected System.String Parameter4 { get; set; }
		// From : "-9223372036854775808"
		protected System.Int64 Parameter6 { get; set; }
		// From : "-9223372036854775808"
		protected System.Int64 Parameter7 { get; set; }
		// From : "143"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventQuestGiverQuestOffered(Dictionary<byte, object> data) : base(EventCodes.QuestGiverQuestOffered, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToString();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 115
	public class GEventRecoveryVaultPlayerInfo : BaseEvent
	{
		// From : "5"
		protected System.Int64 Parameter0 { get; set; }
		// From : "\"0e860295-bb5c-4be1-89d0-9b1bbcba0b99@1001\""
		protected System.String Parameter1 { get; set; }
		// From : "[]"
		protected System.Single[] Parameter2 { get; set; }
		// From : "[]"
		protected System.Single[] Parameter3 { get; set; }
		// From : "[]"
		protected System.Single[] Parameter4 { get; set; }
		// From : "\"\""
		protected System.String Parameter5 { get; set; }
		// From : "[]"
		protected System.Single[] Parameter6 { get; set; }
		// From : "\"\""
		protected System.String Parameter7 { get; set; }
		// From : "\"\""
		protected System.String Parameter8 { get; set; }
		// From : "371"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventRecoveryVaultPlayerInfo(Dictionary<byte, object> data) : base(EventCodes.RecoveryVaultPlayerInfo, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToString();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter3 = SafeExtract(3).ToFloatArray();
			Parameter4 = SafeExtract(4).ToFloatArray();
			Parameter5 = SafeExtract(5).ToString();
			Parameter6 = SafeExtract(6).ToFloatArray();
			Parameter7 = SafeExtract(7).ToString();
			Parameter8 = SafeExtract(8).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 81
	public class GEventRegenerationEnergyChanged : BaseEvent
	{
		// From : "1162981"
		protected System.Int64 Parameter0 { get; set; }
		// From : "56888656"
		protected System.Int64 Parameter1 { get; set; }
		// From : "252.0"
		protected System.Double Parameter2 { get; set; }
		// From : "252.0"
		protected System.Double Parameter3 { get; set; }
		// From : "3.659622"
		protected System.Double Parameter4 { get; set; }
		// From : "637186254375976371"
		protected System.Int64 Parameter5 { get; set; }
		// From : "81"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventRegenerationEnergyChanged(Dictionary<byte, object> data) : base(EventCodes.RegenerationEnergyChanged, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 80
	public class GEventRegenerationHealthChanged : BaseEvent
	{
		// From : "954799"
		protected System.Int64 Parameter0 { get; set; }
		// From : "57095930"
		protected System.Int64 Parameter1 { get; set; }
		// From : "2775.0"
		protected System.Double Parameter2 { get; set; }
		// From : "2775.0"
		protected System.Double Parameter3 { get; set; }
		// From : "70.249344"
		protected System.Double Parameter4 { get; set; }
		// From : "637186256448717379"
		protected System.Int64 Parameter5 { get; set; }
		// From : "80"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventRegenerationHealthChanged(Dictionary<byte, object> data) : base(EventCodes.RegenerationHealthChanged, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 84
	public class GEventRegenerationHealthEnergyComboChanged : BaseEvent
	{
		// From : "949133"
		protected System.Int64 Parameter0 { get; set; }
		// From : "57113188"
		protected System.Int64 Parameter1 { get; set; }
		// From : "1355.0"
		protected System.Double Parameter2 { get; set; }
		// From : "1355.0"
		protected System.Double Parameter3 { get; set; }
		// From : "637186256621301960"
		protected System.Int64 Parameter5 { get; set; }
		// From : "267.0"
		protected System.Double Parameter6 { get; set; }
		// From : "267.0"
		protected System.Double Parameter7 { get; set; }
		// From : "8.0"
		protected System.Double Parameter8 { get; set; }
		// From : "637186256621301960"
		protected System.Int64 Parameter9 { get; set; }
		// From : "84"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventRegenerationHealthEnergyComboChanged(Dictionary<byte, object> data) : base(EventCodes.RegenerationHealthEnergyComboChanged, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToDouble();
			Parameter7 = SafeExtract(7).ToDouble();
			Parameter8 = SafeExtract(8).ToDouble();
			Parameter9 = SafeExtract(9).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 82
	public class GEventRegenerationMountHealthChanged : BaseEvent
	{
		// From : "2510937"
		protected System.Int64 Parameter0 { get; set; }
		// From : "56935161"
		protected System.Int64 Parameter1 { get; set; }
		// From : "797.0"
		protected System.Double Parameter2 { get; set; }
		// From : "797.0"
		protected System.Double Parameter3 { get; set; }
		// From : "7.97"
		protected System.Double Parameter4 { get; set; }
		// From : "637186254841031577"
		protected System.Int64 Parameter5 { get; set; }
		// From : "82"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventRegenerationMountHealthChanged(Dictionary<byte, object> data) : base(EventCodes.RegenerationMountHealthChanged, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 85
	public class GEventRegenerationPlayerComboChanged : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "57148319"
		protected System.Int64 Parameter1 { get; set; }
		// From : "2122.0"
		protected System.Double Parameter2 { get; set; }
		// From : "2122.0"
		protected System.Double Parameter3 { get; set; }
		// From : "48.80414"
		protected System.Double Parameter4 { get; set; }
		// From : "637186256972606960"
		protected System.Int64 Parameter5 { get; set; }
		// From : "269.0"
		protected System.Double Parameter6 { get; set; }
		// From : "269.0"
		protected System.Double Parameter7 { get; set; }
		// From : "3.6963742"
		protected System.Double Parameter8 { get; set; }
		// From : "637186256972606960"
		protected System.Int64 Parameter9 { get; set; }
		// From : "669.0"
		protected System.Double Parameter11 { get; set; }
		// From : "6.69"
		protected System.Double Parameter12 { get; set; }
		// From : "637186256972606960"
		protected System.Int64 Parameter13 { get; set; }
		// From : "85"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventRegenerationPlayerComboChanged(Dictionary<byte, object> data) : base(EventCodes.RegenerationPlayerComboChanged, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToDouble();
			Parameter7 = SafeExtract(7).ToDouble();
			Parameter8 = SafeExtract(8).ToDouble();
			Parameter9 = SafeExtract(9).ToInt();
			Parameter11 = SafeExtract(11).ToDouble();
			Parameter12 = SafeExtract(12).ToDouble();
			Parameter13 = SafeExtract(13).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 39
	public class GEventRepairBuildingInfo : BaseEvent
	{
		// From : "1750"
		protected System.Int64 Parameter0 { get; set; }
		// From : "\"\""
		protected System.String Parameter6 { get; set; }
		// From : "\"\""
		protected System.String Parameter7 { get; set; }
		// From : "39"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventRepairBuildingInfo(Dictionary<byte, object> data) : base(EventCodes.RepairBuildingInfo, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter6 = SafeExtract(6).ToString();
			Parameter7 = SafeExtract(7).ToString();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 27
	public class GEventReputationImplicationUpdate : BaseEvent
	{
		// From : "951595"
		protected System.Int64 Parameter0 { get; set; }
		// From : "56991898"
		protected System.Int64 Parameter1 { get; set; }
		// From : "283"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventReputationImplicationUpdate(Dictionary<byte, object> data) : base(EventCodes.ReputationImplicationUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 22
	public class GEventReputationUpdate : BaseEvent
	{
		// From : "953315"
		protected System.Int64 Parameter0 { get; set; }
		// From : "57110355"
		protected System.Int64 Parameter1 { get; set; }
		// From : "0.14999999"
		protected System.Double Parameter2 { get; set; }
		// From : "10558.394"
		protected System.Double Parameter3 { get; set; }
		// From : "0"
		protected System.Int64 Parameter4 { get; set; }
		// From : "0"
		protected System.Int64 Parameter5 { get; set; }
		// From : "2"
		protected System.Int64 Parameter6 { get; set; }
		// From : "644"
		protected System.Int64 Parameter7 { get; set; }
		// From : "278"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventReputationUpdate(Dictionary<byte, object> data) : base(EventCodes.ReputationUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToDouble();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToInt();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 77
	public class GEventRespawn : BaseEvent
	{
		// From : "2470172"
		protected System.Int64 Parameter0 { get; set; }
		// From : "56809066"
		protected System.Int64 Parameter1 { get; set; }
		// From : "[-54.733135,29.342123]"
		protected System.Single[] Parameter2 { get; set; }
		// From : "203.18211"
		protected System.Double Parameter3 { get; set; }
		// From : "2471.0"
		protected System.Double Parameter4 { get; set; }
		// From : "2472.0"
		protected System.Double Parameter5 { get; set; }
		// From : "40.695225"
		protected System.Double Parameter6 { get; set; }
		// From : "56809066"
		protected System.Int64 Parameter7 { get; set; }
		// From : "216.0"
		protected System.Double Parameter8 { get; set; }
		// From : "296.0"
		protected System.Double Parameter9 { get; set; }
		// From : "4.2200937"
		protected System.Double Parameter10 { get; set; }
		// From : "56808897"
		protected System.Int64 Parameter11 { get; set; }
		// From : "true"
		protected System.Boolean Parameter12 { get; set; }
		// From : "true"
		protected System.Boolean Parameter13 { get; set; }
		// From : "77"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventRespawn(Dictionary<byte, object> data) : base(EventCodes.Respawn, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToDouble();
			Parameter6 = SafeExtract(6).ToDouble();
			Parameter7 = SafeExtract(7).ToInt();
			Parameter8 = SafeExtract(8).ToDouble();
			Parameter9 = SafeExtract(9).ToDouble();
			Parameter10 = SafeExtract(10).ToDouble();
			Parameter11 = SafeExtract(11).ToInt();
			Parameter12 = SafeExtract(12).ToBoolean();
			Parameter13 = SafeExtract(13).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 243
	public class GEventRewardGranted : BaseEvent
	{
		// From : "115"
		protected System.Int64 Parameter1 { get; set; }
		// From : "1"
		protected System.Int64 Parameter3 { get; set; }
		// From : "243"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventRewardGranted(Dictionary<byte, object> data) : base(EventCodes.RewardGranted, data)
		{
			Parameter1 = SafeExtract(1).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 224
	public class GEventSpellCooldownUpdate : BaseEvent
	{
		// From : "Original \"AwQFDA0=\" : Converted : \"QXdRRkRBMD0=\""
		protected System.Byte[] Parameter0 { get; set; }
		// From : "[57158319,57158319,57158319,57158319,57158319]"
		protected System.Int64[] Parameter1 { get; set; }
		// From : "[57148319,57148319,57148319,57148319,57148319]"
		protected System.Int64[] Parameter2 { get; set; }
		// From : "224"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventSpellCooldownUpdate(Dictionary<byte, object> data) : base(EventCodes.SpellCooldownUpdate, data)
		{
			Parameter0 = SafeExtract(0).ToByteArray();
			Parameter1 = SafeExtract(1).ToIntArray();
			Parameter2 = SafeExtract(2).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 68
	public class GEventStopEmote : BaseEvent
	{
		// From : "77384"
		protected System.Int64 Parameter0 { get; set; }
		// From : "68"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventStopEmote(Dictionary<byte, object> data) : base(EventCodes.StopEmote, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 51
	public class GEventTakeSilver : BaseEvent
	{
		// From : "953689"
		protected System.Int64 Parameter0 { get; set; }
		// From : "57020062"
		protected System.Int64 Parameter1 { get; set; }
		// From : "954515"
		protected System.Int64 Parameter2 { get; set; }
		// From : "598500"
		protected System.Int64 Parameter3 { get; set; }
		// From : "0"
		protected System.Int64 Parameter4 { get; set; }
		// From : "0"
		protected System.Int64 Parameter5 { get; set; }
		// From : "true"
		protected System.Boolean Parameter6 { get; set; }
		// From : "true"
		protected System.Boolean Parameter7 { get; set; }
		// From : "51"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventTakeSilver(Dictionary<byte, object> data) : base(EventCodes.TakeSilver, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter6 = SafeExtract(6).ToBoolean();
			Parameter7 = SafeExtract(7).ToBoolean();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 4
	public class GEventTeleport : BaseEvent
	{
		// From : "1679858"
		protected System.Int64 Parameter0 { get; set; }
		// From : "637186248650809958"
		protected System.Int64 Parameter1 { get; set; }
		// From : "[7.5607657,-7.993052]"
		protected System.Single[] Parameter2 { get; set; }
		// From : "137.81096"
		protected System.Double Parameter3 { get; set; }
		// From : "5.5"
		protected System.Double Parameter4 { get; set; }
		// From : "[7.5607657,-7.993052]"
		protected System.Single[] Parameter5 { get; set; }
		// From : "4"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventTeleport(Dictionary<byte, object> data) : base(EventCodes.Teleport, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToFloatArray();
			Parameter3 = SafeExtract(3).ToDouble();
			Parameter4 = SafeExtract(4).ToDouble();
			Parameter5 = SafeExtract(5).ToFloatArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 53
	public class GEventTerritoryClaimCancel : BaseEvent
	{
		// From : "63521"
		protected System.Int64 Parameter0 { get; set; }
		// From : "309"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventTerritoryClaimCancel(Dictionary<byte, object> data) : base(EventCodes.TerritoryClaimCancel, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 54
	public class GEventTerritoryClaimFinished : BaseEvent
	{
		// From : "63508"
		protected System.Int64 Parameter0 { get; set; }
		// From : "310"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventTerritoryClaimFinished(Dictionary<byte, object> data) : base(EventCodes.TerritoryClaimFinished, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 52
	public class GEventTerritoryClaimStart : BaseEvent
	{
		// From : "63521"
		protected System.Int64 Parameter0 { get; set; }
		// From : "52939174"
		protected System.Int64 Parameter1 { get; set; }
		// From : "52954174"
		protected System.Int64 Parameter2 { get; set; }
		// From : "4593"
		protected System.Int64 Parameter3 { get; set; }
		// From : "308"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventTerritoryClaimStart(Dictionary<byte, object> data) : base(EventCodes.TerritoryClaimStart, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 146
	public class GEventTimeSync : BaseEvent
	{
		// From : "57328667"
		protected System.Int64 Parameter0 { get; set; }
		// From : "-990677857"
		protected System.Int64 Parameter1 { get; set; }
		// From : "146"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventTimeSync(Dictionary<byte, object> data) : base(EventCodes.TimeSync, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 104
	public class GEventUpdateChainSpell : BaseEvent
	{
		// From : "954800"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[351.40704,120.712555]"
		protected System.Single[] Parameter1 { get; set; }
		// From : "935416"
		protected System.Int64 Parameter2 { get; set; }
		// From : "104"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventUpdateChainSpell(Dictionary<byte, object> data) : base(EventCodes.UpdateChainSpell, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToFloatArray();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 199
	public class GEventUpdateChatSettings : BaseEvent
	{
		// From : "[\"basico\",\"bardo\",\"fama\",\"cura\",\"@CHAT_SETTINGS_TAB_NAME_PARTY\",\"@CHAT_SETTINGS_TAB_NAME_ALLIANCE\",\"loot\",\"ss\",\"@CHAT_SETTINGS_TAB_NAME_GUILD\"]"
		protected System.String[] Parameter0 { get; set; }
		// From : "[[15,16,19,21,17],[14,13,18,21,0],[33],[28],[17,19],[16,19],[30,31],[27,23],[15,19]]"
		protected System.Int64[][] Parameter1 { get; set; }
		// From : "[false,false,false,false,true,true,false,false,true]"
		protected System.Boolean[] Parameter2 { get; set; }
		// From : "[false,false,false,false,false,false,false,false,false]"
		protected System.Boolean[] Parameter3 { get; set; }
		// From : "199"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventUpdateChatSettings(Dictionary<byte, object> data) : base(EventCodes.UpdateChatSettings, data)
		{
			Parameter0 = SafeExtract(0).ToStringArray();
			Parameter1 = SafeExtract(1).ToInt2DArray();
			Parameter2 = SafeExtract(2).ToBooleanArray();
			Parameter3 = SafeExtract(3).ToBooleanArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 75
	public class GEventUpdateCurrency : BaseEvent
	{
		// From : "3"
		protected System.Int64 Parameter0 { get; set; }
		// From : "1"
		protected System.Int64 Parameter1 { get; set; }
		// From : "-30000000"
		protected System.Int64 Parameter2 { get; set; }
		// From : "0"
		protected System.Int64 Parameter3 { get; set; }
		// From : "110160000"
		protected System.Int64 Parameter4 { get; set; }
		// From : "75"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventUpdateCurrency(Dictionary<byte, object> data) : base(EventCodes.UpdateCurrency, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 76
	public class GEventUpdateFactionStanding : BaseEvent
	{
		// From : "2"
		protected System.Int64 Parameter0 { get; set; }
		// From : "54000"
		protected System.Int64 Parameter1 { get; set; }
		// From : "0"
		protected System.Int64 Parameter2 { get; set; }
		// From : "1868593500"
		protected System.Int64 Parameter3 { get; set; }
		// From : "76"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventUpdateFactionStanding(Dictionary<byte, object> data) : base(EventCodes.UpdateFactionStanding, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 72
	public class GEventUpdateFame : BaseEvent
	{
		// From : "325"
		protected System.Int64 Parameter0 { get; set; }
		// From : "713384497329"
		protected System.Int64 Parameter1 { get; set; }
		// From : "100000"
		protected System.Int64 Parameter2 { get; set; }
		// From : "1"
		protected System.Int64 Parameter3 { get; set; }
		// From : "1.0"
		protected System.Double Parameter6 { get; set; }
		// From : "72"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventUpdateFame(Dictionary<byte, object> data) : base(EventCodes.UpdateFame, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter6 = SafeExtract(6).ToDouble();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 73
	public class GEventUpdateLearningPoints : BaseEvent
	{
		// From : "2383826"
		protected System.Int64 Parameter0 { get; set; }
		// From : "5890000"
		protected System.Int64 Parameter1 { get; set; }
		// From : "637092885816470000"
		protected System.Int64 Parameter2 { get; set; }
		// From : "9999990000"
		protected System.Int64 Parameter3 { get; set; }
		// From : "-20000"
		protected System.Int64 Parameter5 { get; set; }
		// From : "73"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventUpdateLearningPoints(Dictionary<byte, object> data) : base(EventCodes.UpdateLearningPoints, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter5 = SafeExtract(5).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 71
	public class GEventUpdateMoney : BaseEvent
	{
		// From : "2518387"
		protected System.Int64 Parameter0 { get; set; }
		// From : "23636273334"
		protected System.Int64 Parameter1 { get; set; }
		// From : "71"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventUpdateMoney(Dictionary<byte, object> data) : base(EventCodes.UpdateMoney, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 74
	public class GEventUpdateReSpecPoints : BaseEvent
	{
		// From : "[0,7637905960,0,0,0]"
		protected System.Int64[] Parameter0 { get; set; }
		// From : "74"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventUpdateReSpecPoints(Dictionary<byte, object> data) : base(EventCodes.UpdateReSpecPoints, data)
		{
			Parameter0 = SafeExtract(0).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 205
	public class GEventUpdateUnlockedAvatarRings : BaseEvent
	{
		// From : "Original \"FRYLDBcYGQo=\" : Converted : \"RlJZTERCY1lHUW89\""
		protected System.Byte[] Parameter0 { get; set; }
		// From : "205"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventUpdateUnlockedAvatarRings(Dictionary<byte, object> data) : base(EventCodes.UpdateUnlockedAvatarRings, data)
		{
			Parameter0 = SafeExtract(0).ToByteArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 204
	public class GEventUpdateUnlockedAvatars : BaseEvent
	{
		// From : "Original \"ERI=\" : Converted : \"RVJJPQ==\""
		protected System.Byte[] Parameter0 { get; set; }
		// From : "204"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventUpdateUnlockedAvatars(Dictionary<byte, object> data) : base(EventCodes.UpdateUnlockedAvatars, data)
		{
			Parameter0 = SafeExtract(0).ToByteArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//Event | BaseCode - 117
	public class GEventUpdateWardrobe : BaseEvent
	{
		// From : "2382440"
		protected System.Int64 Parameter0 { get; set; }
		// From : "[-1,-1,-1,55,-1,-1,-1]"
		protected System.Int64[] Parameter1 { get; set; }
		// From : "373"
		protected System.Int64 Parameter252 { get; set; }
		protected GEventUpdateWardrobe(Dictionary<byte, object> data) : base(EventCodes.UpdateWardrobe, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToIntArray();
			Parameter252 = SafeExtract(252).ToInt();
		}
	}

	//OperationRequest | BaseCode - 1
	public class GOperationRequestPing : BaseOpRequest
	{
		// From : "26"
		protected System.Int64 Parameter0 { get; set; }
		// From : "26"
		protected System.Int64 Parameter1 { get; set; }
		// From : "26"
		protected System.Int64 Parameter2 { get; set; }
		// From : "786432"
		protected System.Int64 Parameter3 { get; set; }
		// From : "\"1024x768\""
		protected System.String Parameter4 { get; set; }
		// From : "296"
		protected System.Int64 Parameter253 { get; set; }
		protected GOperationRequestPing(Dictionary<byte, object> data) : base(OperationCodes.Ping, data)
		{
			Parameter0 = SafeExtract(0).ToInt();
			Parameter1 = SafeExtract(1).ToInt();
			Parameter2 = SafeExtract(2).ToInt();
			Parameter3 = SafeExtract(3).ToInt();
			Parameter4 = SafeExtract(4).ToString();
			Parameter253 = SafeExtract(253).ToInt();
		}
	}

	//OperationResponse | BaseCode - 1
	//public class GOperationResponsePing : BaseOpResponse
	//{
	//	// From : "146"
	//	protected System.Int64 Parameter255 { get; set; }
	//	// From : "316"
	//	protected System.Int64 Parameter253 { get; set; }
	//	protected GOperationResponsePing(Dictionary<byte, object> data) : base(OperationCodes.Ping, data)
	//	{
	//		Parameter255 = SafeExtract(255).ToInt();
	//		Parameter253 = SafeExtract(253).ToInt();
	//	}
	//}
}
