﻿using System;
using System.Collections.Generic;
using System.Text;
using AOC.Network.Packets.Generated;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __HarvestFinished : GEventHarvestFinished
    {
        public __HarvestFinished(Dictionary<byte, object> data) : base(data)
        {
            IdHarvestable = Parameter3;
            Count = Parameter7;

        }

        public long IdHarvestable { get; }
        public long Count { get; }
    }
}
