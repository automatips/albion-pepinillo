﻿using System.Collections.Generic;
using AOC.Network.Data;
using AOC.Network.Packets.Base;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __Join : BaseOpResponse
    {
      

        public __Join(Dictionary<byte, object> data) : base(OperationCodes.Join,data)
        {
            FullParams = data;
            IdMap = data[8].ToString().Trim();
            Jugador = data[2].ToString().Trim().ToLower();
            Loc = (float[])data[9];
        }

        public Dictionary<byte, object> FullParams { get; }
        public string IdMap { get; }
        public string Jugador { get; }
        public float[] Loc { get; }
    }
}