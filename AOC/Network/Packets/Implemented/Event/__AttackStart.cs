﻿using System;
using System.Collections.Generic;
using System.Text;
using AOC.Network.Data;
using AOC.Network.Packets.Base;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __AttackStart : BaseOpRequest
    {
        public __AttackStart(Dictionary<byte, object> data) : base(OperationCodes.Move,data)
        {
            Id = long.Parse(data[0].ToString());
            Loc = (float[])data[1];
            try { Direction = (float)data[2]; } catch { Direction = -1; }
            NewLoc = (float[])data[3];
            Speed = (float)data[4];
        }

        public long Id { get; }
        public float[] Loc { get; }
        public float Direction { get; }
        public float[] NewLoc { get; }
        public float Speed { get; }
    }
}
