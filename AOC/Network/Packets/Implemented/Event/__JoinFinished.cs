﻿using System.Collections.Generic;
using AOC.Network.Packets.Generated;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __JoinFinished : GEventJoinFinished
    {
        public __JoinFinished(Dictionary<byte, object> data) : base(data)
        {
            _ = Parameter252;
        }
    }
}