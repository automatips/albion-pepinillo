﻿using System.Collections.Generic;
using AOC.Network.Packets.Generated;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __HealthUpdate : GEventHealthUpdate
    {
        public __HealthUpdate(Dictionary<byte, object> data) : base(data)
        {
            MobId = Parameter0;
            NewHp = Parameter3;
            Dmg = Parameter2;
        }

        public long MobId { get; }
        public double NewHp { get; set; }
        public double Dmg { get; set; }
    }
}