﻿using System.Collections.Generic;
using AOC.Network.Packets.Generated;

namespace AOC.Network.Packets.Implemented.Event
{
    public class __UpdateFame : GEventUpdateFame
    {
        public __UpdateFame(Dictionary<byte, object> data) : base(data)
        {
            Id = Parameter0;
            FameAmount = Parameter2 / 10000;
        }

        public float Id { get; }
        public long FameAmount { get; }
    }
}