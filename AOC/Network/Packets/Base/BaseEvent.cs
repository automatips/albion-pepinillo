﻿using System.Collections.Generic;
using AOC.Network.Data;
using AOC.Network.Photon.Protocol16.Core;

namespace AOC.Network.Packets.Base
{
    public class BaseEvent : __EventData
    {
        public BaseEvent(EventCodes eventCode, Dictionary<byte, object> data) : base((byte) eventCode, data)
        {
        }

        protected object SafeExtract(byte key)
        {
            return Parameters.ContainsKey(key) ? Parameters[key] : null;
        }
    }
}