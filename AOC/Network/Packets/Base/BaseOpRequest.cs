﻿using System.Collections.Generic;
using AOC.Network.Data;
using AOC.Network.Photon.Protocol16.Core;

namespace AOC.Network.Packets.Base
{
    public class BaseOpRequest : OperationRequest
    {
        public BaseOpRequest(OperationCodes operationCode, Dictionary<byte, object> data) : base((byte) operationCode,
            data)
        {
        }

        protected object SafeExtract(byte key)
        {
            return Parameters.ContainsKey(key) ? Parameters[key] : null;
        }
    }
}