﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOC.Network.Data;
using AOC.Network.Core.PacketHandler;
using AOC.Network.Packets.Base;
using AOC.Network.Photon;
using AOC.Library.PacketHelpers;
using Newtonsoft.Json;
using System.Reflection;
using PacketDotNet.Utils;

namespace AOC.Network.Core
{
    public class AOCPhotonParser : PhotonParser
    {
        private Comunicator com;
        private ICollection<IPacketHandler> packetHandlers { get; }
        private string Arg { set; get; }
        public void arg(string k){ Arg = k; }


        public AOCPhotonParser(string j)
        {
            packetHandlers = new List<IPacketHandler>();
            com = new Comunicator(j);


        }

       

        internal AOCPhotonParser AddHandler(IPacketHandler handler)
        {
            packetHandlers.LastOrDefault()?.SetNext(handler);
            packetHandlers.Add(handler);
            return this;
        }

        protected override void OnRequest(byte operationCode, Dictionary<byte, object> parameters)
        {
           // ToTableStyleLog("OperationRequest", operationCode, Enum.GetName(typeof(OperationCodes), operationCode), parameters);

            if (com.Fais())
            {
                try
                {
                    var requestPacket = new BaseOpRequest(GetOperationCode(parameters), parameters);
                    packetHandlers.FirstOrDefault()?.__Handle(requestPacket);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());

                }
            }
           

        }

        protected override void OnResponse(byte operationCode, short returnCode, string debugMessage,
            Dictionary<byte, object> parameters)
        {

            //ToTableStyleLog("OperationResponse", operationCode, Enum.GetName(typeof(OperationCodes), operationCode), parameters);
            if (parameters.ContainsKey((byte)253))
            {
                string p = parameters[(byte)253].ToString();
                if (p.Equals("2"))
                {
                    com.GetData((string)parameters[2]);
                }
            }
            if (com.Fais()) 
            {
                try
                {
                    var requestPacket = new BaseOpResponse(GetOperationCode(parameters), parameters);
                    packetHandlers.FirstOrDefault()?.__Handle(requestPacket);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());


                }
            }
            

        }

        protected override void OnEvent(byte code, Dictionary<byte, object> parameters)
        {

            //var translated = "";
            //ToTableStyleLog("Event", (byte)code, "", parameters);
            //if (parameters.ContainsKey(252))
            //{
            //    var test = parameters[252].ToString();
            //    translated = Enum.GetName(typeof(EventCodes), parameters[252]);
            //    if (test == "25" )
            //    {
            //        //ToTableStyleLog("Event", (byte)code, translated, parameters);

            //    }
            //}


            object requestPacket2;

            if (com.Fais())
            {
                var t = 0;
                switch (parameters.Count)
                {
                    case 0:
                    case 1:
                    case 2:
                        if (!parameters.ContainsKey(252))
                            //move event
                            parameters.Add(252, (short)EventCodes.Move);
                        break;
                    case 3:
                        t = 1;
                        break;
                    case 4:
                        t = 1;
                        break;
                    case 5:
                        t = 1;
                        break;
                    case 6:
                        t = 1;
                        break;
                    case 7:
                    case 8:
                        break;

                }
           


                try
                {
                    var requestPacket = new BaseEvent(GetEventCode(parameters), parameters);
                    packetHandlers.FirstOrDefault()?.__Handle(requestPacket);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());

                }
            }

            





        }

        private OperationCodes GetOperationCode(IDictionary<byte, object> data)
        {
            if (!data.TryGetValue(253, out var value)) throw new Exception("Unable to parse out operation code value");

            if (value != null)
                return (OperationCodes) value;
            throw new Exception("The resulting value of parsing operation code was null.");
        }

        private EventCodes GetEventCode(IDictionary<byte, object> data)
        {
            if (!data.TryGetValue(252, out var value)) throw new Exception("Unable to parse out event code value");

            if (value != null)
                return (EventCodes) value;
            throw new Exception("The resulting value of parsing event code was null.");
        }


        //private void ToTableStyleLog(string origin, byte baseCode, string translatedCode, Dictionary<byte, object> parameters)
        //{
        //    try
        //    {
        //        foreach (var p in parameters)
        //        {
        //            if (p.ToString().ToLower().Contains("random"))
        //            {
        //                Console.WriteLine("{0,-20} | {1,-6} | {2,-15} | {3,-30}", origin, baseCode, translatedCode, JsonConvert.SerializeObject(parameters));

        //            }

        //        }

        //        var nom = parameters[1].ToString();
        //        //if (nom.Length > 4 && nom.Length < 10 && nom.Any(x => char.IsLetter(x)))
        //        //Console.WriteLine("{0,-20} | {1,-6} | {2,-15} | {3,-30}", origin, baseCode, translatedCode, JsonConvert.SerializeObject(parameters[1]));
        //    }
        //    catch { }
        //    //Console.WriteLine(JsonConvert.SerializeObject(parameters));


        //    if (parameters.ContainsKey(252))
        //    {

        //    }
        //    //if (PacketArchive.ArchivePacket(origin, baseCode, translatedCode, parameters) || false)
        //    //{
        //    //    //Console.WriteLine("{0,-20} | {1,-6} | {2,-15} | {3,-30}", origin, baseCode, translatedCode,JsonConvert.SerializeObject(parameters));

        //    //}
        //}


        private Type[] GetTypesInNamespace(Assembly assembly, string nameSpace)
        {
            return
              assembly.GetTypes()
                      .Where(t => String.Equals(t.Namespace, nameSpace, StringComparison.Ordinal))
                      .ToArray();
        }
    }
}