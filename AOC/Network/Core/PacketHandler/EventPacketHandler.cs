﻿using System;
using System.Collections.Generic;
using System.Text;
using AOC.Network.Data;
using AOC.Network.Packets.Base;

namespace AOC.Network.Core.PacketHandler
{
    public class EventPacketHandler<T> : GenericPacketHandler<BaseEvent> where T : BaseEvent
    {
        public EventCodes EventCode { get; private set; }
        public Action<T> Action { get; private set; }

        public EventPacketHandler(EventCodes operationCode, Action<T> action)
        {
            EventCode = operationCode;
            Action = action;
        }

        protected internal override void OnHandle(BaseEvent packet)
        {
            // If we are the right packet - process
            //if (packet.Code == (int)EventCode)
            if ((short)packet.Parameters[252] == (int)EventCode) //TODO bugfix
            {
                var instance = (T)Activator.CreateInstance(typeof(T), packet.Parameters);
                Action.Invoke(instance);
            }
            // Always pass to next
            __Next(packet);
        }
    }
}
