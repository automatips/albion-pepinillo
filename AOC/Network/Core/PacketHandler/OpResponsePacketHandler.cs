﻿using System;
using AOC.Network.Data;
using AOC.Network.Packets.Base;

namespace AOC.Network.Core.PacketHandler
{
    public class OpResponsePacketHandler<T> : GenericPacketHandler<BaseOpResponse> where T : BaseOpResponse
    {
        public OpResponsePacketHandler(OperationCodes operationCode, Action<T> action)
        {
            OperationCode = operationCode;
            Action = action;
        }

        public OperationCodes OperationCode { get; }
        public Action<T> Action { get; }

        protected internal override void OnHandle(BaseOpResponse packet)
        {
            // If we are the right packet - process
            if (packet.OperationCode == (int) OperationCode)
            {
                var instance = (T) Activator.CreateInstance(typeof(T), packet.Parameters);
                Action.Invoke(instance);
            }

            // Always pass to next
            __Next(packet);
        }
    }
}