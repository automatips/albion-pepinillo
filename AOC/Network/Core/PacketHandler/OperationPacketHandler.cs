﻿using System;
using AOC.Network.Data;
using AOC.Network.Photon.Protocol16.Core;

namespace AOC.Network.Core.PacketHandler
{
    public class OperationPacketHandler<T> : GenericPacketHandler<OperationResponse> 
    {
        public OperationCodes OperationCode { get; private set; }
        public Action<T> Action { get; private set; }

        public OperationPacketHandler(OperationCodes operationCode, Action<T> action)
        {
            OperationCode = operationCode;
            Action = action;
        }

        protected internal override void OnHandle(OperationResponse packet)
        {
            // If we are the right packet - process
            if (packet.OperationCode == (int)OperationCode)
            {
                var instance = (T)Activator.CreateInstance(typeof(T), packet.Parameters);
                Action.Invoke(instance);
            }
            // Always pass to next
            __Next(packet);
        }
    }
}
