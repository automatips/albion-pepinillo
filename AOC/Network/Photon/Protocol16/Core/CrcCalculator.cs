﻿namespace AOC.Network.Photon.Protocol16.Core
{
    public class CrcCalculator
    {
        public static uint Calculate(byte[] bytes, int length)
        {
            var result = uint.MaxValue;
            var key = 3988292384u;

            for (var i = 0; i < length; i++)
            {
                result ^= bytes[i];
                for (var j = 0; j < 8; j++)
                    if ((result & 1u) > 0u)
                        result = (result >> 1) ^ key;
                    else
                        result >>= 1;
            }

            return result;
        }
    }
}