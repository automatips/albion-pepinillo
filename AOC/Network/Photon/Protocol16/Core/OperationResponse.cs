﻿using System.Collections.Generic;

namespace AOC.Network.Photon.Protocol16.Core
{
    public class OperationResponse
    {
        public OperationResponse(byte operationCode, short returnCode, string debugMessage,
            Dictionary<byte, object> parameters)
        {
            OperationCode = operationCode;
            ReturnCode = returnCode;
            DebugMessage = debugMessage;
            Parameters = parameters;
        }

        public byte OperationCode { get; }
        public short ReturnCode { get; }
        public string DebugMessage { get; }
        public Dictionary<byte, object> Parameters { get; }
    }
}