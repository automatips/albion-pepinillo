﻿namespace AOC.Network.Photon.Protocol16.Core
{
    internal class SegmentedPackage
    {
        public int BytesWritten;
        public int TotalLength;
        public byte[] TotalPayload;
    }
}