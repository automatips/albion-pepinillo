﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace AOC.Library.PacketHelpers
{
    public class PacketArchive
    {
        private static readonly string PacketDirectory = "PacketArchive_new";
        private static readonly Dictionary<string, bool> KnownPackets = new Dictionary<string, bool>();

        static PacketArchive()
        {
            Directory.CreateDirectory(PacketDirectory);
            LoadKnownPackets();
        }

        private static void LoadKnownPackets()
        {
            foreach (var file in Directory.GetFiles(PacketDirectory))
            {
                KnownPackets.Add(file.Replace($"{PacketDirectory}\\", ""), true);

            }
        }

        public static bool ArchivePacket(string origin, byte baseCode, string translatedCode,
            Dictionary<byte, object> parameters)
        {

            if (KnownPackets.ContainsKey($"{origin}{translatedCode}")) return false;

            var packet = new AOCPacket
            {
                BaseCode = baseCode,
                Origin = origin,
                Parameters = parameters,
                TranslatedCode = translatedCode
            };
            var path = Path.GetFullPath($"{PacketDirectory}/{origin}{translatedCode}");
            File.WriteAllText(path,JsonConvert.SerializeObject(packet, Formatting.Indented));
            return true;
        }
    }
}