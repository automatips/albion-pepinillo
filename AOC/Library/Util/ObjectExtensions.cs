﻿using System;
using System.Linq;
using System.Text;
using AOC.Library.Primitive;

namespace AOC.Util
{
    /// <summary>
    ///     Class aimed to abstract readability. I dont know if casting misbehaves on .net core between linux / win . mac
    ///     It also gives us an option to look @ smart casting
    /// </summary>
    public static class ObjectExtensions
    {
        public static long ToInt(this object o)
        {
            return Convert.ToInt64(o);
        }

        public static float[] ToFloatArray(this object o)
        {
            return (float[]) o;
        }

        public static float ToFloat(this object o)
        {
            return (float) o;
        }

        public static Position ToPosition(this object o)
        {
            var array = o.ToFloatArray();
            return new Position(){X = array[0],Y = array[1]};
        }

        public static string[] ToStringArray(this object o)
        {
            return (string[]) o;
        }
        public static long[] ToIntArray(this object o)
        {
            Type expected = new Byte[] { 0 }.GetType();
            Type current = o.GetType();

            if (expected.Equals(current)) 
            { 
                return((System.Byte[])o).Select(item => (long)item).ToArray();
            }

            try
            {
                return ((System.Int16[])o).Select(item => (long)item).ToArray();

            }
            catch
            {
                return new long[0];
            }
            

        }

        public static long[][] ToInt2DArray(this object o)
        {
            return (long[][])o;
        }

        public static double ToDouble(this object o)
        {
            
            return (double)System.Convert.ToDouble(o);
        }

        public static bool ToBoolean(this object o)
        {
            return (bool)o;
        }

        public static bool[] ToBooleanArray(this object o)
        {
            return (bool[])o;
        }

        public static short[] ToInt16Array(this object o)
        {
            if (o == null)
                return new short[0];
            var converted = o.ToByteArray().Select(x=>(short)x).ToArray();
            return converted;
        }

        public static byte[] ToByteArray(this object o)
        {
            var converted = o.ToString();
            var array = Encoding.UTF8.GetBytes(converted);
            return array;
        }
    }
}