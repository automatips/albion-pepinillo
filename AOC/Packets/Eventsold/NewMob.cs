﻿using System;
using System.Collections.Generic;
using AOC.Packets.Generated;

namespace AOC.Packets.Events
{
    public class NewMob : GEventNewMob
    {
        protected NewMob(Dictionary<byte, object> data) : base(data)
        {
            /*
            Rhino Data (4258 HP)
            Key = 0, Value = 12694, Type= System.Int16 // long Object Id ao5
            Key = 1, Value = 41, Type= System.Byte  // short Type Id ao6
            Key = 2, Value = 255, Type= System.Byte // Flagging status ao7
                            Blue = 0,
                            Highland = 1,
                            Forest = 2,
                            Steppe = 3,
                            Mountain = 4,
                            Swamp = 5,
                            Red = byte.MaxValue
            Key = 6, Value = , Type= System.String // apb?
            Key = 7, Value = System.Single[], Type= System.Single[] // Pos // arg apc
            Key = 8, Value = System.Single[], Type= System.Single[] // Pos Target // arg apd
            Key = 9, Value = 26835839, Type= System.Int32 // GameTimeStamp ape
            Key = 10, Value = 171.1836, Type= System.Single // apf (float)
            Key = 11, Value = 2, Type= System.Single // apg (float)
            Key = 13, Value = 4258, Type= System.Single // Health api (float)
            Key = 14, Value = 4258, Type= System.Single // apj (float)
            Key = 16, Value = 26665619, Type= System.Int32 // GameTimeStamp app
            Key = 17, Value = 245, Type= System.Single // float apm
            Key = 18, Value = 245, Type= System.Single // float apn
            Key = 19, Value = 7, Type= System.Single // float apo
            Key = 20, Value = 26835811, Type= System.Int32 // GameTimeStamp app
            Key = 252, Value = 106, Type= System.Int16
 */
            Id = Parameter0;
            IdType = Parameter1;
            LocA = Parameter7;
            LocB = Parameter8;
            DatetimeA = new DateTime(long.Parse(Parameter9.ToString()));
            DatetimeB = new DateTime(long.Parse(Parameter16.ToString()));
            DatetimeC = new DateTime(long.Parse(Parameter20.ToString()));
            HealtUpdate = Parameter13;
            Rarity = Parameter20;
        }

        public long Id { get; }
        public long IdType { get; }
        public float[] LocA { get; }
        public float[] LocB { get; }
        public DateTime DatetimeA { get; }
        public DateTime DatetimeB { get; }
        public DateTime DatetimeC { get; }
        public double HealtUpdate { get; }
        public long Rarity { get; }
    }
}