﻿using System.Collections.Generic;
using AOC.Packets.Generated;

namespace AOC.Packets.Events
{
    public class UpdateFame : GEventUpdateFame
    {
        protected UpdateFame(Dictionary<byte, object> data) : base(data)
        {
            Id = Parameter0;
            FameAmount = Parameter1 / 10000;

        }

        public float Id { get; }
        public long FameAmount { get; }
    }
}