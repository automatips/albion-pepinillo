using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 115
	public class GEventDebugVariablesInfo : BaseEvent
	{
		// From : 1120516
		protected System.Int64 Parameter0 {get;set;}
		// From : 30
		protected System.Int64 Parameter1 {get;set;}
		// From : 255
		protected System.Int64 Parameter2 {get;set;}
		// From : ""
		protected System.String Parameter6 {get;set;}
		// From : [253.75758,305.16357]
		protected System.Single[] Parameter7 {get;set;}
		// From : [254.26141,302.646]
		protected System.Single[] Parameter8 {get;set;}
		// From : 62817271
		protected System.Int64 Parameter9 {get;set;}
		// From : 357.49338
		protected System.Double Parameter10 {get;set;}
		// From : 3.2
		protected System.Double Parameter11 {get;set;}
		// From : 685.0
		protected System.Double Parameter13 {get;set;}
		// From : 685.0
		protected System.Double Parameter14 {get;set;}
		// From : 62730772
		protected System.Int64 Parameter16 {get;set;}
		// From : 112.0
		protected System.Double Parameter17 {get;set;}
		// From : 112.0
		protected System.Double Parameter18 {get;set;}
		// From : 3.0
		protected System.Double Parameter19 {get;set;}
		// From : 62817105
		protected System.Int64 Parameter20 {get;set;}
		// From : 0
		protected System.Int64 Parameter29 {get;set;}
		// From : 115
		protected System.Int64 Parameter252 {get;set;}
		protected GEventDebugVariablesInfo(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.DebugVariablesInfo;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter6 = data[6].ToString();
			Parameter7 = data[7].ToFloatArray();
			Parameter8 = data[8].ToFloatArray();
			Parameter9 = data[9].ToInt();
			Parameter10 = data[10].ToDouble();
			Parameter11 = data[11].ToDouble();
			Parameter13 = data[13].ToDouble();
			Parameter14 = data[14].ToDouble();
			Parameter16 = data[16].ToInt();
			Parameter17 = data[17].ToDouble();
			Parameter18 = data[18].ToDouble();
			Parameter19 = data[19].ToDouble();
			Parameter20 = data[20].ToInt();
			Parameter29 = data[29].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
