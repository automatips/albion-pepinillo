using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 253
	public class GEventSiegeCampClaimStart : BaseEvent
	{
		// From : 1121078
		protected System.Int64 Parameter0 {get;set;}
		// From : true
		protected System.Boolean Parameter1 {get;set;}
		// From : true
		protected System.Boolean Parameter2 {get;set;}
		// From : 253
		protected System.Int64 Parameter252 {get;set;}
		protected GEventSiegeCampClaimStart(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.SiegeCampClaimStart;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToBoolean();
			Parameter2 = data[2].ToBoolean();
			Parameter252 = data[252].ToInt();
		}
	}
}
