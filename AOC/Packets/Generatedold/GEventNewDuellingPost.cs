using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 168
	public class GEventNewDuellingPost : BaseEvent
	{
		// From : 2510621
		protected System.Int64 Parameter0 {get;set;}
		// From : [-53.3927,23.496984]
		protected System.Single[] Parameter1 {get;set;}
		// From : 168
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewDuellingPost(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewDuellingPost;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
