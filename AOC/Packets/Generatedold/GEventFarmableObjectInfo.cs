using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 183
	public class GEventFarmableObjectInfo : BaseEvent
	{
		// From : 302
		protected System.Int64 Parameter0 {get;set;}
		// From : 792000000
		protected System.Int64 Parameter1 {get;set;}
		// From : 637078703417161457
		protected System.Int64 Parameter2 {get;set;}
		// From : true
		protected System.Boolean Parameter3 {get;set;}
		// From : "RQ=="
		protected System.String Parameter6 {get;set;}
		// From : "AQ=="
		protected System.String Parameter7 {get;set;}
		// From : "AA=="
		protected System.String Parameter8 {get;set;}
		// From : [637078703417161457]
		protected System.Int64[] Parameter9 {get;set;}
		// From : ""
		protected System.String Parameter10 {get;set;}
		// From : ""
		protected System.String Parameter11 {get;set;}
		// From : 0.5
		protected System.Double Parameter12 {get;set;}
		// From : -9223372036854775808
		protected System.Int64 Parameter13 {get;set;}
		// From : 183
		protected System.Int64 Parameter252 {get;set;}
		protected GEventFarmableObjectInfo(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.FarmableObjectInfo;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToBoolean();
			Parameter6 = data[6].ToString();
			Parameter7 = data[7].ToString();
			Parameter8 = data[8].ToString();
			Parameter9 = data[9].ToIntArray();
			Parameter10 = data[10].ToString();
			Parameter11 = data[11].ToString();
			Parameter12 = data[12].ToDouble();
			Parameter13 = data[13].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
