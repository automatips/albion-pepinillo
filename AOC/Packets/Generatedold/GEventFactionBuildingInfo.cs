using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 37
	public class GEventFactionBuildingInfo : BaseEvent
	{
		// From : 1752
		protected System.Int64 Parameter0 {get;set;}
		// From : 37
		protected System.Int64 Parameter252 {get;set;}
		protected GEventFactionBuildingInfo(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.FactionBuildingInfo;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
