using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 173
	public class GEventDuelReEnteredArea : BaseEvent
	{
		// From : 63508
		protected System.Int64 Parameter0 {get;set;}
		// From : 173
		protected System.Int64 Parameter252 {get;set;}
		protected GEventDuelReEnteredArea(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.DuelReEnteredArea;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
