using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 31
	public class GEventNewExpeditionAgent : BaseEvent
	{
		// From : 22
		protected System.Int64 Parameter0 {get;set;}
		// From : [18.5,-26.5]
		protected System.Single[] Parameter1 {get;set;}
		// From : "lvXJo5hQ9EyHeExJbBb2Tg=="
		protected System.String Parameter2 {get;set;}
		// From : 4
		protected System.Int64 Parameter3 {get;set;}
		// From : "Recruiter Parker"
		protected System.String Parameter4 {get;set;}
		// From : 287
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewExpeditionAgent(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewExpeditionAgent;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
