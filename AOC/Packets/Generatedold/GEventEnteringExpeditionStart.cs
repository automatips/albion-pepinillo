using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 241
	public class GEventEnteringExpeditionStart : BaseEvent
	{
		// From : 2506271
		protected System.Int64 Parameter0 {get;set;}
		// From : 241
		protected System.Int64 Parameter252 {get;set;}
		protected GEventEnteringExpeditionStart(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.EnteringExpeditionStart;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
