using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 191
	public class GEventAccessStatus : BaseEvent
	{
		// From : 260
		protected System.Int64 Parameter0 {get;set;}
		// From : 3
		protected System.Int64 Parameter1 {get;set;}
		// From : ["@_Owner","@_Guild","@_Everyone","@P_4fbfcb6d-ce5c-4bb2-9c6f-9caf55a2b4f1","@G_d950b314-abdb-4c52-a229-15214238f0d7","@G_5dde9529-3015-44e0-afe3-58de68b22183","@P_1dd09765-57c8-47d9-8fcb-639edd55c2b0","@P_40465ea0-bb01-4036-b48d-4784d9b32c0d","@P_f6512288-7568-46b4-a3e6-0c03efcd0e98","@P_3c7ff5bc-828c-4f36-a95d-8a77626a630f"]
		protected System.String[] Parameter4 {get;set;}
		// From : ["owner","visitor","noaccess","coowner","visitor","visitor","visitor","coowner","visitor","coowner"]
		protected System.String[] Parameter5 {get;set;}
		// From : ["","Sinister Kings","","ANNESTESIA","HustlinHitmans","Academia HustlinHitmans II","Zumbie","ElPepelui","Svannia","Krooocker"]
		protected System.String[] Parameter6 {get;set;}
		// From : 191
		protected System.Int64 Parameter252 {get;set;}
		protected GEventAccessStatus(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.AccessStatus;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter4 = data[4].ToStringArray();
			Parameter5 = data[5].ToStringArray();
			Parameter6 = data[6].ToStringArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
