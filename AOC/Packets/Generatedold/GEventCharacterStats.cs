using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 132
	public class GEventCharacterStats : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : "Fhancuz"
		protected System.String Parameter1 {get;set;}
		// From : ""
		protected System.String Parameter2 {get;set;}
		// From : 0
		protected System.Int64 Parameter3 {get;set;}
		// From : ""
		protected System.String Parameter4 {get;set;}
		// From : "ZYN ZYN ZYN"
		protected System.String Parameter5 {get;set;}
		// From : 41454401974
		protected System.Int64 Parameter6 {get;set;}
		// From : 365170583537
		protected System.Int64 Parameter7 {get;set;}
		// From : 18070.46
		protected System.Double Parameter8 {get;set;}
		// From : 185
		protected System.Int64 Parameter9 {get;set;}
		// From : 16907311327
		protected System.Int64 Parameter10 {get;set;}
		// From : 41974
		protected System.Int64 Parameter11 {get;set;}
		// From : 342494413085
		protected System.Int64 Parameter12 {get;set;}
		// From : 3585420000
		protected System.Int64 Parameter13 {get;set;}
		// From : 541190000
		protected System.Int64 Parameter15 {get;set;}
		// From : 1168270000
		protected System.Int64 Parameter16 {get;set;}
		// From : 4
		protected System.Int64 Parameter23 {get;set;}
		// From : 20
		protected System.Int64 Parameter24 {get;set;}
		// From : 8
		protected System.Int64 Parameter26 {get;set;}
		// From : 4
		protected System.Int64 Parameter27 {get;set;}
		// From : 132
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCharacterStats(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.CharacterStats;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToString();
			Parameter5 = data[5].ToString();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToInt();
			Parameter8 = data[8].ToDouble();
			Parameter9 = data[9].ToInt();
			Parameter10 = data[10].ToInt();
			Parameter11 = data[11].ToInt();
			Parameter12 = data[12].ToInt();
			Parameter13 = data[13].ToInt();
			Parameter15 = data[15].ToInt();
			Parameter16 = data[16].ToInt();
			Parameter23 = data[23].ToInt();
			Parameter24 = data[24].ToInt();
			Parameter26 = data[26].ToInt();
			Parameter27 = data[27].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
