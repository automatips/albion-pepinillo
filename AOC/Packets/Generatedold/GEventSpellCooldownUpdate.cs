using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 224
	public class GEventSpellCooldownUpdate : BaseEvent
	{
		// From : "AwQFDA0="
		protected System.String Parameter0 {get;set;}
		// From : [57158319,57158319,57158319,57158319,57158319]
		protected System.Int64[] Parameter1 {get;set;}
		// From : [57148319,57148319,57148319,57148319,57148319]
		protected System.Int64[] Parameter2 {get;set;}
		// From : 224
		protected System.Int64 Parameter252 {get;set;}
		protected GEventSpellCooldownUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.SpellCooldownUpdate;
			Parameter0 = data[0].ToString();
			Parameter1 = data[1].ToIntArray();
			Parameter2 = data[2].ToIntArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
