using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 210
	public class GEventPartyInvitation : BaseEvent
	{
		// From : "Krooocker"
		protected System.String Parameter0 {get;set;}
		// From : 29973
		protected System.Int64 Parameter1 {get;set;}
		// From : 210
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPartyInvitation(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PartyInvitation;
			Parameter0 = data[0].ToString();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
