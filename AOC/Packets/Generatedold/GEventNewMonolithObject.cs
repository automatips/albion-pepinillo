using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 98
	public class GEventNewMonolithObject : BaseEvent
	{
		// From : 19
		protected System.Int64 Parameter0 {get;set;}
		// From : [10.0,16.0]
		protected System.Single[] Parameter1 {get;set;}
		// From : "7LNG7VHmoUalgOuiEV1nuw=="
		protected System.String Parameter3 {get;set;}
		// From : "June"
		protected System.String Parameter4 {get;set;}
		// From : "mDhrtRso+U2QNtYy5Z4tww=="
		protected System.String Parameter6 {get;set;}
		// From : 0
		protected System.Int64 Parameter7 {get;set;}
		// From : 80080000
		protected System.Int64 Parameter9 {get;set;}
		// From : 0
		protected System.Int64 Parameter11 {get;set;}
		// From : 6
		protected System.Int64 Parameter13 {get;set;}
		// From : 83
		protected System.Int64 Parameter14 {get;set;}
		// From : 8
		protected System.Int64 Parameter15 {get;set;}
		// From : 2
		protected System.Int64 Parameter16 {get;set;}
		// From : 16777215
		protected System.Int64 Parameter17 {get;set;}
		// From : 1.5
		protected System.Double Parameter18 {get;set;}
		// From : -0.2
		protected System.Double Parameter19 {get;set;}
		// From : 0
		protected System.Int64 Parameter20 {get;set;}
		// From : 98
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewMonolithObject(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewMonolithObject;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToString();
			Parameter6 = data[6].ToString();
			Parameter7 = data[7].ToInt();
			Parameter9 = data[9].ToInt();
			Parameter11 = data[11].ToInt();
			Parameter13 = data[13].ToInt();
			Parameter14 = data[14].ToInt();
			Parameter15 = data[15].ToInt();
			Parameter16 = data[16].ToInt();
			Parameter17 = data[17].ToInt();
			Parameter18 = data[18].ToDouble();
			Parameter19 = data[19].ToDouble();
			Parameter20 = data[20].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
