using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//OperationResponse | BaseCode - 1
	public class GOperationResponsePing : BaseOperation
	{
		// From : 146
		protected System.Int64 Parameter255 {get;set;}
		// From : 316
		protected System.Int64 Parameter253 {get;set;}
		protected GOperationResponsePing(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = OperationCodes.Ping;
			Parameter255 = data[255].ToInt();
			Parameter253 = data[253].ToInt();
		}
	}
}
