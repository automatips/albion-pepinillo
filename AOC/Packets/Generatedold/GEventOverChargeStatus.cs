using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 79
	public class GEventOverChargeStatus : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : [-9223372036854775808,-9223372036854775808,-9223372036854775808,-9223372036854775808,-9223372036854775808]
		protected System.Int64[] Parameter2 {get;set;}
		// From : 335
		protected System.Int64 Parameter252 {get;set;}
		protected GEventOverChargeStatus(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.OverChargeStatus;
			Parameter0 = data[0].ToInt();
			Parameter2 = data[2].ToIntArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
