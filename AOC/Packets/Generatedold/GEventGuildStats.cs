using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 135
	public class GEventGuildStats : BaseEvent
	{
		// From : 950383
		protected System.Int64 Parameter0 {get;set;}
		// From : "Sinister Kings"
		protected System.String Parameter1 {get;set;}
		// From : "n/hYzrG/QEKxooIWbbEiBA=="
		protected System.String Parameter2 {get;set;}
		// From : "Going HAM"
		protected System.String Parameter3 {get;set;}
		// From : 180
		protected System.Int64 Parameter4 {get;set;}
		// From : "fivefoot"
		protected System.String Parameter5 {get;set;}
		// From : 1
		protected System.Int64 Parameter6 {get;set;}
		// From : 46670087455
		protected System.Int64 Parameter7 {get;set;}
		// From : 6782939286971
		protected System.Int64 Parameter8 {get;set;}
		// From : 4789
		protected System.Int64 Parameter9 {get;set;}
		// From : 6
		protected System.Int64 Parameter15 {get;set;}
		// From : 936315633705
		protected System.Int64 Parameter17 {get;set;}
		// From : 5110678240754
		protected System.Int64 Parameter18 {get;set;}
		// From : 135131265000
		protected System.Int64 Parameter19 {get;set;}
		// From : 520113961272
		protected System.Int64 Parameter20 {get;set;}
		// From : 6
		protected System.Int64 Parameter26 {get;set;}
		// From : 31
		protected System.Int64 Parameter27 {get;set;}
		// From : 11
		protected System.Int64 Parameter28 {get;set;}
		// From : 15
		protected System.Int64 Parameter29 {get;set;}
		// From : 16777215
		protected System.Int64 Parameter30 {get;set;}
		// From : 0.55
		protected System.Double Parameter31 {get;set;}
		// From : 6767
		protected System.Int64 Parameter33 {get;set;}
		// From : 135
		protected System.Int64 Parameter252 {get;set;}
		protected GEventGuildStats(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.GuildStats;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToString();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToInt();
			Parameter8 = data[8].ToInt();
			Parameter9 = data[9].ToInt();
			Parameter15 = data[15].ToInt();
			Parameter17 = data[17].ToInt();
			Parameter18 = data[18].ToInt();
			Parameter19 = data[19].ToInt();
			Parameter20 = data[20].ToInt();
			Parameter26 = data[26].ToInt();
			Parameter27 = data[27].ToInt();
			Parameter28 = data[28].ToInt();
			Parameter29 = data[29].ToInt();
			Parameter30 = data[30].ToInt();
			Parameter31 = data[31].ToDouble();
			Parameter33 = data[33].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
