using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 100
	public class GEventNewOrbObject : BaseEvent
	{
		// From : 13
		protected System.Int64 Parameter0 {get;set;}
		// From : "Orb"
		protected System.String Parameter1 {get;set;}
		// From : "ORB_GVG"
		protected System.String Parameter2 {get;set;}
		// From : [-150.0,495.0]
		protected System.Single[] Parameter3 {get;set;}
		// From : "7LNG7VHmoUalgOuiEV1nuw=="
		protected System.String Parameter4 {get;set;}
		// From : 100
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewOrbObject(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewOrbObject;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToFloatArray();
			Parameter4 = data[4].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
