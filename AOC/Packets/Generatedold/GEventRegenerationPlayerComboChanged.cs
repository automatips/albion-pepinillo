using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 85
	public class GEventRegenerationPlayerComboChanged : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : 57148319
		protected System.Int64 Parameter1 {get;set;}
		// From : 2122.0
		protected System.Double Parameter2 {get;set;}
		// From : 2122.0
		protected System.Double Parameter3 {get;set;}
		// From : 48.80414
		protected System.Double Parameter4 {get;set;}
		// From : 637186256972606960
		protected System.Int64 Parameter5 {get;set;}
		// From : 269.0
		protected System.Double Parameter6 {get;set;}
		// From : 269.0
		protected System.Double Parameter7 {get;set;}
		// From : 3.6963742
		protected System.Double Parameter8 {get;set;}
		// From : 637186256972606960
		protected System.Int64 Parameter9 {get;set;}
		// From : 669.0
		protected System.Double Parameter11 {get;set;}
		// From : 6.69
		protected System.Double Parameter12 {get;set;}
		// From : 637186256972606960
		protected System.Int64 Parameter13 {get;set;}
		// From : 85
		protected System.Int64 Parameter252 {get;set;}
		protected GEventRegenerationPlayerComboChanged(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.RegenerationPlayerComboChanged;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToDouble();
			Parameter3 = data[3].ToDouble();
			Parameter4 = data[4].ToDouble();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToDouble();
			Parameter7 = data[7].ToDouble();
			Parameter8 = data[8].ToDouble();
			Parameter9 = data[9].ToInt();
			Parameter11 = data[11].ToDouble();
			Parameter12 = data[12].ToDouble();
			Parameter13 = data[13].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
