using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 54
	public class GEventActionOnBuildingFinished : BaseEvent
	{
		// From : 2518566
		protected System.Int64 Parameter0 {get;set;}
		// From : 637186256921527197
		protected System.Int64 Parameter1 {get;set;}
		// From : 1751
		protected System.Int64 Parameter2 {get;set;}
		// From : 22
		protected System.Int64 Parameter3 {get;set;}
		// From : 6
		protected System.Int64 Parameter4 {get;set;}
		// From : 54
		protected System.Int64 Parameter252 {get;set;}
		protected GEventActionOnBuildingFinished(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ActionOnBuildingFinished;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
