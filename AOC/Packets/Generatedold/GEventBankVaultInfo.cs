using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 114
	public class GEventBankVaultInfo : BaseEvent
	{
		// From : 5
		protected System.Int64 Parameter0 {get;set;}
		// From : "0e860295-bb5c-4be1-89d0-9b1bbcba0b99@1001"
		protected System.String Parameter1 {get;set;}
		// From : ["rS1IfFU7y0+LPSJeRa22JQ==","0iXrfdizfUyg+id40usL2Q=="]
		protected System.String[] Parameter2 {get;set;}
		// From : ["equipo","@BUILDINGS_T1_BANK"]
		protected System.String[] Parameter3 {get;set;}
		// From : ["icon_tag_helmet","icon_tag_chest"]
		protected System.String[] Parameter4 {get;set;}
		// From : [-1168500481,724314623]
		protected System.Int64[] Parameter5 {get;set;}
		// From : 370
		protected System.Int64 Parameter252 {get;set;}
		protected GEventBankVaultInfo(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.BankVaultInfo;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToStringArray();
			Parameter3 = data[3].ToStringArray();
			Parameter4 = data[4].ToStringArray();
			Parameter5 = data[5].ToIntArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
