using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 97
	public class GEventNewDynamicGuildLogo : BaseEvent
	{
		// From : "GVG_SEASON_07#3"
		protected System.String Parameter0 {get;set;}
		// From : "SCHEMA_01"
		protected System.String Parameter1 {get;set;}
		// From : "GUILDSYMBOL_EOS"
		protected System.String Parameter2 {get;set;}
		// From : 9
		protected System.Int64 Parameter3 {get;set;}
		// From : 7
		protected System.Int64 Parameter4 {get;set;}
		// From : 15775037
		protected System.Int64 Parameter5 {get;set;}
		// From : 0.5
		protected System.Double Parameter6 {get;set;}
		// From : -0.05
		protected System.Double Parameter7 {get;set;}
		// From : true
		protected System.Boolean Parameter8 {get;set;}
		// From : 353
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewDynamicGuildLogo(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewDynamicGuildLogo;
			Parameter0 = data[0].ToString();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToDouble();
			Parameter7 = data[7].ToDouble();
			Parameter8 = data[8].ToBoolean();
			Parameter252 = data[252].ToInt();
		}
	}
}
