using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 55
	public class GEventItemRerollQualityStart : BaseEvent
	{
		// From : 2508239
		protected System.Int64 Parameter0 {get;set;}
		// From : 56934433
		protected System.Int64 Parameter1 {get;set;}
		// From : 56934583
		protected System.Int64 Parameter2 {get;set;}
		// From : 1750
		protected System.Int64 Parameter3 {get;set;}
		// From : 55
		protected System.Int64 Parameter252 {get;set;}
		protected GEventItemRerollQualityStart(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ItemRerollQualityStart;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
