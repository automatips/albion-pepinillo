using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 35
	public class GEventHarvestableChangeState : BaseEvent
	{
		// From : 7416
		protected System.Int64 Parameter0 {get;set;}
		// From : 0
		protected System.Int64 Parameter2 {get;set;}
		// From : 35
		protected System.Int64 Parameter252 {get;set;}
		protected GEventHarvestableChangeState(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.HarvestableChangeState;
			Parameter0 = data[0].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
