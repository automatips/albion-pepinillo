using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 39
	public class GEventRepairBuildingInfo : BaseEvent
	{
		// From : 1750
		protected System.Int64 Parameter0 {get;set;}
		// From : ""
		protected System.String Parameter6 {get;set;}
		// From : ""
		protected System.String Parameter7 {get;set;}
		// From : 39
		protected System.Int64 Parameter252 {get;set;}
		protected GEventRepairBuildingInfo(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.RepairBuildingInfo;
			Parameter0 = data[0].ToInt();
			Parameter6 = data[6].ToString();
			Parameter7 = data[7].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
