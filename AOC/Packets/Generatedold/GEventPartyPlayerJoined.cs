using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 213
	public class GEventPartyPlayerJoined : BaseEvent
	{
		// From : 21968
		protected System.Int64 Parameter0 {get;set;}
		// From : "SwSakdVlAkW9AFHyI9yAyg=="
		protected System.String Parameter1 {get;set;}
		// From : "MortonBeliever"
		protected System.String Parameter2 {get;set;}
		// From : 0
		protected System.Int64 Parameter3 {get;set;}
		// From : 6
		protected System.Int64 Parameter4 {get;set;}
		// From : 16
		protected System.Int64 Parameter5 {get;set;}
		// From : -1
		protected System.Int64 Parameter6 {get;set;}
		// From : 213
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPartyPlayerJoined(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PartyPlayerJoined;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
