using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 204
	public class GEventUpdateUnlockedAvatars : BaseEvent
	{
		// From : "ERI="
		protected System.String Parameter0 {get;set;}
		// From : 204
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateUnlockedAvatars(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.UpdateUnlockedAvatars;
			Parameter0 = data[0].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
