using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 79
	public class GEventCharacterEquipmentChanged : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : 57148319
		protected System.Int64 Parameter1 {get;set;}
		// From : [1883,0,2583,2733,2204,1790,1562,1895,0,0]
		protected System.Int64[] Parameter2 {get;set;}
		// From : [-1,-1,-1,1732,1810,1885,-1,-1,-1,-1,-1,-1,2208,2061]
		protected System.Int64[] Parameter5 {get;set;}
		// From : true
		protected System.Boolean Parameter6 {get;set;}
		// From : 79
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCharacterEquipmentChanged(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.CharacterEquipmentChanged;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToIntArray();
			Parameter5 = data[5].ToIntArray();
			Parameter6 = data[6].ToBoolean();
			Parameter252 = data[252].ToInt();
		}
	}
}
