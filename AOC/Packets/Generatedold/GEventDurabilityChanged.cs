using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 86
	public class GEventDurabilityChanged : BaseEvent
	{
		// From : [953322,953318,953323,953317,953320,953321]
		protected System.Int64[] Parameter0 {get;set;}
		// From : [107800000,242800000,202300000,98800000,134800000,119800000]
		protected System.Int64[] Parameter1 {get;set;}
		// From : 86
		protected System.Int64 Parameter252 {get;set;}
		protected GEventDurabilityChanged(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.DurabilityChanged;
			Parameter0 = data[0].ToIntArray();
			Parameter1 = data[1].ToIntArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
