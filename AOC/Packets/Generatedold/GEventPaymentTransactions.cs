using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 46
	public class GEventPaymentTransactions : BaseEvent
	{
		// From : ["dPJmLHnrsUut7rlcQwO5wA==","NuH3oLuUEkS5nIUFxTpvkg==","DQeAgb14oEabtvZO9cZoJw=="]
		protected System.String[] Parameter0 {get;set;}
		// From : ["Palurdo","Palurdo","Palurdo"]
		protected System.String[] Parameter1 {get;set;}
		// From : ["generic-shop-transaction","referral-season-reward-game-transaction","anniversary-2"]
		protected System.String[] Parameter2 {get;set;}
		// From : [false,false,false]
		protected System.Boolean[] Parameter3 {get;set;}
		// From : ["","",""]
		protected System.String[] Parameter4 {get;set;}
		// From : 302
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPaymentTransactions(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PaymentTransactions;
			Parameter0 = data[0].ToStringArray();
			Parameter1 = data[1].ToStringArray();
			Parameter2 = data[2].ToStringArray();
			Parameter3 = data[3].ToBooleanArray();
			Parameter4 = data[4].ToStringArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
