using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 131
	public class GEventForcedMovementCancel : BaseEvent
	{
		// From : 2505219
		protected System.Int64 Parameter0 {get;set;}
		// From : 131
		protected System.Int64 Parameter252 {get;set;}
		protected GEventForcedMovementCancel(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ForcedMovementCancel;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
