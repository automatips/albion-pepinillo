using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 10
	public class GEventActiveSpellEffectsUpdate : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : [929,703,2061,459]
		protected System.Int64[] Parameter1 {get;set;}
		// From : [100.0,100.0,397.4152,100.0]
		protected System.Single[] Parameter2 {get;set;}
		// From : [57168364,57113194,56677220,53689337]
		protected System.Int64[] Parameter3 {get;set;}
		// From : "AQEBAQ=="
		protected System.String Parameter4 {get;set;}
		// From : "27YNWwA="
		protected System.String Parameter6 {get;set;}
		// From : [3000,60000,60000,60000,60000,60000,60000,1800000,1800000]
		protected System.Int64[] Parameter7 {get;set;}
		// From : "Dw=="
		protected System.String Parameter8 {get;set;}
		// From : "AAAAAA=="
		protected System.String Parameter9 {get;set;}
		// From : 10
		protected System.Int64 Parameter252 {get;set;}
		protected GEventActiveSpellEffectsUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ActiveSpellEffectsUpdate;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToIntArray();
			Parameter2 = data[2].ToFloatArray();
			Parameter3 = data[3].ToIntArray();
			Parameter4 = data[4].ToString();
			Parameter6 = data[6].ToString();
			Parameter7 = data[7].ToIntArray();
			Parameter8 = data[8].ToString();
			Parameter9 = data[9].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
