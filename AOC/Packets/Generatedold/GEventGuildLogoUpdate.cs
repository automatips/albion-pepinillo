using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 177
	public class GEventGuildLogoUpdate : BaseEvent
	{
		// From : "G8xFBvj8wEGi4nEGJ3nDbw=="
		protected System.String Parameter0 {get;set;}
		// From : 8
		protected System.Int64 Parameter2 {get;set;}
		// From : 2
		protected System.Int64 Parameter3 {get;set;}
		// From : 66
		protected System.Int64 Parameter4 {get;set;}
		// From : 16777215
		protected System.Int64 Parameter5 {get;set;}
		// From : 0.65000004
		protected System.Double Parameter6 {get;set;}
		// From : 177
		protected System.Int64 Parameter252 {get;set;}
		protected GEventGuildLogoUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.GuildLogoUpdate;
			Parameter0 = data[0].ToString();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToDouble();
			Parameter252 = data[252].ToInt();
		}
	}
}
