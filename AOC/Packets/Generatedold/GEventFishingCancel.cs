using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 67
	public class GEventFishingCancel : BaseEvent
	{
		// From : 953314
		protected System.Int64 Parameter0 {get;set;}
		// From : 12
		protected System.Int64 Parameter1 {get;set;}
		// From : 1
		protected System.Int64 Parameter2 {get;set;}
		// From : 323
		protected System.Int64 Parameter252 {get;set;}
		protected GEventFishingCancel(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.FishingCancel;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
