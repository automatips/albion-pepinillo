using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 40
	public class GEventNewPortalExit : BaseEvent
	{
		// From : 3277
		protected System.Int64 Parameter0 {get;set;}
		// From : [-240.0,-240.0]
		protected System.Single[] Parameter1 {get;set;}
		// From : "WN3lTpT15ESdNif5rW01sQ=="
		protected System.String Parameter2 {get;set;}
		// From : 296
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewPortalExit(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewPortalExit;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter2 = data[2].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
