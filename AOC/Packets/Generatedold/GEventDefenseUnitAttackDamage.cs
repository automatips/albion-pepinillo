using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 25
	public class GEventDefenseUnitAttackDamage : BaseEvent
	{
		// From : 4593
		protected System.Int64 Parameter0 {get;set;}
		// From : 52955948
		protected System.Int64 Parameter1 {get;set;}
		// From : 63429
		protected System.Int64 Parameter2 {get;set;}
		// From : 52956659
		protected System.Int64 Parameter3 {get;set;}
		// From : 281
		protected System.Int64 Parameter252 {get;set;}
		protected GEventDefenseUnitAttackDamage(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.DefenseUnitAttackDamage;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
