using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 244
	public class GEventArenaRegistrationInfo : BaseEvent
	{
		// From : 4
		protected System.Int64 Parameter2 {get;set;}
		// From : 637186174276955884
		protected System.Int64 Parameter3 {get;set;}
		// From : 16
		protected System.Int64 Parameter4 {get;set;}
		// From : "Palurdo"
		protected System.String Parameter5 {get;set;}
		// From : 244
		protected System.Int64 Parameter252 {get;set;}
		protected GEventArenaRegistrationInfo(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ArenaRegistrationInfo;
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
