using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//OperationRequest | BaseCode - 1
	public class GOperationRequestPing : BaseOperation
	{
		// From : 26
		protected System.Int64 Parameter0 {get;set;}
		// From : 26
		protected System.Int64 Parameter1 {get;set;}
		// From : 26
		protected System.Int64 Parameter2 {get;set;}
		// From : 786432
		protected System.Int64 Parameter3 {get;set;}
		// From : "1024x768"
		protected System.String Parameter4 {get;set;}
		// From : 296
		protected System.Int64 Parameter253 {get;set;}
		protected GOperationRequestPing(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = OperationCodes.Ping;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToString();
			Parameter253 = data[253].ToInt();
		}
	}
}
