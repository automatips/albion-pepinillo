using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 38
	public class GEventCraftBuildingInfo : BaseEvent
	{
		// From : 1735
		protected System.Int64 Parameter0 {get;set;}
		// From : ""
		protected System.String Parameter5 {get;set;}
		// From : ""
		protected System.String Parameter6 {get;set;}
		// From : 38
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCraftBuildingInfo(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.CraftBuildingInfo;
			Parameter0 = data[0].ToInt();
			Parameter5 = data[5].ToString();
			Parameter6 = data[6].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
