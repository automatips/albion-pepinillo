using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 150
	public class GEventKilledPlayer : BaseEvent
	{
		// From : 77485
		protected System.Int64 Parameter0 {get;set;}
		// From : 77485
		protected System.Int64 Parameter1 {get;set;}
		// From : "Palurdo"
		protected System.String Parameter2 {get;set;}
		// From : 1132378472
		protected System.Int64 Parameter3 {get;set;}
		// From : 150
		protected System.Int64 Parameter252 {get;set;}
		protected GEventKilledPlayer(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.KilledPlayer;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
