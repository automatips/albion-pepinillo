using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 252
	public class GEventOtherGrabbedLoot : BaseEvent
	{
		// From : 953689
		protected System.Int64 Parameter0 {get;set;}
		// From : "JokerCRI"
		protected System.String Parameter2 {get;set;}
		// From : true
		protected System.Boolean Parameter3 {get;set;}
		// From : 598500
		protected System.Int64 Parameter5 {get;set;}
		// From : 252
		protected System.Int64 Parameter252 {get;set;}
		protected GEventOtherGrabbedLoot(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.OtherGrabbedLoot;
			Parameter0 = data[0].ToInt();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToBoolean();
			Parameter5 = data[5].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
