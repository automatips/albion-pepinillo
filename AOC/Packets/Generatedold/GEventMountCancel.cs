using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 194
	public class GEventMountCancel : BaseEvent
	{
		// From : 1154752
		protected System.Int64 Parameter0 {get;set;}
		// From : 194
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMountCancel(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.MountCancel;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
