using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 251
	public class GEventInCombatStateUpdate : BaseEvent
	{
		// From : 953314
		protected System.Int64 Parameter0 {get;set;}
		// From : 251
		protected System.Int64 Parameter252 {get;set;}
		protected GEventInCombatStateUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.InCombatStateUpdate;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
