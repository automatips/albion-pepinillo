using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 138
	public class GEventFinishedAchievement : BaseEvent
	{
		// From : 2383826
		protected System.Int64 Parameter0 {get;set;}
		// From : 18
		protected System.Int64 Parameter1 {get;set;}
		// From : 7
		protected System.Int64 Parameter2 {get;set;}
		// From : 138
		protected System.Int64 Parameter252 {get;set;}
		protected GEventFinishedAchievement(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.FinishedAchievement;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
