using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 207
	public class GEventNewIslandManagement : BaseEvent
	{
		// From : 260
		protected System.Int64 Parameter0 {get;set;}
		// From : [176.5,65.5]
		protected System.Single[] Parameter1 {get;set;}
		// From : "CcxeEDirHk+YRdRgo8iwPg=="
		protected System.String Parameter2 {get;set;}
		// From : "n/hYzrG/QEKxooIWbbEiBA=="
		protected System.String Parameter3 {get;set;}
		// From : "Palurdo"
		protected System.String Parameter4 {get;set;}
		// From : 207
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewIslandManagement(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewIslandManagement;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
