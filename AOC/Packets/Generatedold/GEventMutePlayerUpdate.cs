using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 106
	public class GEventMutePlayerUpdate : BaseEvent
	{
		// From : 0
		protected System.Int64 Parameter1 {get;set;}
		// From : 0
		protected System.Int64 Parameter2 {get;set;}
		// From : ["Sarkoso"]
		protected System.String[] Parameter3 {get;set;}
		// From : 362
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMutePlayerUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.MutePlayerUpdate;
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToStringArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
