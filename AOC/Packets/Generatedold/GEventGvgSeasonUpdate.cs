using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 60
	public class GEventGvgSeasonUpdate : BaseEvent
	{
		// From : 8
		protected System.Int64 Parameter0 {get;set;}
		// From : 7
		protected System.Int64 Parameter1 {get;set;}
		// From : "7d9vs7I27EKHradQpe+LDg=="
		protected System.String Parameter2 {get;set;}
		// From : "ERROR 4O4"
		protected System.String Parameter3 {get;set;}
		// From : "wqgahVsvDk+o44R0bsmIUw=="
		protected System.String Parameter4 {get;set;}
		// From : "EQMS Praise Be Archersnon"
		protected System.String Parameter5 {get;set;}
		// From : "ARCH"
		protected System.String Parameter6 {get;set;}
		// From : 2
		protected System.Int64 Parameter8 {get;set;}
		// From : 5
		protected System.Int64 Parameter9 {get;set;}
		// From : 82
		protected System.Int64 Parameter10 {get;set;}
		// From : 13483934
		protected System.Int64 Parameter11 {get;set;}
		// From : 0.7
		protected System.Double Parameter12 {get;set;}
		// From : 0.2
		protected System.Double Parameter13 {get;set;}
		// From : 300404
		protected System.Int64 Parameter14 {get;set;}
		// From : 1
		protected System.Int64 Parameter15 {get;set;}
		// From : 316
		protected System.Int64 Parameter252 {get;set;}
		protected GEventGvgSeasonUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.GvgSeasonUpdate;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToString();
			Parameter5 = data[5].ToString();
			Parameter6 = data[6].ToString();
			Parameter8 = data[8].ToInt();
			Parameter9 = data[9].ToInt();
			Parameter10 = data[10].ToInt();
			Parameter11 = data[11].ToInt();
			Parameter12 = data[12].ToDouble();
			Parameter13 = data[13].ToDouble();
			Parameter14 = data[14].ToInt();
			Parameter15 = data[15].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
