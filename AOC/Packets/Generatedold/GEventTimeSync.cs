using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 146
	public class GEventTimeSync : BaseEvent
	{
		// From : 57328667
		protected System.Int64 Parameter0 {get;set;}
		// From : -990677857
		protected System.Int64 Parameter1 {get;set;}
		// From : 146
		protected System.Int64 Parameter252 {get;set;}
		protected GEventTimeSync(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.TimeSync;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
