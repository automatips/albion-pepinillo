using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 41
	public class GEventNewRandomDungeonExit : BaseEvent
	{
		// From : 937169
		protected System.Int64 Parameter0 {get;set;}
		// From : [304.0,22.0]
		protected System.Single[] Parameter1 {get;set;}
		// From : "SHARED_RANDOM_EXIT_10x10_PORTAL_SOLO"
		protected System.String Parameter3 {get;set;}
		// From : true
		protected System.Boolean Parameter4 {get;set;}
		// From : true
		protected System.Boolean Parameter6 {get;set;}
		// From : 297
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewRandomDungeonExit(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewRandomDungeonExit;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToBoolean();
			Parameter6 = data[6].ToBoolean();
			Parameter252 = data[252].ToInt();
		}
	}
}
