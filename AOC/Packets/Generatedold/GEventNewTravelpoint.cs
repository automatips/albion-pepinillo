using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 195
	public class GEventNewTravelpoint : BaseEvent
	{
		// From : 259
		protected System.Int64 Parameter0 {get;set;}
		// From : [177.0,53.0]
		protected System.Single[] Parameter1 {get;set;}
		// From : "17TK5uyQuUuuaWU/HQG8KA=="
		protected System.String Parameter2 {get;set;}
		// From : 195
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewTravelpoint(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewTravelpoint;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter2 = data[2].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
