using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 250
	public class GEventPlayerCounts : BaseEvent
	{
		// From : 1
		protected System.Int64 Parameter0 {get;set;}
		// From : -1
		protected System.Int64 Parameter1 {get;set;}
		// From : 250
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPlayerCounts(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PlayerCounts;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
