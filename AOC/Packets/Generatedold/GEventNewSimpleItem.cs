using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 26
	public class GEventNewSimpleItem : BaseEvent
	{
		// From : 342
		protected System.Int64 Parameter0 {get;set;}
		// From : 115
		protected System.Int64 Parameter1 {get;set;}
		// From : 1
		protected System.Int64 Parameter2 {get;set;}
		// From : 26
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewSimpleItem(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewSimpleItem;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
