using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 205
	public class GEventUpdateUnlockedAvatarRings : BaseEvent
	{
		// From : "FRYLDBcYGQo="
		protected System.String Parameter0 {get;set;}
		// From : 205
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateUnlockedAvatarRings(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.UpdateUnlockedAvatarRings;
			Parameter0 = data[0].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
