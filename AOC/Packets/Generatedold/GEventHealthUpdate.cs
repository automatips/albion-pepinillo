using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 6
	public class GEventHealthUpdate : BaseEvent
	{
		// From : 953314
		protected System.Int64 Parameter0 {get;set;}
		// From : 57052089
		protected System.Int64 Parameter1 {get;set;}
		// From : -66.0
		protected System.Double Parameter2 {get;set;}
		// From : 1543.0
		protected System.Double Parameter3 {get;set;}
		// From : 1
		protected System.Int64 Parameter4 {get;set;}
		// From : 0
		protected System.Int64 Parameter5 {get;set;}
		// From : 948510
		protected System.Int64 Parameter6 {get;set;}
		// From : -1
		protected System.Int64 Parameter7 {get;set;}
		// From : 6
		protected System.Int64 Parameter252 {get;set;}
		protected GEventHealthUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.HealthUpdate;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToDouble();
			Parameter3 = data[3].ToDouble();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
