using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 202
	public class GEventLootEquipmentChanged : BaseEvent
	{
		// From : 2355815
		protected System.Int64 Parameter0 {get;set;}
		// From : "AAAAAAAAAAAAAA=="
		protected System.String Parameter1 {get;set;}
		// From : 202
		protected System.Int64 Parameter252 {get;set;}
		protected GEventLootEquipmentChanged(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.LootEquipmentChanged;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
