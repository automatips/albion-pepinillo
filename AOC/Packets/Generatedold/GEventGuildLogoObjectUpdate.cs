using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 185
	public class GEventGuildLogoObjectUpdate : BaseEvent
	{
		// From : 268
		protected System.Int64 Parameter0 {get;set;}
		// From : 7
		protected System.Int64 Parameter1 {get;set;}
		// From : 9
		protected System.Int64 Parameter2 {get;set;}
		// From : 2
		protected System.Int64 Parameter3 {get;set;}
		// From : 8
		protected System.Int64 Parameter4 {get;set;}
		// From : 15775037
		protected System.Int64 Parameter5 {get;set;}
		// From : 0.75
		protected System.Double Parameter6 {get;set;}
		// From : 0.1
		protected System.Double Parameter7 {get;set;}
		// From : 185
		protected System.Int64 Parameter252 {get;set;}
		protected GEventGuildLogoObjectUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.GuildLogoObjectUpdate;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToDouble();
			Parameter7 = data[7].ToDouble();
			Parameter252 = data[252].ToInt();
		}
	}
}
