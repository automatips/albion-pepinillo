using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 64
	public class GEventChatSay : BaseEvent
	{
		// From : 2516845
		protected System.Int64 Parameter0 {get;set;}
		// From : "archiduiiin"
		protected System.String Parameter1 {get;set;}
		// From : "selling 16 Rough Logs"
		protected System.String Parameter2 {get;set;}
		// From : 0
		protected System.Int64 Parameter5 {get;set;}
		// From : 64
		protected System.Int64 Parameter252 {get;set;}
		protected GEventChatSay(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ChatSay;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToString();
			Parameter5 = data[5].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
