using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 234
	public class GEventFullQuestInfo : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : -1
		protected System.Int64 Parameter1 {get;set;}
		// From : ""
		protected System.String Parameter3 {get;set;}
		// From : 0
		protected System.Int64 Parameter4 {get;set;}
		// From : 255
		protected System.Int64 Parameter6 {get;set;}
		// From : -9223372036854775808
		protected System.Int64 Parameter7 {get;set;}
		// From : 1
		protected System.Int64 Parameter8 {get;set;}
		// From : ""
		protected System.String Parameter9 {get;set;}
		// From : 234
		protected System.Int64 Parameter252 {get;set;}
		protected GEventFullQuestInfo(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.FullQuestInfo;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToInt();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToInt();
			Parameter8 = data[8].ToInt();
			Parameter9 = data[9].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
