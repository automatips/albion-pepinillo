using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 113
	public class GEventNewMob : BaseEvent
	{
		// From : 317
		protected System.Int64 Parameter0 {get;set;}
		// From : 27
		protected System.Int64 Parameter1 {get;set;}
		// From : 255
		protected System.Int64 Parameter2 {get;set;}
		// From : ""
		protected System.String Parameter6 {get;set;}
		// From : [172.96603,83.766266]
		protected System.Single[] Parameter7 {get;set;}
		// From : [173.10606,83.500946]
		protected System.Single[] Parameter8 {get;set;}
		// From : 57267714
		protected System.Int64 Parameter9 {get;set;}
		// From : 152.1759
		protected System.Double Parameter10 {get;set;}
		// From : 3.0
		protected System.Double Parameter11 {get;set;}
		// From : 20.0
		protected System.Double Parameter13 {get;set;}
		// From : 20.0
		protected System.Double Parameter14 {get;set;}
		// From : 57143731
		protected System.Int64 Parameter16 {get;set;}
		// From : 57143731
		protected System.Int64 Parameter20 {get;set;}
		// From : 0
		protected System.Int64 Parameter28 {get;set;}
		// From : 113
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewMob(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewMob;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter6 = data[6].ToString();
			Parameter7 = data[7].ToFloatArray();
			Parameter8 = data[8].ToFloatArray();
			Parameter9 = data[9].ToInt();
			Parameter10 = data[10].ToDouble();
			Parameter11 = data[11].ToDouble();
			Parameter13 = data[13].ToDouble();
			Parameter14 = data[14].ToDouble();
			Parameter16 = data[16].ToInt();
			Parameter20 = data[20].ToInt();
			Parameter28 = data[28].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
