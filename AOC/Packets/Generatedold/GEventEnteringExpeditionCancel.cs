using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 242
	public class GEventEnteringExpeditionCancel : BaseEvent
	{
		// From : 2506271
		protected System.Int64 Parameter0 {get;set;}
		// From : true
		protected System.Boolean Parameter1 {get;set;}
		// From : 242
		protected System.Int64 Parameter252 {get;set;}
		protected GEventEnteringExpeditionCancel(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.EnteringExpeditionCancel;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToBoolean();
			Parameter252 = data[252].ToInt();
		}
	}
}
