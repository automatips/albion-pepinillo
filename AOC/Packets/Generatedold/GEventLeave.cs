using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 1
	public class GEventLeave : BaseEvent
	{
		// From : 346
		protected System.Int64 Parameter0 {get;set;}
		// From : 1
		protected System.Int64 Parameter252 {get;set;}
		protected GEventLeave(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.Leave;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
