using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 149
	public class GEventGameEvent : BaseEvent
	{
		// From : 29
		protected System.Int64 Parameter0 {get;set;}
		// From : 1
		protected System.Int64 Parameter1 {get;set;}
		// From : "{\"reason\":\"ChallengeeKnockedDownByChallenger\",\"challenger\":\"Bakedluka\",\"challengee\":\"Majkeel\",\"wager\":\"20000\"}"
		protected System.String Parameter2 {get;set;}
		// From : 637186255906016292
		protected System.Int64 Parameter3 {get;set;}
		// From : 149
		protected System.Int64 Parameter252 {get;set;}
		protected GEventGameEvent(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.GameEvent;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
