using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 72
	public class GEventUpdateFame : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : 713384497329
		protected System.Int64 Parameter1 {get;set;}
		// From : 100000
		protected System.Int64 Parameter2 {get;set;}
		// From : 1
		protected System.Int64 Parameter3 {get;set;}
		// From : 1.0
		protected System.Double Parameter6 {get;set;}
		// From : 72
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateFame(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.UpdateFame;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter6 = data[6].ToDouble();
			Parameter252 = data[252].ToInt();
		}
	}
}
