using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 212
	public class GEventPartyDisbanded : BaseEvent
	{
		// From : 212
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPartyDisbanded(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PartyDisbanded;
			Parameter252 = data[252].ToInt();
		}
	}
}
