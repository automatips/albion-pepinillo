using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 96
	public class GEventNewInformationProvider : BaseEvent
	{
		// From : 25
		protected System.Int64 Parameter0 {get;set;}
		// From : [-28.0,60.0]
		protected System.Single[] Parameter1 {get;set;}
		// From : "@HALLOFFAME_SEASONSTORY_07"
		protected System.String Parameter2 {get;set;}
		// From : 8.0
		protected System.Double Parameter3 {get;set;}
		// From : 7
		protected System.Int64 Parameter5 {get;set;}
		// From : 352
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewInformationProvider(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewInformationProvider;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToDouble();
			Parameter5 = data[5].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
