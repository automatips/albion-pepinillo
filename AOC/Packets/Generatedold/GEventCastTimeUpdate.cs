using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 15
	public class GEventCastTimeUpdate : BaseEvent
	{
		// From : 942872
		protected System.Int64 Parameter0 {get;set;}
		// From : 637186255827539242
		protected System.Int64 Parameter1 {get;set;}
		// From : 1
		protected System.Int64 Parameter3 {get;set;}
		// From : 15
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCastTimeUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.CastTimeUpdate;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
