using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 129
	public class GEventClusterInfoUpdate : BaseEvent
	{
		// From : "4216"
		protected System.String Parameter0 {get;set;}
		// From : ""
		protected System.String Parameter2 {get;set;}
		// From : "Aw=="
		protected System.String Parameter19 {get;set;}
		// From : [9223372036854775807]
		protected System.Int64[] Parameter20 {get;set;}
		// From : [1]
		protected System.Int64[] Parameter21 {get;set;}
		// From : []
		protected System.Single[] Parameter22 {get;set;}
		// From : []
		protected System.Single[] Parameter23 {get;set;}
		// From : ""
		protected System.String Parameter24 {get;set;}
		// From : ""
		protected System.String Parameter25 {get;set;}
		// From : 129
		protected System.Int64 Parameter252 {get;set;}
		protected GEventClusterInfoUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ClusterInfoUpdate;
			Parameter0 = data[0].ToString();
			Parameter2 = data[2].ToString();
			Parameter19 = data[19].ToString();
			Parameter20 = data[20].ToIntArray();
			Parameter21 = data[21].ToIntArray();
			Parameter22 = data[22].ToFloatArray();
			Parameter23 = data[23].ToFloatArray();
			Parameter24 = data[24].ToString();
			Parameter25 = data[25].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
