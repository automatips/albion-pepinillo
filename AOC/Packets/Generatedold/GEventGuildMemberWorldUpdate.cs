using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 95
	public class GEventGuildMemberWorldUpdate : BaseEvent
	{
		// From : "n/hYzrG/QEKxooIWbbEiBA=="
		protected System.String Parameter0 {get;set;}
		// From : "vhq+Ww44FEyqgwIvRT7GGQ=="
		protected System.String Parameter1 {get;set;}
		// From : 63429
		protected System.Int64 Parameter2 {get;set;}
		// From : "Sinister Kings"
		protected System.String Parameter3 {get;set;}
		// From : 6
		protected System.Int64 Parameter4 {get;set;}
		// From : 31
		protected System.Int64 Parameter5 {get;set;}
		// From : 11
		protected System.Int64 Parameter6 {get;set;}
		// From : 15
		protected System.Int64 Parameter7 {get;set;}
		// From : 16777215
		protected System.Int64 Parameter8 {get;set;}
		// From : 0.55
		protected System.Double Parameter9 {get;set;}
		// From : "bMcwYf4DDUil7cjRviSGJg=="
		protected System.String Parameter11 {get;set;}
		// From : "BACON"
		protected System.String Parameter12 {get;set;}
		// From : 95
		protected System.Int64 Parameter252 {get;set;}
		protected GEventGuildMemberWorldUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.GuildMemberWorldUpdate;
			Parameter0 = data[0].ToString();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToInt();
			Parameter8 = data[8].ToInt();
			Parameter9 = data[9].ToDouble();
			Parameter11 = data[11].ToString();
			Parameter12 = data[12].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
