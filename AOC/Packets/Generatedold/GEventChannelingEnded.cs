using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 20
	public class GEventChannelingEnded : BaseEvent
	{
		// From : 953689
		protected System.Int64 Parameter0 {get;set;}
		// From : 3
		protected System.Int64 Parameter1 {get;set;}
		// From : 9
		protected System.Int64 Parameter2 {get;set;}
		// From : 20
		protected System.Int64 Parameter252 {get;set;}
		protected GEventChannelingEnded(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ChannelingEnded;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
