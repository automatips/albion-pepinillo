using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 52
	public class GEventActionOnBuildingStart : BaseEvent
	{
		// From : 2517851
		protected System.Int64 Parameter0 {get;set;}
		// From : 1751
		protected System.Int64 Parameter1 {get;set;}
		// From : 637186256911125051
		protected System.Int64 Parameter2 {get;set;}
		// From : 637186256941125051
		protected System.Int64 Parameter3 {get;set;}
		// From : 22
		protected System.Int64 Parameter4 {get;set;}
		// From : 6
		protected System.Int64 Parameter5 {get;set;}
		// From : 52
		protected System.Int64 Parameter252 {get;set;}
		protected GEventActionOnBuildingStart(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ActionOnBuildingStart;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
