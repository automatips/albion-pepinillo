using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 75
	public class GEventOutpostUpdate : BaseEvent
	{
		// From : 6500
		protected System.Int64 Parameter0 {get;set;}
		// From : 2
		protected System.Int64 Parameter1 {get;set;}
		// From : 2
		protected System.Int64 Parameter2 {get;set;}
		// From : 2
		protected System.Int64 Parameter3 {get;set;}
		// From : 9223372036854775807
		protected System.Int64 Parameter4 {get;set;}
		// From : 2
		protected System.Int64 Parameter5 {get;set;}
		// From : 10000000
		protected System.Int64 Parameter6 {get;set;}
		// From : 10000000
		protected System.Int64 Parameter7 {get;set;}
		// From : 637185687280520558
		protected System.Int64 Parameter8 {get;set;}
		// From : 331
		protected System.Int64 Parameter252 {get;set;}
		protected GEventOutpostUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.OutpostUpdate;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToInt();
			Parameter8 = data[8].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
