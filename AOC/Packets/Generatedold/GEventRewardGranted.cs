using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 243
	public class GEventRewardGranted : BaseEvent
	{
		// From : 115
		protected System.Int64 Parameter1 {get;set;}
		// From : 1
		protected System.Int64 Parameter3 {get;set;}
		// From : 243
		protected System.Int64 Parameter252 {get;set;}
		protected GEventRewardGranted(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.RewardGranted;
			Parameter1 = data[1].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
