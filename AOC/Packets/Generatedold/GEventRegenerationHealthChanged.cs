using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 80
	public class GEventRegenerationHealthChanged : BaseEvent
	{
		// From : 954799
		protected System.Int64 Parameter0 {get;set;}
		// From : 57095930
		protected System.Int64 Parameter1 {get;set;}
		// From : 2775.0
		protected System.Double Parameter2 {get;set;}
		// From : 2775.0
		protected System.Double Parameter3 {get;set;}
		// From : 70.249344
		protected System.Double Parameter4 {get;set;}
		// From : 637186256448717379
		protected System.Int64 Parameter5 {get;set;}
		// From : 80
		protected System.Int64 Parameter252 {get;set;}
		protected GEventRegenerationHealthChanged(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.RegenerationHealthChanged;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToDouble();
			Parameter3 = data[3].ToDouble();
			Parameter4 = data[4].ToDouble();
			Parameter5 = data[5].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
