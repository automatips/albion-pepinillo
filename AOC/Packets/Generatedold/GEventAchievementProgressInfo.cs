using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 139
	public class GEventAchievementProgressInfo : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : 386
		protected System.Int64 Parameter1 {get;set;}
		// From : 0.25742856
		protected System.Double Parameter3 {get;set;}
		// From : "[[18025000]]"
		protected System.String Parameter4 {get;set;}
		// From : 139
		protected System.Int64 Parameter252 {get;set;}
		protected GEventAchievementProgressInfo(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.AchievementProgressInfo;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter3 = data[3].ToDouble();
			Parameter4 = data[4].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
