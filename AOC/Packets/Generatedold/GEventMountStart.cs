using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 193
	public class GEventMountStart : BaseEvent
	{
		// From : 2518566
		protected System.Int64 Parameter0 {get;set;}
		// From : 57143494
		protected System.Int64 Parameter1 {get;set;}
		// From : 1927
		protected System.Int64 Parameter2 {get;set;}
		// From : true
		protected System.Boolean Parameter3 {get;set;}
		// From : 193
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMountStart(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.MountStart;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToBoolean();
			Parameter252 = data[252].ToInt();
		}
	}
}
