using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 192
	public class GEventMounted : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : 57168364
		protected System.Int64 Parameter1 {get;set;}
		// From : 527.0
		protected System.Double Parameter4 {get;set;}
		// From : 6.69
		protected System.Double Parameter5 {get;set;}
		// From : 637186257172905872
		protected System.Int64 Parameter6 {get;set;}
		// From : true
		protected System.Boolean Parameter7 {get;set;}
		// From : true
		protected System.Boolean Parameter8 {get;set;}
		// From : 104.69832
		protected System.Double Parameter10 {get;set;}
		// From : 192
		protected System.Int64 Parameter252 {get;set;}
		protected GEventMounted(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.Mounted;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter4 = data[4].ToDouble();
			Parameter5 = data[5].ToDouble();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToBoolean();
			Parameter8 = data[8].ToBoolean();
			Parameter10 = data[10].ToDouble();
			Parameter252 = data[252].ToInt();
		}
	}
}
