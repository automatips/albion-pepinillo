using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 152
	public class GEventKnockedDown : BaseEvent
	{
		// From : 2470439
		protected System.Int64 Parameter0 {get;set;}
		// From : 56601530
		protected System.Int64 Parameter1 {get;set;}
		// From : 2456241
		protected System.Int64 Parameter2 {get;set;}
		// From : "Treewok"
		protected System.String Parameter3 {get;set;}
		// From : true
		protected System.Boolean Parameter5 {get;set;}
		// From : 152
		protected System.Int64 Parameter252 {get;set;}
		protected GEventKnockedDown(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.KnockedDown;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToString();
			Parameter5 = data[5].ToBoolean();
			Parameter252 = data[252].ToInt();
		}
	}
}
