using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 31
	public class GEventNewSimpleHarvestableObjectList : BaseEvent
	{
		// From : "Fys="
		protected System.String Parameter0 {get;set;}
		// From : "AAA="
		protected System.String Parameter1 {get;set;}
		// From : "AQE="
		protected System.String Parameter2 {get;set;}
		// From : [237.5,85.5,235.5,84.5]
		protected System.Single[] Parameter3 {get;set;}
		// From : "AgI="
		protected System.String Parameter4 {get;set;}
		// From : 31
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewSimpleHarvestableObjectList(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewSimpleHarvestableObjectList;
			Parameter0 = data[0].ToString();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToFloatArray();
			Parameter4 = data[4].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
