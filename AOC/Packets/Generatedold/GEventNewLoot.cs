using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 87
	public class GEventNewLoot : BaseEvent
	{
		// From : 2459106
		protected System.Int64 Parameter0 {get;set;}
		// From : 1
		protected System.Int64 Parameter1 {get;set;}
		// From : 2458698
		protected System.Int64 Parameter2 {get;set;}
		// From : "PurpleNChill"
		protected System.String Parameter3 {get;set;}
		// From : [-28.65694,-23.293032]
		protected System.Single[] Parameter4 {get;set;}
		// From : 249.1734
		protected System.Double Parameter5 {get;set;}
		// From : true
		protected System.Boolean Parameter6 {get;set;}
		// From : 1
		protected System.Int64 Parameter7 {get;set;}
		// From : "I8DLO4qEXUypJS7XOuc+iQ=="
		protected System.String Parameter8 {get;set;}
		// From : 637186241728067767
		protected System.Int64 Parameter9 {get;set;}
		// From : []
		protected System.Single[] Parameter14 {get;set;}
		// From : "itIULCIYn024ip+mX8E24Q=="
		protected System.String Parameter15 {get;set;}
		// From : "7PkyNy1gf0u3ENYaFtO04A=="
		protected System.String Parameter16 {get;set;}
		// From : 0
		protected System.Int64 Parameter18 {get;set;}
		// From : 3
		protected System.Int64 Parameter19 {get;set;}
		// From : 87
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewLoot(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewLoot;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToFloatArray();
			Parameter5 = data[5].ToDouble();
			Parameter6 = data[6].ToBoolean();
			Parameter7 = data[7].ToInt();
			Parameter8 = data[8].ToString();
			Parameter9 = data[9].ToInt();
			Parameter14 = data[14].ToFloatArray();
			Parameter15 = data[15].ToString();
			Parameter16 = data[16].ToString();
			Parameter18 = data[18].ToInt();
			Parameter19 = data[19].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
