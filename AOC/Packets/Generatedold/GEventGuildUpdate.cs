using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 92
	public class GEventGuildUpdate : BaseEvent
	{
		// From : 2
		protected System.Int64 Parameter9 {get;set;}
		// From : 46675913795
		protected System.Int64 Parameter10 {get;set;}
		// From : 800
		protected System.Int64 Parameter13 {get;set;}
		// From : "BACON"
		protected System.String Parameter15 {get;set;}
		// From : "bMcwYf4DDUil7cjRviSGJg=="
		protected System.String Parameter16 {get;set;}
		// From : {"0":68300000}
		protected System.Object Parameter18 {get;set;}
		// From : 6
		protected System.Int64 Parameter19 {get;set;}
		// From : 31
		protected System.Int64 Parameter20 {get;set;}
		// From : 11
		protected System.Int64 Parameter21 {get;set;}
		// From : 15
		protected System.Int64 Parameter22 {get;set;}
		// From : 16777215
		protected System.Int64 Parameter23 {get;set;}
		// From : 0.55
		protected System.Double Parameter24 {get;set;}
		// From : 6767
		protected System.Int64 Parameter27 {get;set;}
		// From : 0
		protected System.Int64 Parameter29 {get;set;}
		// From : 92
		protected System.Int64 Parameter252 {get;set;}
		protected GEventGuildUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.GuildUpdate;
			Parameter9 = data[9].ToInt();
			Parameter10 = data[10].ToInt();
			Parameter13 = data[13].ToInt();
			Parameter15 = data[15].ToString();
			Parameter16 = data[16].ToString();
			Parameter18 = data[18];
			Parameter19 = data[19].ToInt();
			Parameter20 = data[20].ToInt();
			Parameter21 = data[21].ToInt();
			Parameter22 = data[22].ToInt();
			Parameter23 = data[23].ToInt();
			Parameter24 = data[24].ToDouble();
			Parameter27 = data[27].ToInt();
			Parameter29 = data[29].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
