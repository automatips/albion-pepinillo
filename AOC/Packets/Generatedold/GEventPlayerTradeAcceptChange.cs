using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 164
	public class GEventPlayerTradeAcceptChange : BaseEvent
	{
		// From : 4005
		protected System.Int64 Parameter0 {get;set;}
		// From : 164
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPlayerTradeAcceptChange(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PlayerTradeAcceptChange;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
