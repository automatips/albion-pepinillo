using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 222
	public class GEventPartyOnClusterPartyJoined : BaseEvent
	{
		// From : ["CcxeEDirHk+YRdRgo8iwPg=="]
		protected System.String[] Parameter0 {get;set;}
		// From : "Bg=="
		protected System.String Parameter1 {get;set;}
		// From : "AAAAAAAA"
		protected System.String Parameter2 {get;set;}
		// From : 222
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPartyOnClusterPartyJoined(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PartyOnClusterPartyJoined;
			Parameter0 = data[0].ToStringArray();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
