using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 229
	public class GEventExitEnterStart : BaseEvent
	{
		// From : 828058
		protected System.Int64 Parameter0 {get;set;}
		// From : 48394686
		protected System.Int64 Parameter1 {get;set;}
		// From : 48396686
		protected System.Int64 Parameter2 {get;set;}
		// From : 801062
		protected System.Int64 Parameter3 {get;set;}
		// From : 229
		protected System.Int64 Parameter252 {get;set;}
		protected GEventExitEnterStart(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ExitEnterStart;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
