using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 13
	public class GEventFriendRequestInfos : BaseEvent
	{
		// From : ["dvT+Jb71sE2dSIxY8pesIg=="]
		protected System.String[] Parameter1 {get;set;}
		// From : ["XlunaX"]
		protected System.String[] Parameter2 {get;set;}
		// From : ["Blessed"]
		protected System.String[] Parameter3 {get;set;}
		// From : "Ag=="
		protected System.String Parameter4 {get;set;}
		// From : "CA=="
		protected System.String Parameter5 {get;set;}
		// From : "AQ=="
		protected System.String Parameter6 {get;set;}
		// From : 269
		protected System.Int64 Parameter252 {get;set;}
		protected GEventFriendRequestInfos(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.FriendRequestInfos;
			Parameter1 = data[1].ToStringArray();
			Parameter2 = data[2].ToStringArray();
			Parameter3 = data[3].ToStringArray();
			Parameter4 = data[4].ToString();
			Parameter5 = data[5].ToString();
			Parameter6 = data[6].ToString();
			Parameter252 = data[252].ToInt();
		}
	}
}
