using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 51
	public class GEventTakeSilver : BaseEvent
	{
		// From : 953689
		protected System.Int64 Parameter0 {get;set;}
		// From : 57020062
		protected System.Int64 Parameter1 {get;set;}
		// From : 954515
		protected System.Int64 Parameter2 {get;set;}
		// From : 598500
		protected System.Int64 Parameter3 {get;set;}
		// From : 0
		protected System.Int64 Parameter4 {get;set;}
		// From : 0
		protected System.Int64 Parameter5 {get;set;}
		// From : true
		protected System.Boolean Parameter6 {get;set;}
		// From : true
		protected System.Boolean Parameter7 {get;set;}
		// From : 51
		protected System.Int64 Parameter252 {get;set;}
		protected GEventTakeSilver(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.TakeSilver;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToBoolean();
			Parameter7 = data[7].ToBoolean();
			Parameter252 = data[252].ToInt();
		}
	}
}
