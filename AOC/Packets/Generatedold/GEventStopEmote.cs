using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 68
	public class GEventStopEmote : BaseEvent
	{
		// From : 77384
		protected System.Int64 Parameter0 {get;set;}
		// From : 68
		protected System.Int64 Parameter252 {get;set;}
		protected GEventStopEmote(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.StopEmote;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
