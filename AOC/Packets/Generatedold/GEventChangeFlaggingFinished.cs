using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 73
	public class GEventChangeFlaggingFinished : BaseEvent
	{
		// From : 2518543
		protected System.Int64 Parameter0 {get;set;}
		// From : 2
		protected System.Int64 Parameter1 {get;set;}
		// From : 329
		protected System.Int64 Parameter252 {get;set;}
		protected GEventChangeFlaggingFinished(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ChangeFlaggingFinished;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
