using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 103
	public class GEventNewChainSpell : BaseEvent
	{
		// From : 954800
		protected System.Int64 Parameter0 {get;set;}
		// From : [346.02,129.03]
		protected System.Single[] Parameter1 {get;set;}
		// From : 155
		protected System.Int64 Parameter2 {get;set;}
		// From : 0
		protected System.Int64 Parameter3 {get;set;}
		// From : 10
		protected System.Int64 Parameter4 {get;set;}
		// From : 953689
		protected System.Int64 Parameter5 {get;set;}
		// From : 57039255
		protected System.Int64 Parameter6 {get;set;}
		// From : [950730]
		protected System.Int64[] Parameter7 {get;set;}
		// From : 103
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewChainSpell(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewChainSpell;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToFloatArray();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToIntArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
