using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 134
	public class GEventCharacterStatsDeathHistory : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : "Zigvalt"
		protected System.String Parameter1 {get;set;}
		// From : ["@MOB_T5_MOB_DEMON_IMP_YELLOW_VETERAN","Zigvalt","Emirk","Joakiga","@MOB_T4_MOB_DEMON_SPIKED","@MOB_T6_MOB_HRD_HERETIC_SCAVENGER_VETERAN"]
		protected System.String[] Parameter2 {get;set;}
		// From : [198065700,449526962,521181663,618588663,123665400,1488367888]
		protected System.Int64[] Parameter3 {get;set;}
		// From : ["VJyITC+FV0CksgCg3LErlw==","kFGUY75/NEygwDmG+dBq8A==","ej3TCfwfIE+FdUxDcburKg==","Y3VDsZaiYkiK+s5w2pOT9g==","dbMiTnwCd06vn9SntYy0BQ==","FDfZyQCogkeFTIdyDtpujQ=="]
		protected System.String[] Parameter4 {get;set;}
		// From : 134
		protected System.Int64 Parameter252 {get;set;}
		protected GEventCharacterStatsDeathHistory(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.CharacterStatsDeathHistory;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToStringArray();
			Parameter3 = data[3].ToIntArray();
			Parameter4 = data[4].ToStringArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
