using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 117
	public class GEventUpdateWardrobe : BaseEvent
	{
		// From : 2382440
		protected System.Int64 Parameter0 {get;set;}
		// From : [-1,-1,-1,55,-1,-1,-1]
		protected System.Int64[] Parameter1 {get;set;}
		// From : 373
		protected System.Int64 Parameter252 {get;set;}
		protected GEventUpdateWardrobe(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.UpdateWardrobe;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToIntArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
