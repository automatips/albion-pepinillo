using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 211
	public class GEventPartyJoined : BaseEvent
	{
		// From : 29973
		protected System.Int64 Parameter0 {get;set;}
		// From : 1
		protected System.Int64 Parameter2 {get;set;}
		// From : "vPV/PIyCNk+pXYp3YmpjDw=="
		protected System.String Parameter3 {get;set;}
		// From : ["vPV/PIyCNk+pXYp3YmpjDw==","CcxeEDirHk+YRdRgo8iwPg=="]
		protected System.String[] Parameter4 {get;set;}
		// From : ["Krooocker","Palurdo"]
		protected System.String[] Parameter5 {get;set;}
		// From : "AAA="
		protected System.String Parameter6 {get;set;}
		// From : "Fxw="
		protected System.String Parameter7 {get;set;}
		// From : "DRk="
		protected System.String Parameter8 {get;set;}
		// From : [-1,-1]
		protected System.Int64[] Parameter9 {get;set;}
		// From : [true,true]
		protected System.Boolean[] Parameter10 {get;set;}
		// From : 211
		protected System.Int64 Parameter252 {get;set;}
		protected GEventPartyJoined(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.PartyJoined;
			Parameter0 = data[0].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToString();
			Parameter4 = data[4].ToStringArray();
			Parameter5 = data[5].ToStringArray();
			Parameter6 = data[6].ToString();
			Parameter7 = data[7].ToString();
			Parameter8 = data[8].ToString();
			Parameter9 = data[9].ToIntArray();
			Parameter10 = data[10].ToBooleanArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
