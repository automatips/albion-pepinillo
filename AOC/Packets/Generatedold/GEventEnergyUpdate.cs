using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 7
	public class GEventEnergyUpdate : BaseEvent
	{
		// From : 2518543
		protected System.Int64 Parameter0 {get;set;}
		// From : 57136005
		protected System.Int64 Parameter1 {get;set;}
		// From : 275.0
		protected System.Double Parameter3 {get;set;}
		// From : 7
		protected System.Int64 Parameter4 {get;set;}
		// From : 2518543
		protected System.Int64 Parameter5 {get;set;}
		// From : 711
		protected System.Int64 Parameter6 {get;set;}
		// From : 7
		protected System.Int64 Parameter252 {get;set;}
		protected GEventEnergyUpdate(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.EnergyUpdate;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter3 = data[3].ToDouble();
			Parameter4 = data[4].ToInt();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
