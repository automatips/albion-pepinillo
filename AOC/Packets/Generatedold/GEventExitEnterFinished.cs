using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 231
	public class GEventExitEnterFinished : BaseEvent
	{
		// From : 948974
		protected System.Int64 Parameter0 {get;set;}
		// From : 231
		protected System.Int64 Parameter252 {get;set;}
		protected GEventExitEnterFinished(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.ExitEnterFinished;
			Parameter0 = data[0].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
