using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 236
	public class GEventQuestGiverInfoForPlayer : BaseEvent
	{
		// From : 1752
		protected System.Int64 Parameter0 {get;set;}
		// From : "FAcIM+NJB0aFo7jx+ARp3w=="
		protected System.String Parameter1 {get;set;}
		// From : 20
		protected System.Int64 Parameter2 {get;set;}
		// From : 0
		protected System.Int64 Parameter3 {get;set;}
		// From : 637185687285671942
		protected System.Int64 Parameter4 {get;set;}
		// From : 236
		protected System.Int64 Parameter252 {get;set;}
		protected GEventQuestGiverInfoForPlayer(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.QuestGiverInfoForPlayer;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToString();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToInt();
			Parameter4 = data[4].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
