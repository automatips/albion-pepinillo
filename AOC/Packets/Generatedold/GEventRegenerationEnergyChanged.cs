using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 81
	public class GEventRegenerationEnergyChanged : BaseEvent
	{
		// From : 1162981
		protected System.Int64 Parameter0 {get;set;}
		// From : 56888656
		protected System.Int64 Parameter1 {get;set;}
		// From : 252.0
		protected System.Double Parameter2 {get;set;}
		// From : 252.0
		protected System.Double Parameter3 {get;set;}
		// From : 3.659622
		protected System.Double Parameter4 {get;set;}
		// From : 637186254375976371
		protected System.Int64 Parameter5 {get;set;}
		// From : 81
		protected System.Int64 Parameter252 {get;set;}
		protected GEventRegenerationEnergyChanged(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.RegenerationEnergyChanged;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToDouble();
			Parameter3 = data[3].ToDouble();
			Parameter4 = data[4].ToDouble();
			Parameter5 = data[5].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
