using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 140
	public class GEventFullAchievementProgressInfo : BaseEvent
	{
		// From : 325
		protected System.Int64 Parameter0 {get;set;}
		// From : [364,369,381,386,394,400,408,410]
		protected System.Int64[] Parameter1 {get;set;}
		// From : "CQQAAAAABgA="
		protected System.String Parameter2 {get;set;}
		// From : [0.5123967,0.13482143,0.0072,0.256,0.656,0.0017777778,0.0066696308,0.056382503]
		protected System.Single[] Parameter3 {get;set;}
		// From : ["[[14880004]]","[[3020001]]","[[2160000]]","[[17925000]]","[[14760000]]","[[45000]]","[[450000]]","[[289434375]]"]
		protected System.String[] Parameter4 {get;set;}
		// From : 140
		protected System.Int64 Parameter252 {get;set;}
		protected GEventFullAchievementProgressInfo(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.FullAchievementProgressInfo;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToIntArray();
			Parameter2 = data[2].ToString();
			Parameter3 = data[3].ToFloatArray();
			Parameter4 = data[4].ToStringArray();
			Parameter252 = data[252].ToInt();
		}
	}
}
