using System.Collections.Generic; 
using AOC.Library.Primitive; 
using AOC.Util;
using Albion.Common.Photon;
namespace AOC.Packets.Generated
{

	//Event | BaseCode - 32
	public class GEventNewHarvestableObject : BaseEvent
	{
		// From : 949864
		protected System.Int64 Parameter0 {get;set;}
		// From : 947913
		protected System.Int64 Parameter1 {get;set;}
		// From : 637186252465102788
		protected System.Int64 Parameter2 {get;set;}
		// From : "H28okCwsDkKlZlDJ1bbLlQ=="
		protected System.String Parameter3 {get;set;}
		// From : 23
		protected System.Int64 Parameter5 {get;set;}
		// From : 31
		protected System.Int64 Parameter6 {get;set;}
		// From : 4
		protected System.Int64 Parameter7 {get;set;}
		// From : [318.0701,-41.571766]
		protected System.Single[] Parameter8 {get;set;}
		// From : 132.68182
		protected System.Double Parameter9 {get;set;}
		// From : 0
		protected System.Int64 Parameter11 {get;set;}
		// From : 32
		protected System.Int64 Parameter252 {get;set;}
		protected GEventNewHarvestableObject(Dictionary<byte,object> data) : base(data)
		{
			BaseCode = EventCodes.NewHarvestableObject;
			Parameter0 = data[0].ToInt();
			Parameter1 = data[1].ToInt();
			Parameter2 = data[2].ToInt();
			Parameter3 = data[3].ToString();
			Parameter5 = data[5].ToInt();
			Parameter6 = data[6].ToInt();
			Parameter7 = data[7].ToInt();
			Parameter8 = data[8].ToFloatArray();
			Parameter9 = data[9].ToDouble();
			Parameter11 = data[11].ToInt();
			Parameter252 = data[252].ToInt();
		}
	}
}
