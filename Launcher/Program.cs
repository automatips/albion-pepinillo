﻿using DeviceId;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Cryptography;

namespace Launcher
{
    class Program
    {
        public static string SERVER;

        static Dictionary<string, string> internetFiles = new Dictionary<string, string>();
        static Dictionary<string, string> LocalFiles = new Dictionary<string, string>();
        private static string DID;

        static void Main(string[] args)
        {
            DID = new DeviceIdBuilder().AddMachineName().AddMacAddress().AddProcessorId().AddMotherboardSerialNumber().ToString();
            SERVER = "3.16.45.116";

            try
            {
                Directory.CreateDirectory("images");
            }
            catch (Exception ex)
            {
                // handle them here
            }

            using (WebClient client = new WebClient())
            {
                string s = client.DownloadString("http://" + SERVER + "/Update/hashes.txt");

                Console.WriteLine("checking updates...");

                var resultString = s.Replace("\r\n", "\n");
                var lineas = resultString.Split(System.Environment.NewLine.ToCharArray());
                int index = 0;
                foreach (var linea in lineas)
                {

                    var key = "";
                    var value = "";
                    if (linea.Contains("hash"))
                    {
                        key = linea.Replace(@"MD5 hash de PublicRelease\", "").Replace(":", "");
                        value = lineas[index + 1].Replace(" ", "").ToUpper();
                        internetFiles.Add(key, value);

                    }
                    else
                    {
                        continue;
                    }

                    index = index + 2;
                }

             
            }

            var e = internetFiles;

            string[] files = Directory.GetFiles(".", "*.*", SearchOption.AllDirectories);
            foreach (string file in files)
            {


                try
                {
                    if (file.ToLower().Contains("ipserver.txt"))
                    {
                        continue;
                    }
                    string key = "";
                    string value = "";
                    key = file.Replace(".\\", "");
                    using (var md5 = MD5.Create())
                    {
                        using (var stream = File.OpenRead(file))
                        {
                            value = BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "");
                        }
                    }
                    LocalFiles.Add(key, value);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("12");
                    Console.WriteLine(ex.ToString());
                }

            }

            //var cantidad_archivos = internetFiles.Count();

            foreach (KeyValuePair<string, string> archivo in internetFiles)
            {

                try
                {

                    var nombre = archivo.Key;
                    var md5 = archivo.Value;
                    if (LocalFiles.ContainsKey(nombre))
                    {
                        var nombre_local = nombre;
                        var md5_local = "";
                        LocalFiles.TryGetValue(nombre, out md5_local);

                        if (!md5_local.Equals(md5))
                        {
                            ActualizarArchivo(nombre);
                        }
                    }
                    else
                    {
                        ActualizarArchivo(nombre);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());

                }
            }
            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName.Replace("Launcher.exe","") + "Map.exe"; // Your absolute PATH 
                startInfo.UseShellExecute = false;
                startInfo.Arguments = ""+ DID; 
                startInfo.Arguments = Path.GetFullPath("items.json"); 
                startInfo.Arguments = Path.GetFullPath("mobs.json"); 
                startInfo.Arguments = Path.GetFullPath("worlds.json"); 
                Process.Start(startInfo);


            
            }
            catch (Exception ex)
            {
                Console.WriteLine( System.AppDomain.CurrentDomain.BaseDirectory);
                Console.WriteLine(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
                Console.WriteLine(ex);
                Console.ReadKey();

            }


        }

        private static void ActualizarArchivo(string nombre)
        {
            string path = System.AppDomain.CurrentDomain.BaseDirectory;
            try { File.Delete(@"./" + nombre); } catch (Exception ex) { }
            try
            {
                using (var client = new WebClient())
                {
                    Console.WriteLine("Actualizando " + nombre);
                    client.DownloadFile("http://" + SERVER + "/Update/" + nombre.Replace('\\', '/'), nombre);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("55");
                //Console.WriteLine("Error actualizando " + nombre);
                Console.WriteLine(ex);
            }
           
        }
    }
}
